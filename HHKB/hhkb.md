# HHKBの設定メモ

- DEL <-> BS

- Super L -> Fn
- Super L <-> Alt
    - 結果としてSuper L -> Alt, Alt L -> Fn
    - アプリケーションランチャーはSuper Rに割り振った

- Fn + 1
    - ミュート
- Fn + 2
    - ボリュームダウン
- Fn + 3
    - ボリュームアップ

- Fn + Ctrl + 1
    - work space 1への移動

- Fn + Ctrl + 2
    - work space 2への移動

- Fn + Ctrl + 3
    - work space 3への移動
