# このリポジトリについて

Arch Linuxの情報を以前はTex_textというリポジトリで管理していたが、ごちゃごちゃしてわかりにくいためまとめ直す。

## コンテンツ

### 動作環境(コピペ用)

毎回自分の動作環境を調べるのも面倒なのでコピペできるようここに書いておく。

- デスクトップマシン(自作マシン)
    - OS: Arch Linux x86\_64
    - Kernel: 6.11.4-arch2-1
    - CPU: 13th Gen Intel i7-13700 (24) @ 5.100GHz
    - Memory: 31851MiB

- ラップトップマシン(Lenovo thinkpad E14 gen5)
    - OS: Arch Linux x86_64
    - Kernel: 6.11.4-arch1-1
    - CPU: 13th Gen Intel i5-13500H (16) @ 4.700GHz
    - Memory: 15655MiB

### Arch Linux インストール

- [install_uefi](./install_uefi)
    - UEFI環境でのArch Linuxのインストール方法のメモ

- [advanced](./advanced)
    - Arch Linuxをインストールした後の細々とした設定(サウンド、wifi,ブートローダー)

- [theme](./theme)
    - デスクトップ環境のテーマ、アイコンテーマのカスタムのメモ

- [wayland](./wayland)
    - WaylandでのKDE Plasmaデスクトップの構築

- [env var](./env_var)
    - 環境変数のノート

### タイル型ウィンドウマネージャー

- [i3wm](./i3wm)
    - i3wmでの環境構築

- [sway](./sway)
    - swayでの環境構築

- [polkit](./polkit)
    - polkit関連でハマったこと

### キーボード

- [RK61](./RK61)
    - 愛用しているメカニカルキーボードRk61の使い方のメモ

- [HHKB](./HHKB)
    - HHKBの設定メモ

- [keymap](./keymap)
    - Linuxでkeymapをカスタムするときのメモ

### 色々な ツール & ノウハウ

- [git & GitHub & GitLab](./git)
    - git & GitHub & GitLab のノート

- [dd command](./dd_command)
    - dd コマンドのメモ

- [curl](./curl)
    - culrコマンドのメモ

- [battery](./battery)
    - Arch Linuxをラップトップで使う場合の管理事情

- [grub](./grub)
    - grubの設定

- [clock](./clock)
    - hardware clock と system clock

- [GnuPG](./gnupg)
    - GnuPGによる\*.isoファイルの検証
;
- [Virtual Box](./virtual_box)
    - Virtual Box のメモ

- [act](./act)
    - GitHub Actionsをローカルで動かすactについて

- [mount](./mount)
    - mountコマンドについて
    - NTFSをLinuxでマウントする方法

- [du](./du)
    - duコマンドのメモ

- [Docker](./docker)
    - Dockerのメモ

- [デバッガ](./debugger)
    - デバッガ(GDB,LLDB)のメモ

- [CLIの論文管理ツールpubs](./pubs)
    - pubsの使い方などのメモ

- [HITRAN](./hitran)

### 番外編: Windowsのメモ

- [msys2](./msys2)
    - msys2: WindowsでGNU tool chainを使う
- [Flow Launcher](./flow_launcher)
    - Windowsで使えるrofiのようなランチャー
- [WSL](./wsl)
    - wslの設定

### 記録
- [migrate_storage](./migrate_storage)
    - Linuxのストレージ移行メモ

- [diary_2023](./diary/diary_2023.md)
- diary_2024
    - [part_1](./diary/diary_2024.md)
    - [part_2](./diary/diary_2024_part_2.md)
- diary_2025
    - [part_1](./diary/diary_2025.md)
    - [part_2](./diary/diary_2025_part_2.md)
    - [part_3](./diary/diary_2025_part_3.md)

- [fcitx5](./fcitx5)
    - fcitxをfcitx5に乗り換えた時のメモ

- [thinkpad E14 Gen5](./thinkpad)
    - thinkpad E14 Gen5のメモ
