# act

GiHub Actionsをローカルで実行するためのツール。

## install

```bash
$ sudo pacman -S act
```

## 実行

プロジェクト中にディレクトリ`.github/workflows`を作成し、そこに`<name>.yml`を置く

このプロジェクトのルートで

```bash
$ act
```

を実行する。
