# デスクトップ環境(Xfce4)を構築後の種々の設定

デスクトップ環境を構築した後で必要になった種々の設定のメモ。

## サウンド

サウンドクライアントとしては`pulseaudio`をインストールする

```bash
$ sudo pacman -S pulseaudio
```

このままでは、GUIでの調整ができないためGUIのクライアントもインストールする。

```bash
$ sudo pacman -S pavucontorol
```

## wifi接続(netctl)

- 参考
    - 1 https://aznote.jakou.com/archlinux/wireless.html
    - 2 https://wiki.archlinux.jp/index.php/Netctl


毎回`wpa_supplicant`で手動でwifi接続をするのは骨が折れるので`netctl`を導入する。

### netctl

インストール

```bash
$ sudo pacman -S netctl
```

なお、依存パッケージとしては`dhcpcd`, `wpa_supplicant`が必要である。

### 設定

`/etc/netctl/examples` にあるサンプルから必要なものを選び `/etc/netctl/`に適当な名前
をつけてコピーする。たいていの場合`wireless-wpa`を元に設定ファイルを作ればok。

次にそのファイルを開きInterface, ESSID, Key に書き込む。

次にインターフェイスをdownする

```bash
$ sudo iplink set [interface-name] down
```

最後に

```bash
$ sudo netctl start [profile_name]
```

として接続する。

切断する際には

```bash
$ sudo netctl stop [profile_name]
```

とする。

### 自動接続

```bash
$ netctl enable [profile_name]
```

とすれば起動時にデーモンが起動する。

## wifi接続(iwd)

### インストール

パッケージのインストールを行い、`iwd.service`を有効化して起動する

```bash
$ sudo pacman -S iwd
$ sudo systemctl enable iwd.service
$ sudo systemctl start iwd.service
```

### 基本的な使い方

- 参考
 [iwd - ArchWiki](https://wiki.archlinux.jp/index.php/Iwd#.E3.82.A4.E3.83.B3.E3.82.B9.E3.83.88.E3.83.BC.E3.83.AB)

対話型のプロンプトは以下のコマンドで起動する

```bash
# iwctl
```

ヘルプが見たければ

```bash
[iwd]# help
```

wifiデバイスを検索するには

```bash
[iwd]# device list
```

を実行する。

ネットワークをスキャンするには

```bash
[iwd]# station <device name> scan
```

利用可能なネットワーク一覧を得るには

```bash
[iwd]# station <device name> get-networks
```

最後にネットワークに接続するには

```bash
[iwd]# statoin <device name> connect <SSID>
```

現在登録されているネットワーク一覧を得るには

```bash
[iwd] known-networks list
```

既存のネットワークを削除するには

```bash
[iwd] known-networks <SSID> forget
```

## ブートローダー

- 参考
    - https://forum.manjaro.org/t/warning-os-prober-will-not-be-executed-to-detect-other-bootable-partitions/57849/2
    - https://h3poteto.hatenablog.com/entry/2021/03/20/234446
    - https://wiki.archlinux.jp/index.php/GRUB#.E3.83.87.E3.83.A5.E3.82.A2.E3.83.AB.E3.83.96.E3.83.BC.E3.83.88

### os-proberでデュアルブート

```bash
$ sudo pacman -S os-prober
```

で`os-prober`をインストール。

現在のバージョンでは`os-prober`が動作しないように設定されているため、`/etc/default/grub`に

```txt
GRUB_DISABLE_OS_PROBER=false
```

と追加して

```bash
$ sudo update-grub
```

とする

- 追記

ssdを載せ替えた後では`update-grub`がうまく行かなかったので

```bash
$ sudo grub-mkconfig -o /boo/grub/grub.cfg
```

としたらブート選択画面が出るようになった。

## swap file

後からswap fileを作成する時のメモ


参考：https://wiki.archlinux.jp/index.php/%E3%82%B9%E3%83%AF%E3%83%83%E3%83%97

今回は`/home`以下に作成する
```bash
 $ sudo mkswap -U clear --size 20G --file /home/swapfile
Setting up swapspace version 1, size = 20 GiB (21474832384 bytes)
no label, UUID=00000000-0000-0000-0000-000000000000
```

続いてswapの有効化

```bash
$ sudo swapon /home/swapfile
```

最後に`fstab`を編集してswap fileのエントリを追加

/etc/fstab
```txt
#/home/swapfile
/home/swapfile none swap defaults 0 0
```
