# ラップトップPCのバッテリー管理

## 共通のtips

メーカーによらない共通の部分

### tlp

いい感じに調節してくれるデーモンらしい。
インストールして、デーモンをたてとけばok

```bash
$ sudo pacman -S tlp
$ sudo systemctl enable tlp.service
```

より進んだ設定を書くならば以下のようにする

```txt
# ACアダプタに繋がっているときはパフォーマンス重視
CPU_SCALING_GOVERNOR_ON_AC=performance
# バッテリー駆動のときは要求に応じたパフォーマンス
CPU_SCALING_GOVERNOR_ON_BAT=ondemand

# ACアダプタに繋がっているときはターボブーストを有効化
CPU_BOOST_ON_AC=1
# バッテリー駆動のときはターボブーストを無効化
CPU_BOOST_ON_BAT=0
```

## Asus製のラップトップ

以前は同じツールのバイナリを直接ディレクトリにおいて自分でpathを通していたが、
AURで配布されるようになっているようなのでyayでインストールした。

## ThinkPad E14 Gen5

tpacpi-batをインストールする

```bash
$ sudo pacman -S tpacpi-bat
```

現在のしきい値を確認する

```bash
$ sudo tpacpi-bat -g ST <1,2,0> # 充電を開始するパーセンテージ
$ sudo tpacpi-bat -g SP <1,2,0> # 充電を終了するパーセンテージ
```

しきい値を設定する。ここでは充電を80\%で終了し、70\%まで減った後に充電を開始するように設定した

```bash
$ sudo tpacpi-bat -s ST <1,2,0> 70 # 充電を開始するパーセンテージ
$ sudo tpacpi-bat -s SP <1,2,0> 80 # 充電を終了するパーセンテージ
```
