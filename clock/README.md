# clock 

参考:[arch wiki システム時間](https://wiki.archlinux.jp/index.php/%E3%82%B7%E3%82%B9%E3%83%86%E3%83%A0%E6%99%82%E5%88%BB#.E3.82.B7.E3.82.B9.E3.83.86.E3.83.A0.E3.82.AF.E3.83.AD.E3.83.83.E3.82.AF)

## hardware clock

hardware clockとはマザーボード上のCMOS clockで管理されている時計である。

hardware clockの状態を確認するには以下のコマンドを実行する

```bash
$ sudo hwclock --show 
```

## system clock

system clock(またの名はsoftware clock)はLinuxカーネルによって管理されている時計である。

system clockの状態を確認するには以下のコマンドを実行する

```bash
$ timedatectl status
```

hardware clockをsystem clockに合わせたいときは以下のコマンドを実行する

```bash
$ sudo hwclock --systohc
```

## ntpサーバーを使ってsystem clockを合わせる

インターネットに接続されている場合ではntpサーバーを用いた時間合わせが便利である。

まず`ntp`パッケージをインストールする

```bash
$ sudo pacman -S ntp
```

ここでntpサーバーとしてはNICTのものを用いて以下のコマンドを実行する。

```bash
$ sudo ntpdate ntp.nict.jp
```
