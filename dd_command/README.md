# dd コマンドのメモ

- 参考
    - [RedHat: 3.2USBインストールメディアの作成](https://access.redhat.com/documentation/ja-jp/red_hat_enterprise_linux/7/html/installation_guide/sect-making-usb-media)

ddコマンドでusbメモリでisoイメージを書き込む

```bash
$ sudo dd if=<path to *.iso file> of=<device file to write(ex: /dev/sda)> bs=1048567 
```

ここで`if`は書き込むファイルへのパスで`of`は書き込み先のストレージのデバイスファイルを設定する。
また、`bs`は一度に読み書きをするファイルのサイズ(ブロックサイズ)である。
`bs=1048567`と設定した場合は一度に1MiB読み書きするという意味である。
