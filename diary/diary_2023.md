# Linux関係の日記的なもの

逐一、まとめるにはめんどくさいが、記録しておきたいことを記しておく。後でまとめるかも。

## 1/20頃 

thunderbirdのバージョンが102.7.0にバージョンアップした際に、Microsoft365関係の不具合
(MS側がプロトコル周りを変更したのが原因らしい)がありサーバーに接続できなくなった。

そのため、パッケージバージョンを102.6.1にダウングレードした。
今回は/var/cache/pacman/pkgに102.6.1のパッケージのキャッシュがあったためそれを用いてダウングレードした。コマンド操作は以下の通り

```bash
$ sudo pacman -U /var/cache/pcman/pkg/thunderbird-102.6.1-x86_64.pkg.tar.zst

```

また、このままではパッケージの更新時にthunderbirdも更新されてしまうので/etc/pacman.conf
のoptionsのセクションに

```pacman.conf
[options]
# thunderbirdのアップデートを一時中止
IgnorePkg=thunderbird
```

としてアップデートを一時中止状態にした[1]。

また今回はキャッシュに残ったパッケージを利用してダウングレードしたが、ほしいバージョンがキャッシュにない場合やキャッシュを削除してしまった場合ではアーカイブを用いるとよい。

```bash
$ sudo pacman -U https://archive.archlinux.org/packages/ ... packagename.pkg.tar.xz
```

とすればパッケージをダウングレードできる。[2]


### 参考
1. [archwiki: パッケージのダウングレード](https://wiki.archlinux.jp/index.php/%E3%83%91%E3%83%83%E3%82%B1%E3%83%BC%E3%82%B8%E3%81%AE%E3%83%80%E3%82%A6%E3%83%B3%E3%82%B0%E3%83%AC%E3%83%BC%E3%83%89)
2. [archwiki: Arch Linux Archive](https://wiki.archlinux.jp/index.php/Arch_Linux_Archive)

## 2/2頃

thunderbirdの102.7.1がリリースされMicrosoft365関係の不具合は解消されてようなのでpacman.confのIgnorePkgをコメントアウトしてアップデートした。結果として2/2現在正常に動いている。

## 2/3頃

neovimでのLSPを導入してpython,C++,Rustのコードの補完ができるようになった。

## 2/5頃

Luaでもできるようになった。[Luaの公式ドキュメント](https://www.lua.org/manual/5.4/)と[公式教科書](http://www.lua.org/pil/contents.html)を読んでC APIを読み始めた。
