# 2024年の日記的なメモ

とりあえずメモしておく用。
あとではてなとかにまとめるかも...

### 4/15

新ラップトップであるThinkPadE14Gen5でHDMI -> VGAケーブルを使うと映像が出力されなかった。

色々調べるとFPSや解像度のせいのようなことがあったので解像度を1920x1080にしてFPSを59.9と60より小さくしてもダメだった。

結局変換部分でデジタル->アナログの変換を行うためかうまく電源供給されないなどの"相性問題"があるらしい

そこでthunderbolt3対応のusb-cからVGA,HDMI,USB3.0,RJ45などに変換するハブを購入した。
これを用いた場合ではVGAによる映像出力はうまくいった。

### 4/17

Linuxでプリンタを使おうとしてドライバ等の設定を行った。結論を先に書くとうまく設定をすることはできなかった。

まず`CUPS`をインストールしてデーモンを立ち上げる。

```bash
$ sudo pacman -S cups
$ suso systemctl enable cups.service
$ sudo systemctl start cups.service
```

続いてブラウザで`localhost:631`にアクセスして`CUPS`のGUIにアクセスする。
[エンジニアの入り口: 【Linux入門】しっかりわかる！プリンタ管理と印刷コマンド](https://eng-entrance.com/linux-printer-cups)を参考に手順を進めた。
しかし、プリンタ選択で目的のプリンタ(Canon)が出てこなかったので公式サイトを調べると`CARPS2/LIPSLX`に対応したドライバが必要なことが分かった。

そこで[Arch Wiki: CUPS/Printer-specific problems](https://wiki.archlinux.org/title/CUPS/Printer-specific_problems)を参考にして、結局`cnrdrvcups-lb`をインストールした。

```bash
$ yay -S cnrdrvcups-lb
```

これをインストールしたあとでもう一回`CUPS`の設定に戻ると目的のプリンタが見つかったので無事設定を完了することができた。
そこで、試しに適当なpdfファイルを印刷してみるとプリンタ側にジョブが送信されるところまではうまくいくが、そこからいくら待っても印刷が始まらず、PC側では`CUPS`から立ち上がっているプロセスが暴走してしまっていた。
この暴走は`CUPS`のデーモン自体を停止すれば停止できた。しかし、結局印刷することはできなかった。

### 4/26

Arch Linuxのインストールされているディスクトップpcの`/`ディレクトリがいっぱいいっぱい(残り1GiBほど)であった。

なにが原因かを`du`コマンドで調べていくと`/usr/share`の容量がでかくてさらに`/usr/share/fonts`がほぼ7GiBも占めていた。

その中を調べるとどうやらAURからinstallした`nerdfont`が原因のように見えた。


そんなにフォントのサイズが大きいなら`$HOME`以下に置きたい気もする。

実際[nerd-fontsのGitHubのページ](https://github.com/ryanoasis/nerd-fonts?tab=readme-ov-file#option-4-arch-extra-repository)を見ると`~/.local/share` 以下に`fonts`ディレクトリを作成して`~/.local/share/fonts`以下にフォントのファイルGitHubからダウンロードしているし...

### 4/27

バイナリビューアーとして[hexyl](https://github.com/sharkdp/hexyl)がいいらしい

Rust製!

Arch Linuxへのインストールは

```bash
$ sudo pacman -S hexyl
```

でok使うには


```bash
$ hexyl <path_to_file>
```

を実行すればok。結果は標準出力に表示される。

### 5/2

#### GParted

Arch Linux x86\_64の`/`パーティションの容量が足りなくなったので
容量に余裕のある`/home`パーティションから12GiBほど`/`に追加で割りあてようとした。
こときに`Debian`のライブUSBから起動して`GParted`でディスクを操作するとパーティションの縮小に失敗してできなかった。しかし、`Manjaro`のライブUSBから起動した場合では`GParted`からのディスク操作に成功し、その後のArch Linuxのマシンは無事起動した(その後も)

この違いは何なんだろうか?

#### rust-analyzerがヘンな挙動をする

Neovimの最新版のrust-analyzerで挙動がおかしくなった。

空のrustのソースコードファイル`hoge.rs`もしくは以下のような
`main()`の`{}`が空のソースコード

```Rust
fn main(){
}
```

を触っていると入力を受け付けず、もっさり動き、以下のエラーメッセージが出た。

```txt
rust_analyzer: -32603: Invalid offset LineCol { line: 1, col: 0 } (line index le ngth: 24)
```

このメッセージでググると以下のISSUを見つけた[ISSUE](https://github.com/rust-lang/rust-analyzer/issues/17289)

ここでは、結局rust-analyzerが悪いのではなくてNeovimのLSPのクライアント側が悪いのではないかという感じになっていた。

Lazyで管理しているプラグインのダウングレード、Masonでインストールしたrust-analyzerのダウングレード方法がわからなかったので、試しにNeovim自体を最新版の`0.1.0-5`から`0.9.5`にダウングレードした。

とりあえず、さっきの不具合はなくなった。
あまり良い方法ではないが、とりあえずこれで...

### 最近やっていること

- Ray tracing

今はBVHで停滞中

- kaleidoscope

パーサーあたり

### やろうとしていること

- POSIX threadの本をやる

- かんたんなMD simulationをRustで書く

- WebGPUのC++ののイントロをやる

### skkをneovimに導入した

skkをneovimに導入した。
まだ慣れない

普通のIMEをvimで使うとon/offが面倒
今の自分の理解ではIMEを直接動かすのではなくて、vimの上で別のimeを動かすイメージ

- skkの方法:
    - 今日は天気が晴れです。(Kyou ha Tenki ga HaRedesu.)
    - 我輩はタコである。理由は未だ無い。(Wagahai haTakoqdearu.Riyuu haMaDaNaI.)
    - 大丈夫？結婚する？(Daijoubu ? Kekkonn suru?)
    - Shiftは毎回使うのは小指が痛くなるので変りにセミコロン(';')使うことが出来る
    - 変換をやめるにはCtrl + gとすれば良い

### waybarの時計が変な時間を示すようになったので修正した

[redditのこのスレッドに](https://www.reddit.com/r/archlinux/comments/1fc5o2f/waybar_suddenly_has_the_wrong_time/)によると
`tzdata`パッケージのバージョンが新しくなったことが原因なようだった。
waybarの[GitHubのissue](https://github.com/Alexays/Waybar/issues/3024)でもダウングレードするように書いてあった。

そこでそのパッケージをダウングレードした。

```bash
$ wget https://archive.archlinux.org/packages/t/tzdata/tzdata-2024a-2-x86_64.pkg.tar.zst
$ sudo pacman -U *.pkg.tar.zst
```

その後でアップデートされないように`/etc/pacman.conf`に以下をつけ加える

```txt
[option]
IgnorePkg = tzdata
```

### fcitx5-skk

vimでskkを使い初めたので、システム全体の日本語入力でも使ってみようと考えました。
現在日本語入力としては`fcitx5`,`fcitx5-mozc`を使っているので、`fcitx5-skk`を使うことを考えました。
`fcitx5-skk`をpacmanで普通にインストールしたのですが、skkが動かず、Not availableと出ていました。

冷静に考えると、普通に`fcitx5-skk`をインストールすると依存関係として付いてくる`libskk`が悪そうです(fcitx5側にうまくアタッチできていないのかな？)。
どうやら[このツイート](https://x.com/CSSlayer/status/1833689273650098407)によるとバグがあるらしいです。
そこでこのツイートにあるように、代りに`libskk-git`をインストールしてから`fcitx5-skk`をインストールしたらうまくskkが動きました。

```bash
$ yay -S libskk-git
$ sudo pacman -S fcitx5-skk
```

また、毎回Shiftキーを押すのは小指がイカレそうになるので、代りに`;`キーで代用できるようにした。

このとき[ココ](https://neko-mac.blogspot.com/2021/06/archlinuxskk.html)を参考にした

Twitter(現X)の情報力はすごいですねえ...

### skkに絵文字を対応させる

usr/share/fcitx5/skk/dictionary_list
```txt
type=file,file=$FCITX_CONFIG_DIR/skk/user.dict,mode=readwrite
type=file,file=/usr/share/skk/SKK-JISYO.L,mode=readonly
type=file,file=/usr/share/skk-emoji-jisyo/SKK-JISYO.emoji.utf8,mode=readonly
type=file,file=/usr/share/skk-emoji-jisyo-ja/SKK-JISYO.emoji-ja.utf8,mode=readonly
```

ん~これだとうまくいかない！！

### Neovimのversion管理について

Neovimのversionが0.9.5以上では、Rustの空のソースコードを開くとエラーが出てうまく動かない！
これまでは、pacman経由でinstallしたものをdowngradeしていた。
msgpackなどの依存パッケージの関係からちょっとこの方法ではうまくいかなくなっていた。

[GitHubのissue](https://github.com/rust-lang/rust-analyzer/issues/17289)を見ると、以下のように結局ダウングレードする方法くらいしかないみたいで、
それようのツールとして[bob](https://github.com/MordechaiHadad/bob)があるみたいなので、それを使うことにした。

pacmanでinstallすると`/usr/bin`以下にinstallされるが、bobを使てinstallすると、`~/.local/share/bob`以下にinstallされる。

このディレクトリは普通PATHが通っていないので、自分で通す必要がある。

今回も、`0.9.5`をinstallしておいた。

### NerdFontsのinstall

AURから、`NerdFonts`をinstallすると`/`以下のディレクトリを圧迫するので、`~/.local/share/`にインストール方法に変えることにした。

参考:https://vwrs.github.io/font/2018/09/26/nerd-fonts/

リポジトリのclone

容量が大きいので結構時間がかかる

```bash
$ git clone --branch=master --depth 1 https://github.com/ryanoasis/nerd-fonts.git
$ cd nerd-fonts
$ ./install.sh
$ cd ..
$ rm -r nerd-fonts
```

フォントのデータは`~/.local/share/fonts`に`NerdFonts`としてインストールされる。

```bash
$ du -s ~/.local/share/fonts/NerdFonts
```

で調べてみると、大体8GiBほどであった。

### Dockerコンテナの中でLSPを動かす

[Zenn:Docker コンテナ上の RuboCop LSP を使って Ruby コードをフォーマットする](https://zenn.dev/socialplus/articles/fa0eac39bfa174)を読んでいたら割と簡単にLSPをコンテナ内で動かしつつホストのNeovimからコーディングできそうだな~
と感じたのでC/C++のLSPである`clangd`で試してみることにした。

ホストのinit.luaでclangdの設定を

```Lua
require("lspconfig").clangd.setup {
    cmd = {"docker","exec","-i","clangd_docker", "clangd","--log=verbose", "--background-index", "--clang-tidy",
    "--completion-style=detailed", "--header-insertion=iwyu",
    "--suggest-missing-includes", "--cross-file-rename"},
}
```

とする。

一方、コンテナはプロジェクトルートで

```bash
$ docker run -it --rm --name clangd_docker -v `pwd`:/my_project archlinux
```

と起動し、必要なclangをinstallする

```bash
# pacman -Syu
# pacman -S clang
```

その後でホストマシンのプロジェクトルートでnvimでC/C++のコーディングを行なえば、ローカル環境と同じように補完が効いた。

コンテナ内のコンパイラでビルドを行いたい場合は以下のようにすれば良い。

```bash
$ docker exec -i clangd_docker g++ -o my_project/foo my_project/foo.cpp
```

ここで、重要なのは、pwdをコンテナ側では`/my_project`としてマウントしていたのでファイル名の前にそれを置くことが必要なことだ。

### rust-analyzerをDockerコンテナ内で動かす -> 起動はするけど補完が効かない。

まず、clangdの時と同じようにホストのinit.luaを

```Lua
require("lspconfig").rust_analyzer.setup({
    cmd = {"docker", "exec", "-i", "rust_docker", "/root/.cargo/bin/rust-analyzer"},
    on_attach=on_attach,
    settings = {
        ["rust-analyzer"] = {
            imports = {
                granularity = {
                    group = "module",
                },
                prefix = "self",
            },
            cargo = {
                buildScripts = {
                    enable = true,
                },
            },
            procMacro = {
                enable = true
            },
        }
    },
    root_dir = require("lspconfig.util").find_git_ancestor
})
```

と設定する。


続いて、適当なRustのプロジェクトを作成してからそこで、コンテナを起動する。

```bash
$ cargo new hoge_project
$ cd hoge_project
$ docker run -it --rm --name rust_docker -v `pwd`:/my_project archlinux
```

コンテナ内に入ったらrustの環境を作る。

```bash
# pacman -Syu
# pacman -S base-devel
# curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
# export PATH=$PATH:/~/.cargo/bin
# rustup component add rust-analyzer
```

それで、ホストのNeovimからプロジェクトを開けばコンテナ側と通信してrust-analyzerを起動することができた(補完は効かなかった)。

```bash
$ nvim src/main.rs # rust-analyzerは起動するが補完は出来ず。
```

また後でチャレンジしてみるか~

### 逆に(?)コンテナ内にNeovim,rust-analyzer環境を構築してホストに置いたプロジェクトをマウントして動かすのは簡単

本来は、Dockerfileを書いてコンテナイメージを作るべきではあるが、今回はお試しなので直で作業する。

まず、hostで適当なプロジェクトを作成してから、そこの入り、コンテナを起動する(このプロジェクトのrootをマウントすること)

```bash
$ cargo new <project_name>
$ cd <project_name>
$ docker run -it --rm --name rust_docker -v `pwd`:/project archlinux
```

その後でコンテナ内で必要なパッケージをinstsallして設定をすればok

```bash
# pacman -Syu
# pacman -S base-devel git bob # 必要なパッケージ

# curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh # rustup
# export PATH=$PATH:$HOME/.cargo/bin
# rustup component add rust-src

# bob install 0.9.5
# bob use 0.9.5
# export PATH=$PATH:/root/.local/share/bob/nvim-bin/

# pwd #=> /root
# git clone https://github.com/aki-ph-chem/neovim-config.git # 自分のneovimのconfig file
# mkdir ~/.cofig
# ln -s /root/neovim-config/nvim ~/.config/
# nvim
```

初回のnvimの起動でlazy.nvimによって、プラグインがinstallされる。
その後に,nvimの中で :Mason を起動してそこからrust-analyzer選択してinstallすればok

### rust-analyzerをDockerコンテナ内で動かすリベンジ

最初に`docker run` するところでコンテナでマウントされる領域も`pwd`にしたらいけた！(Neovim側の設定は全く同じ)

```bash
$ docker pull rust # 今回はrustのイメージを使うことにする
$ caro new <project_name>
$ cd <project_name>
$ docker run -it --rm --name rust_docker -v `pwd`:`pwd` --workdir `pwd` rust
```

コンテナに入ったら`rust-analyzer`をinstallする。

```bash
# rustup component add rust-analyzer
```

最後にdockerでマウントしたプロジェクトをNeovimで開けばok!
```bash
$ nvim src/main.rs
```

ホストでのプロセスを見てみると確かに`docker exec`で`rust-analyzer`を起動していることが分る。
```bash
$ ps aux | grep docker
root        2509  0.1  0.4 2792056 76420 ?       Ssl  10:55   0:01 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
aki         9109  0.0  0.1 2139916 27232 pts/0   Sl+  11:09   0:00 docker run -it --rm --name rust_docker -v /home/aki/gomi/rust_docker:/home/aki/gomi/rust_docker --workdir /home/aki/gomi/rust_docker rust
aki         9345  0.0  0.1 2066184 25568 ?       Ssl  11:09   0:00 /usr/bin/docker exec -i rust_docker /usr/local/cargo/bin/rust-analyzer -v --log-file /rust-analyzer.log
aki        10714  0.0  0.0   7444  3948 pts/3    S+   11:18   0:00 grep --colour=auto docker
```

ちなみに、起動しているプロセスは上から、dockerのデーモン、今回起動したコンテナ、`docker exec`で起動している`rust-analyzer`である。

また、コンテナの中のプロセスを見てみると以下のように`rust-analyzer`が起動していることが分る。

```bash
root@909db868962f:/home/aki/gomi/rust_docker# ps aux
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root           1  0.0  0.0   4188  3320 pts/0    Ss   02:09   0:00 bash
root          43  0.4  2.7 3736184 443412 ?      Ssl  02:09   0:04 /usr/local/rustup/toolchains/1.82.0-x86_64-unknown-linux-gnu/bin/rust-analyzer -v --log-file /rust-analyz
root         136  0.0  0.1 243412 25808 ?        S    02:09   0:00 /usr/local/rustup/toolchains/1.82.0-x86_64-unknown-linux-gnu/libexec/rust-analyzer-proc-macro-srv
root         363  0.0  0.0   8104  4312 pts/0    R+   02:25   0:00 ps aux
```

### Python 3.13.0をソースコードからビルドする

Python3.13.0の新しい機能を試してみようと思ったので...

- ソースのアーカイブ: https://www.python.org/downloads/source/
- 公式のビルドガイド: https://devguide.python.org/getting-started/setup-building/

ソースコードを取得する。
```bash
$ wget https://www.python.org/ftp/python/3.13.0/Python-3.13.0.tgz
$ tar -xvf Python-3.13.0.tgz
$ cd Python-3.13.0
```

普通にやるならその場で`./configure && make`するがオプションを色々と変えてをビルドを試したいのでビルド用のディレクトリを作成することにした。

```bash
$ mkdir build
$ cd build
$ ../configure
$ make <some options>
```

今回はビルドのオプションを4種類用意して色々と比較してみることにした。

- case 1: 何のオプションもつけない

```bash
$ mkdir build_normal
$ cd build_normal
$ ../configure
$ make -j 4
```

- case 2: jitコンパイラのオプションだけ付ける。

```bash
$ mkdir build_jit
$ cd build_jit
$ ../configure --enable-experimental-jit=yes
$ make -j 4
```

- case 3: optimizationのオプションだけ付ける。

```bash
$ mkdir build_optimization
$ cd build_optimization
$ ../configure --enable-optimizations
$ make -j 4
```

- case 4: jitとoptimizationのオプションの両方を付ける

```bash
$ mkdir build_optimization_jit
$ cd build_optimization_jit
$ ../configure --enable-experimental-jit=yes --enable-optimizations
$ make -j 4
```

今回は16スレッドのラップトップ4種類のパターンを試したので、並列化はそれぞれ4スレッドじ実行することにした(make -j 4)。

まず、バイナリサイズを比較する。

- case1

```bash
$ du -BK ./python
31788K  ./python
```

- case2

```bash
$ du -BK ./python
32892K  ./python
```

- case3

```bash
$ du -BK python
29540K  python
```

- case4

```bash
$ du -BK ./python
30304K  ./python
```

この比較では、jitとoptimizationの両方を適用したものが一番小さなバイナリであった。

### journalctlでswayのログを確認した

swayがいきなりログアウトするアクシデントがあったので,クラッシュログを見てみることにした。

logを見るにはjournalctlコマンドを使う。

```bash
$ journalctl
```

と何もオプションを付けずに実行すると保存されている全てのログが表示される。`-b`オプションを付けて実行すると現在起動した際のセッションのログを表示する。

`i`回前に起動した時のセッションを見るには`-b -i`オプションを付ける。


なお、今回のswayのクラッシュログを見るとcore dumpを吐いていてどうやらgalliumすなわちGPUドライバが原因だったみたいだった。

一応最後にswayを起動しているスクリプトである`/etc/lemurs/wayland/sway`(login managerに[lemurs](https://github.com/coastalwhite/lemurs)を使っているため)を単に`sway`と書いてあるだけのものから、以下のようにログを記録するように書き変えた。

```bash
#! /bin/sh
exec sway -d >> ~/.sway.log 2>&1
```

### sway wm がクラッシュする件について

sway wm で作業しているとsway wm がクラッシュしてログイン画面に戻ってしまう現象が起るようになってしまった。

`journalctl`等で調査した所では自分が調べた限りではgpuドライバが悪いように思えた。そのときちょうどmesaのアップデートがあったがその後も一向にクラッシュ問題が解決しないのでカーネルをltsカーネルにすることにした。

```baash
$ sudo pacman -S linux-lts
$ sudo grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
$ sudo grub-mkconfig -o /boot/grub/grub.cfg
```

その後も何時間か動かしてみたが、クラッシュ問題は起らなかった。

### voicevoxを公式Dockerイメージで動かしてみる

- [dockerhub: voicevox/voicevox_engine](https://hub.docker.com/r/voicevox/voicevox_engine)
- [Zenn: DockerでVOICEVOX(音声合成)を動かす](https://zenn.dev/isi00141/articles/54c5c176f6ac1a)

voicevoxの公式Dockerイメージがあるので動かしてみる。

筆者のマシンにはNvidia製のGPUは搭載されていないのでcpu版のイメージを使う。

```bash
$ docker pull voicevox/voicevox_engine:cpu-ubuntu20.04-latest
```

適当なポートを共有するように`-p`オプションを使ってコンテナを起動する。

```bash
$ docker run --rm -d -p '127.0.0.1:50021:50021' voicevox/voicevox_engine:cpu-ubuntu20.04-latest
```

続いて喋らせる用の適当な日本語のテキストをtxtに書きこみ用意する。

```txt
僕はずんだの妖精ずんだもんなのだ
```

まず、クエリ用のJSONファイルを生成させる。

```bash
$ curl -s \
    -X POST \
    "localhost:50021/audio_query?speaker=1"\
    --get --data-urlencode text@txt \
    > query.json
```

続いて、先程生成したJSONを用いて以下のコマンドでWAVファイルを生成させる。
```bash
$ curl -s \
    -H "Content-Type: application/json" \
    -X POST \
    -d @query.json \
    "localhost:50021/synthesis?speaker=1" \
    > audio.wav
```

WAVファイルは適当なメディアプレイヤーで再生すれば音声が再生される。

### sway wmのクラッシュ問題

昨日一日の運用ではクラッシュしなかった。なんとなく、weztermが悪いような気もする。
というのも昨日は基本的にxfce4-terminalで作業していたから...

```bash
sway -D noatomic
```

で安定するらしい。[参考: Issues with most Wayland compositors; Sway crashes "at random".](https://www.reddit.com/r/swaywm/comments/1croo12/issues_with_most_wayland_compositors_sway_crashes/)

sway wmのソースコードのmain.cを見てみると extended debug optionsでの起動らしい。

### nvim-lspconfigを使わずに直接ビルドインなlspクライアントから言語サーバーを呼ぶ

clangdを例に

最低限動かすのに必要な設定は以下
```Lua
vim.api.nvim_create_autocmd("FileType", {
	pattern = {"c", "cpp"},
	callback = function()
		vim.lsp.start({
			name = "clangd",
			cmd = {
				"/usr/bin/clangd",
			},
			capabilities = vim.lsp.protocol.make_client_capabilities(),
		})
	end,
})
```

`compile_commands.json`などのローカルなコンフィグも使いたい場合は以下のように`root_dir`も設定する
```Lua
vim.api.nvim_create_autocmd("FileType", {
	pattern = {"c", "cpp"},
	callback = function()
		vim.lsp.start({
			name = "clangd",
			cmd = {
				"/usr/bin/clangd",
			},
            root_dir = vim.fs.dirname(vim.fs.find({ ".clangd", "compile_commands.json", ".git" }, { upward = true })[1]),
			capabilities = vim.lsp.protocol.make_client_capabilities(),
		})
	end,
})
```

### ハイバネーションの設定

- [Arch wiki: 電源管理/サスペンドとハイバネート](https://wiki.archlinux.jp/index.php/%E9%9B%BB%E6%BA%90%E7%AE%A1%E7%90%86/%E3%82%B5%E3%82%B9%E3%83%9A%E3%83%B3%E3%83%89%E3%81%A8%E3%83%8F%E3%82%A4%E3%83%90%E3%83%8D%E3%83%BC%E3%83%88#.E3.83.8F.E3.82.A4.E3.83.90.E3.83.8D.E3.83.BC.E3.82.B7.E3.83.A7.E3.83.B3)

スワップ領域に現在のRAMの内容を退避させることで次回の起動が速くなる仕組みで、普通のbootとsuspendとの間のような操作。

`/etc/mkinitcpio.conf`を編集する

以下のように`resume`をHOOKSに追加する

```txt
HOOKS=(base udev autodetect microcode modconf kms keyboard keymap consolefont block filesystems fsck)
```

↓

```txt
HOOKS=(base udev autodetect microcode modconf kms keyboard keymap consolefont block filesystems resume fsck)
```

編集後は

```bash
$ sudo mkinitcpio -P
```

によって、`initramfs`の再生成をする。

とりあえずArch Wiki の通りに設定してみたけど

```bash
$ systemctl hibernate
```

としてもハイバーネートされない。

また後で見直す。

### CLIの論文管理ツール

論文管理ツールと言えば[Mendeley]()のようなGUIなものが代表的であるが、CLIで作業できるものはないのだろうか？
と探してみると以下が見つかった。

- [pubs](https://github.com/pubs/pubs)
- [papis](https://github.com/papis/papis)
- [scholar](https://github.com/cgxeiji/scholar)

上二つは、Python製で残る一つはGo製。

リポジトリを見ると、papisのみ現在でも盛んに開発が進められていて、Vim/Neovim向けのプラグインもあったりとコミュニティーが
盛り上っている様子が伺える。

pubsはわりと昔から知っていて現在使用しているが、papisも試してみようと思う。

### systemd-bootでarch-linuxをbootする


ブートローダーをinstallする前まではgrubで起動する構成をとる時と同じ。

systemdで動くarch linuxの場合installは

```bash
# bootctl install
```

systemdで自動で更新されるようにするために以下の設定を行なう

```bash
# systemctl enable systemd-boot-update.service
```

更に`/boot/loader/loader.conf`に

```txt
default  arch.conf
timeout  4
console-mode max
editor   no
```

を追記する。

最後に`/boot/loader/entries/`にブートメニューのアイテムを追加する

`/boot/loader/entries/arch.conf`
```txt
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options root=UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx rw
```

ここで、optionsのUUIDの値は`/`のパーテーションのUUIDを入力すること。

### typing中はトラックパッドを無効化できるようにした

Laptopのマシンで作業しているとタイピング中にトラックパッドに触れてしまいカーソルを変な場所へ飛してしまうことがある。
そこで、タイピング中はトラックパッドを無効化できれば便利だと思ったので調べてみることにした。

- 参考
    - [GitHub: sway/sway wiki (2024 11/08 13:00)](https://github.com/swaywm/sway/wiki)
    - [Reddit:r/swaywm: How do I enable the touchpad while typing? (2024 11/08 13:00)](https://www.reddit.com/r/swaywm/comments/n4uwsx/how_do_i_enable_the_touchpad_while_typing/)

inputデバイスのことなので

```bash
$ man 5 sway-input
```

を見て`disable`や`disable-while`で探してみると

```txt
input <identifier> dwt enabled|disabled
    Enables or disables disable-while-trackpointing for the specified input device.
```

が見つかったのでその通りに設定をしてみる。

~/.config/sway/config
```txt
input type:touchpad {
    tap enabled
    natural_scroll enabled
    dwt enable disable-while-typing
}
```

Super + Shift + Cで再読み込みするとタイピング中はトラックパッドが反応しないようになった。

### Zolaでブログを作ろうと思っている件

static site generator Zolaでブログを作ってGitHub Pagesもしくは、GitLab Pagesでホストしてみようと考えている。

以下のサイトが役に立ちそう。

- [Zolaで始める技術ブログ- shimopion\`s blog](https://shimopino.github.io/blog/crafting-tech-blog-with-zola/)
- [Zenn: Zolaでブログを作る](https://zenn.dev/natsuka_sili/articles/5455694806017e)
- [Zenn: zolaでブログサイトを作成しGitHub Pagesにデプロイする](https://zenn.dev/st_little/articles/create-blog-site-with-zola)

### GitHub Actionsでpoetry install してから poetry buildするCIを組む(1)

Python環境のsetupには[actions/setup-python](https://github.com/actions/setup-python)を使い、poetryのsetupには[abatilo/actions-poetry](https://github.com/marketplace/actions/python-poetry-action)を使い、それぞれのversionの指定は`with:`のセクションで指定する。

ビルド済みのパッケージをreleaseに持っていくflowは後で書く。

```YAML
name: build_and_release

on:
  push:
    branches:
      - main

jobs:
  build:
    runs-on: ubuntu-latest
    steps:
      - name: checkout repositry
        uses: actions/checkout@v4

      - name: setup Python environment
        uses: actions/setup-python@v5
        with:
          python-version: '3.12'

      - name: setup poetry environment
        uses: abatilo/actions-poetry@v2
        with:
          poetry-version: '1.8.2'

      - name: install
        run: poetry install

      - name: build package
        run: poetry build
```

### GitHub Actionsでpoetry install してから poetry buildするCIを組む(2)

前回残したアーティファクトファイルをreleaseにuploadするまでの処理を追加する。

- `on`で指定するトリガに`workflow_dispatch`を追加して手動でトリガできるようにした。
- ビルドしたパッケージはプロジェクト中の`dist/`の中に生成されるので、そのファイルをuploadして次のjobである`deploy`に繋げる処理を追加した。
- パッケージをuploadするjobである`deploy`を追加した
```YAML
name: build_and_release

on:
  workflow_dispatch:
  push:
    branches:
      - main

jobs:

  build:
    runs-on: ubuntu-latest
    steps:
      - name: checkout repositry
        uses: actions/checkout@v4

      - name: setup Python environment
        uses: actions/setup-python@v5
        with:
          python-version: '3.12'

      - name: setup poetry environment
        uses: abatilo/actions-poetry@v2
        with:
          poetry-version: '1.8.2'

      - name: install
        run: poetry install

      - name: build package
        run: poetry build

      - name: upload artifact
        uses: actions/upload-artifact@v4
        with:
          name: my-artifact
          path: dist/


  deploy:
    runs-on: ubuntu-latest
    needs: build

    permissions:
      contents: write

    steps:

      - uses: actions/checkout@v4
      - name: dwonload artifact file
        uses: actions/download-artifact@v4
        with:
          name: my-artifact
          path: dist/

      - name: deploy to Gihub Release
        run: gh release upload v0.1.0 dist/* --clobber
        env:
          GH_TOKEN: ${{ secrets.GITHUB_TOKEN }}
```

### とりあえずでpapisを導入する

とりあえずGitHubのリポジトリにあるチュートリアルをやってみる。

ともあれまずはinstall
```bash
$ pipx install papis
```

適当な論文をdownload & 登録
```bash
$ wget https://www.gnu.org/s/libc/manual/pdf/libc.pdf
$ wget https://www.ams.org/notices/201304/rnoti-p434.pdf
```

doiから登録

```bash
$ papis add --from doi 10.1090/noti963 rnoti-p434.pdf
```

大量のエラーが出た😭

```txt
Error while loading entrypoint 'EntryPoint(name='isbn', value='papis.isbn:Importer', group='papis.importer')': No module named 'pkg_resources'.
Error while loading entrypoint 'EntryPoint(name='isbn', value='papis.isbn:explorer', group='papis.explorer')': No module named 'pkg_resources'.
[WARNING] commands.default: No configuration file exists at '/home/aki/.config/papis/config'.
[WARNING] commands.default: Create a configuration file and define your libraries before using papis. You can use 'papis init /path/to/my/library' for a quick interactive setup.
[INFO] utils: Merging data from importer 'doi'.
[INFO] commands.add: Created reference 'What_Is_New_in_Gratze_2013'.
Traceback (most recent call last):
  File "/home/aki/.local/bin/papis", line 8, in <module>
    sys.exit(run())
             ^^^^^
  File "/home/aki/.local/share/pipx/venvs/papis/lib/python3.12/site-packages/click/core.py", line 1157, in __call__
    return self.main(*args, **kwargs)
           ^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "/home/aki/.local/share/pipx/venvs/papis/lib/python3.12/site-packages/click/core.py", line 1078, in main
    rv = self.invoke(ctx)
         ^^^^^^^^^^^^^^^^
  File "/home/aki/.local/share/pipx/venvs/papis/lib/python3.12/site-packages/click/core.py", line 1688, in invoke
    return _process_result(sub_ctx.command.invoke(sub_ctx))
                           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "/home/aki/.local/share/pipx/venvs/papis/lib/python3.12/site-packages/click/core.py", line 1434, in invoke
    return ctx.invoke(self.callback, **ctx.params)
           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "/home/aki/.local/share/pipx/venvs/papis/lib/python3.12/site-packages/click/core.py", line 783, in invoke
    return __callback(*args, **kwargs)
           ^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "/home/aki/.local/share/pipx/venvs/papis/lib/python3.12/site-packages/papis/commands/add.py", line 565, in cli
    run(ctx.files,
  File "/home/aki/.local/share/pipx/venvs/papis/lib/python3.12/site-packages/papis/commands/add.py", line 278, in run
    base_path = os.path.expanduser(papis.config.get_lib_dirs()[0])
                                   ~~~~~~~~~~~~~~~~~~~~~~~~~~~^^^
IndexError: list index out of range
```

[Quick guide](https://papis.readthedocs.io/en/latest/quick_guide.html)によると、どうやら最初は `papis init`でライブラリを作成する必要があるらしいので(0.14で導入されたらしい)
以下を実行する

```bash
$ mkdir papis_tutorial
$ cd papis_tutorial
$ papis init
```

気をとり直してdoiからの登録
```bash
$ papis add --from doi 10.1090/noti963 rnoti-p434.pdf
Error while loading entrypoint 'EntryPoint(name='isbn', value='papis.isbn:Importer', group='papis.importer')': No module named 'pkg_resources'.
Error while loading entrypoint 'EntryPoint(name='isbn', value='papis.isbn:explorer', group='papis.explorer')': No module named 'pkg_resources'.
[INFO] utils: Merging data from importer 'doi'.
[INFO] database.cache: Indexing library. This might take a while...
[INFO] commands.add: Created reference 'What_Is_New_in_Gratze_2013'.
[INFO] commands.add: [CP] 'rnoti-p434.pdf' to 'rnoti-p434.pdf'.
[INFO] paths: Falling back to 'papis_id' as a reference folder name.
[INFO] commands.add: Document folder is '/home/aki/References/papis_tutorial/2caa7b96baa0ce19e47e4f9253b258fb'.
[INFO] commands.add: Checking if this document is already in the library. This uses the keys ['doi', 'isbn', 'isbn10', 'eprint', 'url', 'doc_url'] to determine uniqueness.
[INFO] commands.add: No document matching the new metadata found in the 'my_1st_library' library.
[INFO] commands.add: [MV] '/tmp/tmph_ofgh0m' to '/home/aki/References/papis_tutorial/2caa7b96baa0ce19e47e4f9253b258fb'.
```

手動で登録
```bash
$ papis add libc.pdf --set author "Sandra Loosemore" \
                   --set title "GNU C reference manual" \
                   --set year 2018 \
                   --set tags programming \
                   --confirmv
papis add libc.pdf --set author "Sandra Loosemore" \
                   --set title "GNU C reference manual" \
                   --set year 2018 \
                   --set tags programming \
                   --confirm
Error while loading entrypoint 'EntryPoint(name='isbn', value='papis.isbn:Importer', group='papis.importer')': No module named 'pkg_resources'.
Error while loading entrypoint 'EntryPoint(name='isbn', value='papis.isbn:explorer', group='papis.explorer')': No module named 'pkg_resources'.
[INFO] commands.add: Created reference 'GNU_C_reference_Sandra_2018'.
[INFO] commands.add: [CP] 'libc.pdf' to 'libc.pdf'.
[INFO] paths: Falling back to 'papis_id' as a reference folder name.
[INFO] commands.add: Document folder is '/home/aki/References/papis_tutorial/2ab887b6ce8ca8a24ec01a36219a7303'.
[INFO] commands.add: Checking if this document is already in the library. This uses the keys ['doi', 'isbn', 'isbn10', 'eprint', 'url', 'doc_url'] to determine uniqueness.
[INFO] commands.add: No document matching the new metadata found in the 'my_1st_library' library.
┌───────────────────────────────────────────────| This document will be added to your library |───────────────────────────────────────────────┐
│author: Sandra Loosemore                                                                                                                     │
│files:                                                                                                                                       │
│- libc.pdf                                                                                                                                   │
│ref: GNU_C_reference_Sandra_2018                                                                                                             │
│tags: programming                                                                                                                            │
│time-added: 2024-11-11-11:35:02                                                                                                              │
│title: GNU C reference manual                                                                                                                │
│year: '2018'                                                                                                                                 │
└─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
Do you want to add the new document? (Y/n): y
[INFO] commands.add: [MV] '/tmp/tmpmkqhseko' to '/home/aki/References/papis_tutorial/2ab887b6ce8ca8a24ec01a36219a7303'.
```

- ざっくりとした感想：
    - pubsとは結構設計思想が違う
    - pubsは`~/.pubs`に`bib`,`doc`,`meta`でbibtex、pdfファイル、メタデータを分けて論文ごと分けずに入れている
    - 一方、papisは論文ごとにディレクトリが作成されてそこにメタデータ、pdfファイルが置かれる

pubsからの移行はスクリプトを後で書く(手で論文登録はメンドウだし...)


どちらにせよこのエラーが出る

```txt
Error while loading entrypoint 'EntryPoint(name='isbn', value='papis.isbn:Importer', group='papis.importer')': No module named 'pkg_resources'.
Error while loading entrypoint 'EntryPoint(name='isbn', value='papis.isbn:explorer', group='papis.explorer')': No module named 'pkg_resources'.
```

pdbでトレースしてみたところ(python -m pdbだと仮想環境を認識しない)

```bash
$ /home/aki/.local/share/pipx/venvs/papis/bin/python -m pdb $(which papis)
```

エラーは再現して、エントリーポイント

```bash
$ nvim $(which papis)
```
ソースコードの該当個所はimportをしているところだった。

```bash
#!/home/aki/.local/share/pipx/venvs/papis/bin/python
# -*- coding: utf-8 -*-
import re
import sys
from papis.commands.default import run # <- ここで上記のエラーが出る(pdbで分った)
if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(run())
```

pipxが悪いのか？と思ってpoetryで仮想環境を構築してそこの中で動かしてみる。

仮想環境の構築
```bash
$ mkdir papis_poetry
$ cd papis_poetry
$ poetty init
$ poetry config --local virtualenvs.in-project true
```

papisをinstallして動かす
```bash
$ poetry add papis # install
$ poetry run papis --version
Error while loading entrypoint 'EntryPoint(name='isbn', value='papis.isbn:Importer', group='papis.importer')': No module named 'pkg_resources'.
Error while loading entrypoint 'EntryPoint(name='isbn', value='papis.isbn:explorer', group='papis.explorer')': No module named 'pkg_resources'.
papis, version 0.14
```

一応動くがimportのエラーは相変らず出る。
