### Python製のテンプレートジェネレーター: cookiecutter

よく使う雛形のコードを毎回どこかしらからコピペするのは面倒だなーと思っていたら`cookiecutter`をいうツールがあるらしい。

install
```bash
$ pipx install cookiecutter
```

基本的のこのツールはプロジェクトのrootに置いた`cookiecutter.json`に書いたルールにしたがってプロジェクト内のディレクトリ名、ファイル名、ファイルの中身のテキスト中の`{{}}`で囲われた部分を置換した物を生成するツールである。

簡単な例として、Pythonのスクリプトのテンプレを作る例を挙げる。


まずテンプレートを収めるディレクトリを作成し、rootに`cookiecutter.json`を置く。
```bash
$ mkdir hoge_template
$ vim cookiecutter.json
```

`cookiecutter.json`には以下を書く
```JSON
{

    "project_name": "hoge_project",
    "author_name": "aki"
}
```

続いてディレクトリ`{{cookiecutter.project_name}}`を作成し、そこにfoo.pyを置く。
```bash
$ mkdir {{cookiecutter.project_name}}
$ vim {{cookiecutter.project_name}}/foo.py
```

foo.pyには簡単に以下を実装する。
```Python
'''
Author: {{cookiecutter.author_name}}
Project: {{cookiecutter.project_name}}
'''
def main() -> None:
    print("Hi!")

if __name__ == "__main__":
    main()
```
これで、テンプレートの作成はok


テンプレートを使うときは
```bash
$ cookiecutter <path_to_template>
```

とするとプロジェクト名や作者名を聞かれるので入力する(何も入れないでEnterするとそのまま)とテンプレートを元にたプロジェクトが作成される。

またテンプレートをGitHub,GitLabに置いておいてそこへのurlを指定することでもプロジェクト生成が可能である。

### Emscriptenのプロジェクトのsetup

簡単なCのプロジェクトをCMakeでビルドするsetup。
ここではごく簡単な以下のコード`fib.c`をコンパイルする例を考える。

fib.c
```C
#include <emscripten/emscripten.h>

int main(void) {
    return 0;
}

EMSCRIPTEN_KEEPALIVE int fib(int n) {
    if(n == 0 || n == 1) {
        return 1;
    }

    return fib(n - 1) + fib(n - 2);
}
```

とりあえずビルドを実行するだけならSDKについてくる環境変数セットスクリプトを実行してから

```bash
$ emcc -o fib.wasm fib.c
```

とさえすれば良いが、そのままでは`<emscripten/emscripten.h>`等のヘッダファイルやSDKが提供するAPI周りがclangd(LSP)から出たエラーまみれになってしまうし、
そもそもコード補完が効かないのでCMakeから`compile_commands.json`を生成しclangdにSDKのコードの情報を与えるようにしたい。


プロジェクトのディレクトリ構成
```txt
.
├── CMakeLists.txt
└── src
    └── fib.c
```

CMakeLists.txtにコンパイラへのパスと、ヘッダファイルへのパスを与えたいので以下のように調査を行なった。

コンパイラへのパスは
```bash
$ which emcc
/home/aki/emsdk/upstream/emscripten/emcc
```
で簡単に取得できた。

問題はヘッダファイルへのパスであるが、emccで直接のビルドは可能だったので`-v`でコンパイラがincludeパスを決定する過程を見て決定されたパスをCMakeに書けば良いと考えたので以下を実行した。

```bash
$ emcc -v -o fib.wasm fib.c
 /home/aki/emsdk/upstream/bin/clang -target wasm32-unknown-emscripten -fignore-exceptions -mno-bulk-memory -mno-nontrapping-fptoint -mllvm -combiner-global-alias-analysis=false -mllvm -enable-emscripten-sjlj -mllvm -disable-lsr --sysroot=/home/aki/emsdk/upstream/emscripten/cache/sysroot -DEMSCRIPTEN -Werror=implicit-function-declaration -Xclang -iwithsysroot/include/fakesdl -Xclang -iwithsysroot/include/compat -v fib.c -c -o /tmp/emscripten_temp_eosrbgfp/fib_0.o
clang version 20.0.0git (https:/github.com/llvm/llvm-project d6344c1cd0d099f8d99ee320f33fc9254dbe8288)
Target: wasm32-unknown-emscripten
Thread model: posix
InstalledDir: /home/aki/emsdk/upstream/bin
 (in-process)
 "/home/aki/emsdk/upstream/bin/clang-20" -cc1 -triple wasm32-unknown-emscripten -emit-obj -disable-free -clear-ast-before-backend -disable-llvm-verifier -discard-value-names -main-file-name fib.c -mrelocation-model static -mframe-pointer=none -ffp-contract=on -fno-rounding-math -mconstructor-aliases -target-cpu generic -target-feature -bulk-memory -target-feature -nontrapping-fptoint -fvisibility=hidden -debugger-tuning=gdb -fdebug-compilation-dir=/home/aki/wasm_handson/hoge/src -v -fcoverage-compilation-dir=/home/aki/wasm_handson/hoge/src -resource-dir /home/aki/emsdk/upstream/lib/clang/20 -D EMSCRIPTEN -isysroot /home/aki/emsdk/upstream/emscripten/cache/sysroot -internal-isystem /home/aki/emsdk/upstream/lib/clang/20/include -internal-isystem /home/aki/emsdk/upstream/emscripten/cache/sysroot/include/wasm32-emscripten -internal-isystem /home/aki/emsdk/upstream/emscripten/cache/sysroot/include -Werror=implicit-function-declaration -ferror-limit 19 -fgnuc-version=4.2.1 -fskip-odr-check-in-gmf -fignore-exceptions -fcolor-diagnostics -iwithsysroot/include/fakesdl -iwithsysroot/include/compat -mllvm -combiner-global-alias-analysis=false -mllvm -enable-emscripten-sjlj -mllvm -disable-lsr -o /tmp/emscripten_temp_eosrbgfp/fib_0.o -x c fib.c
clang -cc1 version 20.0.0git based upon LLVM 20.0.0git default target x86_64-unknown-linux-gnu
ignoring nonexistent directory "/home/aki/emsdk/upstream/emscripten/cache/sysroot/include/wasm32-emscripten"
#include "..." search starts here:
#include <...> search starts here:
 /home/aki/emsdk/upstream/emscripten/cache/sysroot/include/fakesdl
 /home/aki/emsdk/upstream/emscripten/cache/sysroot/include/compat
 /home/aki/emsdk/upstream/lib/clang/20/include
 /home/aki/emsdk/upstream/emscripten/cache/sysroot/include
End of search list.
 /home/aki/emsdk/upstream/bin/clang --version
 /home/aki/emsdk/upstream/bin/wasm-ld -o fib.wasm -L/home/aki/emsdk/upstream/emscripten/cache/sysroot/lib/wasm32-emscripten /tmp/emscripten_temp_eosrbgfp/fib_0.o /home/aki/emsdk/upstream/emscripten/cache/sysroot/lib/wasm32-emscripten/crt1.o -lGL-getprocaddr -lal -lhtml5 -lstandalonewasm-nocatch -lstubs-debug -lc-debug -ldlmalloc -lcompiler_rt -lc++-noexcept -lc++abi-debug-noexcept -lsockets -mllvm -combiner-global-alias-analysis=false -mllvm -enable-emscripten-sjlj -mllvm -disable-lsr /tmp/tmpwrsushaplibemscripten_js_symbols.so --strip-debug --export=emscripten_stack_get_end --export=emscripten_stack_get_free --export=emscripten_stack_get_base --export=emscripten_stack_get_current --export=emscripten_stack_init --export=_emscripten_stack_restore --export-if-defined=__start_em_asm --export-if-defined=__stop_em_asm --export-if-defined=__start_em_lib_deps --export-if-defined=__stop_em_lib_deps --export-if-defined=__start_em_js --export-if-defined=__stop_em_js --export-table -z stack-size=65536 --no-growable-memory --initial-heap=16777216 --stack-first --table-base=1
 /home/aki/emsdk/upstream/bin/llvm-objcopy fib.wasm fib.wasm --remove-section=.debug* --remove-section=producers
 /home/aki/emsdk/upstream/bin/wasm-emscripten-finalize --dyncalls-i64 --no-legalize-javascript-ffi --standalone-wasm fib.wasm -o fib.wasm --detect-features
```

結果includeパスを`/home/aki/emsdk/upstream/emscripten/cache/sysroot/include`と決定した。

これらを踏まえてCMakeを以下のように書いた。
```CMakeLists.txt
cmake_minimum_required(VERSION 3.13)
project(Emscripten C)

# Enable generation of compile_commands.json
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Set Emscripten C compiler (emcc)
set(CMAKE_C_COMPILER /home/aki/emsdk/upstream/emscripten/emcc)
set(CMAKE_C_FLAGS "-o")

# add path to header
include_directories(/home/aki/emsdk/upstream/emscripten/cache/sysroot/include)

# Specify source files
add_executable(fib.wasm src/fib.c)
```

後はいつも通りの方法でok

```bash
$ mkdir build_cmake && cd build_cmake
$ cmake ..
$ cd ..
$ ln -s build_cmake/compile_commands.json .
$ cd build_cmake
$ make
```

### RSSフィードの購読設定

RSSを購読する方法のメモ

- 参考
    - [blog: 今こそRSSでサイトやブログを購読する方法(2024 11/13 13:00)](https://libsy.net/blog/3547)

- URLを取得する方法
    - サイト中のRSSマーク探してそこクリックしてURLを取得する
    - wordpress製のブログの場合はそのサイトのURLの末尾に`/feed`と追加することでRSSへのURLを得る
    - FC2ブログURLの.comの次に`/?xml`を追加したURL
    - はてな[更新フィード](https://help.hatenablog.com/entry/feed)

### privateリポジトリのreleaseにあるアセットをGitHub CLIでdownloadする

```bash
$ gh release download <tag name> --repo <user name>/<reposityr name> -p <file name>
```

### PGOPHERで帰属を周波数で出力

以下はQ枝の$K_a = 1$ の帰属についての例

```bash
cat *35.pgo | grep '[pqr]Q1'  |sort -s -n -k 10
```

`1`の部分を変えれば他の量子数でも対応可能だし、`Q`の部分を`P`,`R`にすれば他の枝でも対応可能。

### ディレクトリの暗号化を検討中

セキュリティ的に怖いとかそういう理由よりも技術的な興味からLinuxマシンのディスクを暗号化してみようと考えている。

現在検討しているのは方式は[fscrypt](https://wiki.archlinux.jp/index.php/Fscrypt)でこの方法ではディレクトリごとにで暗号化が可能でpamを使ってログイン時に復号する等が出来るらしい。

また他には[Yubikey](https://wiki.archlinux.jp/index.php/YubiKey)を使った二段階認証とかも興味がある。

### Bluetoothの導入

- 参考
    - [Arch wiki: Bluetooth](https://wiki.archlinux.jp/index.php/Bluetooth#.E3.82.A4.E3.83.B3.E3.82.B9.E3.83.88.E3.83.BC.E3.83.AB)

- 必要なパッケージ
    - bluez
    - bluez-utils

```bash
$ sudo pacman -S bluez bluez-utils
```

`btusb`カーネルモジュールが読み込まれている必要があるので`dmesg`で調べる。

```bash
$ sudo dmesg | grep  'btsub'
```

`bluetooth.service`を有効化&起動する

```bash
$ sudo systemctl enable bluetooth.service
$ sudo systemctl start bluetooth.service
```

ちゃんとサービスが動いているかをチェック

```bash
$ systemctl status
```

これで、ペアリングするための準備が完了したので以下の手順で進める。

```bash
$ bluetoothctl # bluetoothctlを起動
#[bluetooth] # こんなプロンプトが表示されてインタラクティブモードになる
#[bluetooth] power on #デバイスがoffになっている場合必要
#[bluetooth] scan on #scanを開始
#[bluetooth] pair <MAC Address>
#[bluetooth] trust <MAC Address>
#[bluetooth] connect <MAC Address>
```

Bluetoothのセットアップをしてから上記の手順で接続を試みると接続まではいけたが、マウス(M575)は動かなかった(再起動後では接続できた)。

### fscryptを試す (1)

- 参考
    - [Arch Wiki: fscrypt](https://wiki.archlinux.jp/index.php/Fscrypt)

いきなり実機は怖いのでとりあえずはVirtual Box上のArch Linux のVMで

`/dev/sda2`内のディレクトリを暗号化したいので、そこに対して`encrypt`フラグを有効化する。
```bash
$ sudo tune2fs -O encrypt /dev/sda2
```

ここからの作業に必要なパッケージ`fscrypt`をinstallする。

```bash
$ sudo pacman -S fscrypt
```

まず以下のコマンド実行してsetupを行う。

```bash
$ sudo fscrypt setup
```

このコマンドによって`/etc/fscrypt.conf`と`/.fscrypt`が生成される。

このときに、`/`はsetupされる。しかし、それ以外のパーティションを対象にする場合は`fscrypt setup`を特定のマウンティングポイントに対して実行する。

```bash
$ sudo fscrypt setup <mountpoint>
```

空のディレクトリ`fuga`を作って暗号化してみる。

```bash
$ mkdir fuga
$ fscrypt encrypt fuga
```

`fscrypt encrypt`を実行するとパスワードをどうするかと聞かれる。
候補としては、1. userのlogin passwordで2. はカスタムpasswordが選択可能である。

これでlockする準備ができたのでまずlockしてみる

```bash
$ fscrypt lock fuga
```

暗号化してディレクトリの中身をvimで見てみると確かに暗号化されていた。

lock解除してみる。

```bash
$ fscrypt unlock fuga
```

設定した設定したpasswordを求められるので入力するすると解除される。

暗号化したディレクトリは`fscrypt status`で状態を確認できる。

```bash
$ fscrypt status fuga
```

また現在の状態では再起動するとlockされた状態に戻っている。

### fscryptを試す (2)

ここまでやり方では再起動するたびにlockされる。
暗号化の設定を行う際にuser passwordでlockする方式を選択した場合では `PAM`を利用することでログインしたときにunlockするよう設定することができる。

筆者は最初カスタムpassphraseを選択した場合でもokかと思ったがそれは不可能である(そもそもそれができるならカスタムpassphraseを選択する意味がなように思える)。

これを行うには以下のように`/etc/pam.d/system-login`を編集して `PAM`の構成を行なっていく。

まずのauthのあるセクションの次に以下を追加する

```txt
auth       optional   pam_fscrypt.so
```

次に`session`セクションの`session include system-auth`の前に次の行を挿入する。

```txt
session    [success=1 default=ignore]  pam_succeed_if.so  service = systemd-user quiet
session    optional                    pam_fscrypt.so
```

最後に`/etc/pam.d/passwd`を編集して最終行に以下を追加する

```txt
password    optional    pam_fscrypt.so
```

試しに`fuga_user_passwd`ディレクトリをuserのログインパスワードで暗号化して、lockが掛かった状態から再起動すると確かにunlockされた状態になっていた。

```bash
$ mkdir fuga_user_passwd
$ fscrypt encrypt fuga_user_passwd # 1の 'Your login passphrase (pam_passphrase)'を選択する
$ fscrypt lock fuga_user_passwd
$ reboot
$ fscrypt status fuga_user_passwd | grep Unlocked
# => Unlocked: Yes
```

とりあえず、Arch Wikiを見ながら設定をすることはできたが`PAM`のことは一切理解していないので何かドキュメントを読んで理解を深めたい。

### Emscriptenのビルド問題

CMakeを使ったビルドの問題であるが、EmscriptenのヘッダのPATHを設定しないと`#include <emscripten.h>`とするとclangdがエラーを出す。
一方,設定すると`cstdlib`,`cstring`等のヘッダを認識できなくなる。

どうすれば良い？

- Emscripten & CMakeの問題解決の参考になりそうなサイト
    - [Emscripten with CMake](https://stunlock.gg/posts/emscripten_with_cmake/)

```bash
$ em++ -v -o foo.js foo.cpp
```

で探索されるpathを全てCMakeに設定するとちゃんと補完されるようになった。


現状ではemcc/em++へのpathをハードコードしているけど`emcmake`を使って

```bash
$ emcmake cmake ..
```

した方が良さそう。

### duコマンドでファイルの容量一覧を容量で逆ソートする

```bash
$ du -BM . |awk '{gsub(/M$/, "", $1); print $1, $2}' | sort -r -n -k 1 | less
```

### Zolaで静的なサイトを構築する (1)

[Zolaで始める技術ブログ- shimopion\`s blog](https://shimopino.github.io/blog/crafting-tech-blog-with-zola/)のやり方にそってやってみる。

```bash
$ zola init myblog # setup
$ zola build # weolcomeの画面が作られる
```

生成されたHTMLは`public`にあるのでそのディレクトリでサーバーを立てればwelcomの画面が生成される

```bash
$ python http.server 8080
```

ブラウザで`localhost:8080/public`にアクセスする。

### fzfの設定

- 参考
    - [GitHub: junegunn/fzf](https://github.com/junegunn/fzf)
    - [Qiita: fzfを活用してTerminalの作業効率を高める](https://qiita.com/kamykn/items/aa9920f07487559c0c7e)

install

```bash
$ sudo pacman -S fzf
```

まずkey-bindingsを`~/.bashrc`に設定する

```bash
# fuzzy finder
eval "$(fzf --bash)"
```

とりあえずこの設定で`Ctrl + r`のコマンド履歴探索がfzfの画面でできるようになる。

### PGOPHERのCLIバージョンをPythonから呼ぶ

PGOPHERでスペクトルシミュレーションを行なってそのスペクトルを出力してPythonのmatplotlibでplotして図を作成している。
しかし、PGOPHERの\*.pgoファイルとそのシミュレーションから生成したCSVファイルを両方ともGitで管理すると、\*.pgoファイルを更新&commitするたびに対応するCSVファイルの更新も手動で行なう必要があるため面倒臭い。

そこでGitで管理するのは\*.pgoファイルのみとしplotする時にCSVファイルを生成するようにした。

```Python
import subprocess
import os

# 最終更新時間をチェック
st_mtime_pgo = os.stat("hoge.pgo").st_mtime
st_mtime_csv_calc = os.stat("./.tmp_data/hoge_calc.csv").st_mtime

# *.pgoファイルが更新されていた場合のみ再びPGOPHERでスペクトルを計算する
if st_mtime_pgo > st_mtime_csv_calc:
    #generate *.csv file
    print("calculating by PGOPHER")
    _cp = subprocess.run(['pgo', '--plot','hoge.pgo','.tmp_data/hoge_calc.csv'])

# read *.csv file
df_calc = pd.read_csv(".tmp_data/hoge_calc.csv",
                 delimiter='\t',
                 header=None,
                 names=["x", "y"])
```

ポイントはPGOPHERのGUIの付いたversionではなくてCLI versionを使い`subprocess.run()`でコマンドを実行することと、\*.pgoファイルに差分がないのに計算するのはスマートではないので最終更新時刻を取得して更新されていた場合のみ再計算を行なうようにした点である。

まあ、エラーハンドリングは全くしていないし、CSVファイルを保存するディレクトリをどうするかにも議論の余地があるがそれはまた後で。

### pacmanで逆依存関係を知る

`-r`オプションを使うとそのパッケージに依存しているパッケージ、すなわち逆依存関係を知ることができる。

```bash
$ pactree -r <pkg name>
```

### Google-Chromeのgpuの状態の確認

`chrome://gpu`にアドレスバーからアクセスするとHardware Accelatation等が有効か否かの確認ができる。

さらに高度な設定を編集したい場合は`chrome://flags`にアクセスすれば可能である。

### swayのログで気になったやつ

ラップトップに外部ディスプレイを接続して二画面構成にしているが、
`DP-1`(外部ディスプレイ側)で操作しているときに`eDP-1`(ラップトップのディスプレイ側)に出力しようとしたけど失敗しましたのような?エラーが出力されている。

```txt
03:57:16.063 [ERROR] [sway/desktop/output.c:317] Page-flip failed on output eDP-1
04:01:05.204 [ERROR] [wlr] [backend/drm/atomic.c:79] connector eDP-1: Atomic commit failed: Device or resource busy
04:01:05.204 [ERROR] [sway/desktop/output.c:317] Page-flip failed on output eDP-1
[121012:121853:1127/174933.258577:ERROR:object_proxy.cc(576)] Failed to call method: org.freedesktop.NetworkManager.GetDevices: object_path= /org/freedesktop/NetworkManager: org.freedesktop.DBus.Error.ServiceUnknown: The name is not activatable
```

呼び出しに失敗する系のメッセージChrome関連らしい

```txt
[121012:121853:1127/193547.226095:ERROR:object_proxy.cc(576)] Failed to call method: org.freedesktop.NetworkManager.GetDevices: object_path= /org/freedesktop/NetworkManager: org.freedesktop.DBus.Error.ServiceUnknown: The name is not activatable
[121057:121057:1127/193559.075210:ERROR:gles2_cmd_decoder_passthrough.cc(1053)] [GroupMarkerNotSet(crbug.com/242999)!:A0F0220094040000]Automatic fallback to software WebGL has been deprecated. Please use the --enable-unsafe-swiftshader flag to opt in to lower security guarantees for trusted content.
```

[kanshi](https://sr.ht/~emersion/kanshi/)

### 最近もまたグラフィックがフリーズするのだが...

逆にkernelを普通に戻してみるか...

```bash
$ sudo pacman -Rs linux-lts linux-lts-headers
$ sudo grub-mkconfig -o /boot/grub/grub.cfg
```
rebootしてカーネルが最新版になっていることを確認 => Ok

### NVMeの不調

NVMe m.2 SSDが原因でマシンがフリーズすることがあったので原因を軽く調べてみた。

I/Oがabortする問題は

[Archのフォーラム](https://wiki.archlinux.org/title/Solid_state_drive/NVMe#Troubleshooting)

で議論されていて。Arch wikiのnvmeのトラブルシューティングの記事が貼られていた。

[Arch Wiki: Solid state drive/NVMe](https://wiki.archlinux.org/title/Solid_state_drive/NVMe#Troubleshooting)

どうやら、カーネルパラメータを触る必要があるらしい

この対処はまだやってない。後でやってみるかも

### デバイスの診断

そもそもSSDの健康状態がどうなのか気になったので調べてみた。

`smartmonotools`に含まれるsmartctlを使って行なう

intall
```bash
$ sudo pacman -S smartmonotools
```

チェック
```bash
$ sudo smartctl -a /dev/nvme0
```

特に目立った以上は見られなかったが、`Available Spare`が残り21%になっていたのでこのまま使用を続けるのは怖いので交換することを考えた。

### 2242 m.2 ssd

自分のこの症状が出たマシンも以下の記事と同じでセカンダリのssdが 2242規格の小さいものだったのでこの記事を参考にして交換することを検討中である。

[ThinkPad E14 Gen5(Core i7 13700H)でSSDとメモリを増設](https://risaiku.net/archives/8044/)

### ssdの不具合の原因がAPSTらしい

[Arch wiki: ソリッドステートドライブ](https://wiki.archlinux.jp/index.php/%E3%82%BD%E3%83%AA%E3%83%83%E3%83%89%E3%82%B9%E3%83%86%E3%83%BC%E3%83%88%E3%83%89%E3%83%A9%E3%82%A4%E3%83%96#.E3.83.88.E3.83.A9.E3.83.96.E3.83.AB.E3.82.B7.E3.83.A5.E3.83.BC.E3.83.86.E3.82.A3.E3.83.B3.E3.82.B0)

[minisforum UM790 x Ubuntu 22.04 で SSD が読み込み専用になる問題の対応](https://zenn.dev/mamayukawaii/articles/20240430114449)

tlpの設定も見直してみる。


nvmeの状態を見る
```bash
$ sudo pacman -S nvme-cli
```

APSTの状態を見てみる
```bash
$ sudo nvme get-feature -f 0x0c -H /dev/nvme0
get-feature:0x0c (Autonomous Power State Transition), Current value:0x00000001
	Autonomous Power State Transition Enable (APSTE): Enabled
	Auto PST Entries	.................
	Entry[ 0]
	.................
```

確かに`Enabled`に設定されている。

`/etc/default/grub`の`GRUB_CMDLINE_LINUX_DEFAULT`を以下のように編集する

前
```txt
GRUB_CMDLINE_LINUX_DEFAULT=""
```

後
```txt
GRUB_CMDLINE_LINUX_DEFAULT="nvme_core.default_ps_max_latency_us=0"
```

最後に

```bash
$ sudo grub-mkconfig -o /boot/grub/grub.cfg
```

で設定の更新を反映してから再起動する

```bash
$ reboot
```

もう一度確認して`Disabled`になっていたらOk

```bash
$ sudo nvme get-feature -f 0x0c -H /dev/nvme0
[sudo] password for aki:
get-feature:0x0c (Autonomous Power State Transition), Current value:00000000
	Autonomous Power State Transition Enable (APSTE): Disabled
	Auto PST Entries	.................
	Entry[ 0]
	.................
```

### Thunderbirdのデータの移行

[Support mozilla:Thunderbird のデータを新しいコンピューターに移動する](https://support.mozilla.org/ja/kb/moving-thunderbird-data-to-a-new-computer)

Linuxの場合は`~/.thunderbird`をコピーすればok。他のプラットフォームやGUIでデータを取り出したければ以下の手順を踏めば良い。

menue -> help -> troubuelshooting information

これで別windowが開く

一番上の`Application Basics`の中の`Profile Directory`の中の`Open Directory`をクリック

これでファイルエクスプローラーが開く。

開かれたディレクトリの二段階上位の階層に移動して`.thunderbird`を見つけてコピーして適当なクラウトストレージやusbストレージに保存すればok


別のマシンにバックアップしたデータを移すには先程と同じ方法を用いて`.thunderbird`が置かれていた場所にバックアップしたデータを置けばok。

### Inkscapeで論文の図を切り抜く

```bash
$ inkscape <name of journal>.pdf
```

Inkscapeが開き、pdfファイルの内容が複数のSVGファイルに分割される。
必要なページを選択して 400 dpi程のpngとして保存する。

```bash
$ inkscape <name of image>.png
```

左端の長方形選択をクリックしてから、切り抜きたい部分をマウスでなぞってから一旦切り抜きたい元の画像を右クリックする。

その後で、Shiftを押しながら選択した部分を右クリックして上のメニューからObject -> Clip -> Se Clipで切り抜きができる。

### skkの設定(以前メモり忘れていた)

変換の辞書には`skk-jisyo`が必要で、他のアプリケーションとの連携には`libskk`が必要
my-jisyo

```bash
$ sudo pacman -S libskk skk-jisyo
```

設定はまず`~/.config`に以下のようにMy辞書の設定を置く。

```bash
$ mkdir ~/.config/eskk
$ touch ~/.config/eskk/my_jisyo
```

NeoVimから使う場合には`vim-skk/eskk.vim`をinstallし以下のように`init.lua`を設定する。

```Lua
-- config eskk
vim.cmd([[let g:eskk#directory = "~/.eskk"]])
vim.cmd([[let g:eskk#dictionary = { 'path': "~/.config/eskk/my_jisyo", 'sorted': 1, 'encoding': 'utf-8',}]])
vim.cmd([[let g:eskk#large_dictionary = {'path': "/usr/share/skk/SKK-JISYO.L", 'sorted': 1, 'encoding': 'euc-jp',} ]])
vim.cmd([[let g:eskk#enable_completion = 1]])
```

### Google-ChromeでMarkdownを見る

以前は`markdown-preview-plus`を使用していたが、最近はどいう訳か、レンダリングされないので代替品として[Markdown Viewer](https://chromewebstore.google.com/detail/markdown-viewer/ckkdlimhmcjmikdlpkmbgfkaikojcbjk)を使うことにした。

最低限必要な設定は`Manage extensitions` から `Markdown Viewer`の`Details`をクリックして`Allow access to file URLs`を有効化すること。

今使い初めたところ、色々なテーマがあってかつ、左側にコンテンツのindexがあるのでより便利に使えそう。

### swayの明さ調節

明さ調整ツールをinstall

```bash
$ sudo pacman -S brightnessctl
```

して

`~/.config/sway/config `に以下を設定する
```txt
bindsym F5 exec brightnessctl set 5%-
bindsym F6 exec brightnessctl set 5%+
```

### C++ , Rustで関数型プログラミングをする際の役立ちそうなリンク

- [Rust for Haskell Developers](https://serokell.io/blog/rust-for-haskellers)
- [C++ on a Friday: Efficient Pure Functional Programming in C++ Using Move Semantics](https://blog.knatten.org/2012/11/02/efficient-pure-functional-programming-in-c-using-move-semantics/)

### Rustでデータ指向プログラミング

- [James McMurray's Blog: An introduction to Data Oriented Design with Rust](https://jamesmcm.github.io/blog/intro-dod/)
- [Guillaume Endignoux: Optimization adventures: making a parallel Rust workload even faster with data-oriented design (and other tricks)](https://gendignoux.com/blog/2024/12/02/rust-data-oriented-design.html)
- [Medium: Data-driven performance optimization with Rust and Miri](https://medium.com/source-and-buggy/data-driven-performance-optimization-with-rust-and-miri-70cb6dde0d35)

Rustに限らずデータ指向全般だが以下もある。

- [GitHub: dbartolini/data-oriented-design](https://github.com/dbartolini/data-oriented-design)

C++の向けの解説が参考になりそう。

### Virtual Boxの仮想マシンイメージの移行

Virtual Boxの仮想マシンイメージは`~/VirtualBox VMs`に保存されているので、それを何かしらの外部ストレージに保存する

```bash
$ cp -r ~/'VirtualBox VMs'/vm_image directory_for_backup
```

バックアップしたイメージを外部ストレージから別のマシンの適当なディレクトリに移す。一般的には`VirtualBox VMs`に置くことが多い。

Virtual Boxを起動して、緑色のプラス印の`Add`をクリックしてコピーしたいvm イメージを選択しそのディレクトリの中にある \*.vboxなファイルを選択して`Open`をクリックすればOK。

### HITRANのツールをRustで作りたい

[Pythonによる公式実装](https://github.com/hitranonline/hapi)と[Juliaによる実装](https://github.com/TacHawkes/HITRAN.jl)は存在するようです。

### juliaの環境構築

Linux環境では

```bash
$ curl -fsSL https://install.julialang.org | sh
```

を実行すればjulia自体とjuliaup(Rustのrustup相当)がinstallされる

LSPは julia-lspをinstallして`init.lua`に以下を設定する

```Lua
-- julia
require("lspconfig").julials.setup({})
```

なお今現在(2024 12/19 16:18(UTC+9))うまく動いていない模様
~/.local/state/lsp.logあたりを調査して原因を探っているところです。

### GaussianのDFTに分散力補正を追加する

- 参考
    - [Coordination Bond: DFTの分散力補正](https://whereareelectrons.blogspot.com/2018/05/dft.html)

GUIからだとできない?

gjfファイルの`# opt`から初まるセクションに以下を追加する

```txt
EmpiricalDispersion=hoge
```

hogeには以下のようなものが該当する

```txt
FD
GD3
GD3BJ
```

```txt
# opt b3lyp/6-31g geom=connectivity
```

の場合では以下のようにする。(`GD3BJ`)を選択した場合

```txt
# opt b3lyp/6-31g EmpiricalDispersion=GD3BJ geom=connectivity
```

とする。

### Rustのマクロ回り

- [speakerdeck: Rustの全マクロ種別が分かったつもりになれる話! / rust-all-kinds-of-macro](https://speakerdeck.com/optim/rust-all-kinds-of-macro)
- [FLINECT: How I Use Declarative Macros in Rust](https://flinect.com/blog/quick-tips-rust-declarative-macros)

### windowsのエクスプローラーで拡張子を表示する

ファイルエクスプローラーを開き、一番上のタブから`表示` -> `表示` -> `ファイル名拡張子`にチェックを入れる。

### GaussViewで\*.chkファイルが開けない時の対処法

- 参考
    - [DR.JOAQUIN BARROSO'S BLOG: If a .fchk file wont open in GaussView5.0](https://joaquinbarroso.com/2014/05/07/if-a-fchk-file-wont-open-in-gaussview5-0/)

ある日にGaussView(version 5.0)で計算結果として生成される\*.chkファイルを開こうとしたところ以下のようなメッセージが書かれたエラーwindowが表示されて開けなかった。

```txt
CConnectionGFCHK::Parse_GFCHK()
Missing or bad data: Alpha Orbital Energies
Line Number 1234
```

まず、`formchk`によってバイナリ形式の\*.chkファイルをアスキー形式に変換する。
ここで`formchk`はGaussianに付属するデフォルトではバイナリ形式な\*.chkファイルをアスキー形式に変換するツールでGaussianがinstallされているPath(多くの場合ではC:\G09w)を環境変数`Path`に追加することでコマンドラインから利用可能である。

```PS
PS > formchk <chk file name>.chk
```

これによって`<chk file name>.fch`が生成される。
これを適当なテキストエディタで開いてヘッダにある`Number of basis functions I`と`Number of independent functions I`の後に続く数字を等しくする。

これをGauss Viewで開くと無事開けるようになった。

しかし、この\*.fchファイルを`unfchk`でもう一度バイナリに変換してからGauss Viewで開くと開くことに失敗した。(なぜだ？？)

### neofetchの代用

neofetchがサ終したので代替品をと思い(今更?)

[色々ある](https://itsfoss.com/neofetch-alternatives/)が今回はfastfetchをinstallすることにした。

```bash
$ sudo pacman -S fastfetch
```

JOSONで設定をカスタムできるらしい

ついでに`lscpu`で出力されるようなCPUの情報をグラフィカルに表示するツールとして
`cpufetch`もinstallした

```bash
$ sudo pacman -S cpufetch
```

### Neovimの設定でしくじって動作をもっさりさせた話

LSPを使ってコードを読むときに定義にジャンプできると非常に便利である。
そこで以下のように`init.lua`に設定をして`jump to definition`なので`jd`とした

```Lua
--- jump to definition by 'jd'
vim.keymap.set("n", "jd", "<cmd>:lua vim.lsp.buf.definition()<CR>")
```

しかし下にカーソル移動をする時に`j`を使うので`j`が入力されると次の文字を待つモードに一旦入るため動作がもっさりしてしまう。

そこで`jd`から`gd`で定義へジャンプするように変更した。

```Lua
--- jump to definition by 'gd'
vim.keymap.set("n", "gd", "<cmd>:lua vim.lsp.buf.definition()<CR>")
```

### git blame

- [参考]
    - [Qiita: 他人が書いたようわからんコードを`git blame`で追う](https://qiita.com/tsuboyataiki/items/13e29abcad546776e8a9)

```bash
$ git blame <path_to_src>
```

とすると各行に対してハッシュ値、変更者の名前、コミットに日時の情報が得られる。

あるハッシュ値について対応するコミットに関する詳細な情報が欲しければ

```bash
$ git show <hash value>
```

とする。

コードが長くて一部分のみの情報を見たければ`-L`オプションで範囲を与えてやれば良い。

```bash
$ git blame -L <begin> <end> <path_to_src>
```

### pactree

パッケージの依存関係を表示するためのツールである。
以前は`pacman`パッケージの一部ではなくなって`pacman-contrib`に含まれているのでこれをinstallする。

```bash
$ sudo pacman -S pacman-contrib
```

### uv

- 参考
    - [zenn: uvだけでPythonプロジェクトを管理する](https://zenn.dev/turing_motors/articles/594fbef42a36ee)

install
```bash
$ sudo pacman -S uv
```

特定のversionのpythonをinstallするには
```bash
$ uv python install 3.12
```

installされているPython一覧を表示するには

```bash
$ uv python list
```

プロジェクトを作成してみる

```bash
$ mkdir <project name>
$ cd <project name>
$ uv init
$ uv pin python 3.12 # python 3.12を選択
$ uv sync # pyproject.tomlに基づいてパッケージをinstall
$. .venv/bin/activate # 仮想環境のactivate (deactivateするには deactivateを実行する)
```

### システムのPythonとは異なるversionのpythonを使う

- [参考]
    - [stack overflow:How does pipx know which Python version to use?](https://stackoverflow.com/questions/68735503/how-does-pipx-know-which-python-version-to-use)

Arch Linuxがシステムで仕様しているPythonのversionが3.13にupdateされたせいで`pipx`でinstallした`pubs`という文献管理ツールが動かなくなってしまった(3.13でpipesがなくなったことが原因みたい..)。
そこで、`uv`で3.12あたりのversionのPython環境を作ってそこで動かすことにした。

重要なポイントは特定のversionの仮想環境の中で

```bash
$ pipx install pubs --python $(which python)
```

と`--python`で指定することである。

```bash
$ mkdir install_pubs && cd install_pubs
$ uv init
$ uv pin python 3.12 # python 3.12を選択
$ uv sync # pyproject.tomlに基づいてパッケージをinstall
$. .venv/bin/activate # 仮想環境のactivate (deactivateするには deactivateを実行する)
$ pipx install pubs --python $(which python) # 構築した3.12の環境にpubsをinstallする
```

### cargo doc --openで開くブラウザを指定する

```bash
$ BROWSER=google-chrome-stable cargo doc --open # google-chrome
$ BROWSER=brave cargo doc --open # brave
$ BROWSER=firefox cargo doc --open # firefox
```

### Python 3.13でcmake-language-serverが動かなくなった

`pubs`の時と同じように`uv`でPython 3.12の仮想環境を構築してからその環境で`pipx`によってinstallしたところ無事動作した。

```bash
$ mkdir lsp_python && cd lsp_python
$ uv init
$ uv pin python 3.12 # python 3.12を選択
$ uv sync # pyproject.tomlに基づいてパッケージをinstall
$. .venv/bin/activate # 仮想環境のactivate (deactivateするには deactivateを実行する)
$ pipx install cmake-language-server  --python $(which python) # 構築した3.12の環境にpubsをinstallする
```
