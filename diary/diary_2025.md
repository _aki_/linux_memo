### Lua-language-serverでvim.apiなどでも補完が出るようにする

- 参考:
    - [Zenn:Neovim Lua のための LuaLS セットアップ](https://zenn.dev/uga_rosa/articles/afe384341fc2e1)

`Lua-language-server`をただ単に入れただけでは、`vim`から初まるようなNeovimのapiでは補完が効かないので以下のように設定すると良い。

```Lua
require("lspconfig").lua_ls.setup({
    settings = {
        Lua = {
            runtime = {
                version = "LuaJIT",
                pathStrict = true,
                path = { "?.lua", "?/init.lua" },
            },
            workspace = {
                library = vim.list_extend(vim.api.nvim_get_runtime_file("lua", true), {
                    "${3rd}/luv/library",
                    "${3rd}/busted/library",
                    "${3rd}/luassert/library",
                }),
                checkThirdParty = "Disable",
            },

        }
    }
})
```

細かいコードの内容は後で参考にした記事を読むことにする。

### タスクランナーをどうするか？(`cargo-make`編)

いつもmakeやシェルスクリプトを使いがちであるが..代替品を模索中

今回はRust製の`cargo-make`を試す。


- 参考:
    - [おおたの物置: cargo-makeがrust以外でも便利](https://ota42y.com/blog/2020/08/02/cago-make/)
    - [tkat0.dev: タスクランナーをmakeからcargo-makeへ移行](https://www.tkat0.dev/posts/cargo-make-1/)


install
```bash
$ cargo install cargo-make
```

ジョブは以下のようにTOMLファイルに記述する

Makefile.toml
```TOML
[tasks.CMD]
script = [
'''
#!/usr/bin/env bash
echo "Hi!"
'''
]
```

実行するときはジョブが`tasks.<job name>`と記述されているときに以下のように

```bash
$ cargo make <job name>
```

とすればOK

スクリプトとしてではなくてコマンドと引数としてジョブを記述したいときは以下のようにする。

```TOML
[tasks.BUILD]
cartegory = "develop"
description = "build program"
command = "poetry"
args = [
"build"
]
```

また以下のようにすれば`script`に直接Pythonを記述することも可能である。

```TOML
[tasks.PYJOB]
script_runner = "python"
script_extension = "py"
script = [
'''
x = 10
y = 20
print(f"x + y = {x + y}")
'''
]
```

### 畳み込み??

一般的に $f_i$ と $g_i$ 畳み込みといえば以下の式で定義される

$$
(f*g)_i = \sum_{k} f_k g_{i - k}
$$

一方画像処理やCNNの文脈では

$$
(f \otimes g)_i = \sum_{k} f_k g_{k + i}
$$

によって定義されたものを畳み込みと呼んでいる。

この後者によって定義されるものは正確には`相互相関関数`と呼ばれているものである。

### NeoVimで既存のバッファを水平画面分割して開く

以下のコマンドで可能

```VimScript
:vert sb<number of buffer>
```

例: 1番目のバッファを垂直画面分割して開きたい場合

```VimScript
:vert sb1
```

### グラフィカルなタスクマネージャー

たまにグラフィカルなタスクマネージャーを使いたくなるので...

xfce環境に付いてくるやつが軽量かつ必要十分な機能があるので`xfce4-taskmanager`を使う。

```bash
$ sudo pacman -S xfce4-taskmanager
```

設定は以下のようにしている

- General(有効にしている項目)
    - Show all process
    - Show process as tree
    - show legend
    - show values with more precision

- Columns(表示しているもの)
    - PID
    - PPID
    - Group Virtual Bytes
    - Resident Bytes
    - Group Resident Bytes
    - UID
    - CPU
    - Group CPU

### poetryからuvに移行するかな~~

[Qiita: uv へ移行 from pyenv & poetry (docker前提）](https://qiita.com/hkzm/items/3366e09733acfc524b2b)

### blenderをArch Linuxにinstallしてみる

- 参考
    - [オッサンはDesktopが好き: Ubuntu 20.04LTSにblender 3系をインストールする](https://changlikesdesktop.hatenablog.com/entry/2023/01/24/043601)
    - [Arch Wiki: デスクトップエントリ](https://wiki.archlinux.jp/index.php/%E3%83%87%E3%82%B9%E3%82%AF%E3%83%88%E3%83%83%E3%83%97%E3%82%A8%E3%83%B3%E3%83%88%E3%83%AA#.E5.9F.BA.E6.9C.AC)

pacmanで入れるか、直接バイナリを公式サイトからダウンロードしてくるか迷ったけど今回は
公式からダウンロードすることにした。

1. 公式サイト (https://www.blender.org/download/) からダウンロードする

2. ダウンロードしたディレクトリに入り、解凍する

```bash
$ cd blenders
$ ls *.tar.xz | xargs -I@ tar -Jxvf @
```

あとは適当な方法でバイナリ`blender`にPATHを通せばPATHが通っている端末から

```bash
$ blender
```

で起動が可能

3. ランチャーから起動できるようにする

解凍したフィルに含まれていた`blender.desktop`を`~/.local/share/applications`にコピーしてから以下のように編集した

```txt
Exec=~/.blenders/blender-4.3.2-linux-x64/blender %f
Icon=~/.blenders/blender-4.3.2-linux-x64/blender.svg
```

この書き方ではランチャーからアイコンは表示されるが、`blender`の起動ができなかった。

そこで

```bash
$ desktop-file-validate ~/.local/share/applications/blender.desktop #検証
```

によって検証をしてみるとアイコン及び実行ファイルへのPATHを絶対pathにしろと言われたので以下のように修正した。

```txt
Exec=/home/aki/.blenders/blender-4.3.2-linux-x64/blender %f
Icon=/home/aki/.blenders/blender-4.3.2-linux-x64/blender.svg
```

これでランチャーからも起動ができるようになった。

### clang-formatの設定

`clang-format`で既に存在するスタイルを利用する場合では以下のようにして`.clang-format`を生成すれば良い。

```bash
$ clang-format -style=google -dump-config > .clang-format
```

この例ではGoogleのコーディングスタイルを用いている。他にも`-sytle=llvm`などのスタイルが存在する。

### blenderでやろうとしていること

簡単な三次元データの可視化をできたらいいかな~と思っています。

[このブログ](https://kamino.hatenablog.com/entry/blender_viz_coordinates)を参考にしてPythonスクリプトを書いてやってみようと思っています。

### OpenCVをintel-oneapiでビルドする => 上手くいかない

- [参考]
    - [intel: How to Build openCV with Intel® oneAPI DPC++/C++ Compiler](https://www.intel.com/content/www/us/en/developer/articles/training/how-to-build-opencv-with-intel-compilers.html)

```bash
$ source /opt/intel/oneapi/setvars.sh
$ wget https://github.com/opencv/opencv/archive/4.x.zip # ソースを取得
$ mkdir build_cmake && cd build_cmake
$ cmake ../opencv-4.x --install-prefix /home/aki/opencv_oneapi/dependency \
-DCMAKE_C_COMPILER=icx -DCMAKE_CXX_COMPILER=icpx
$ cmake --build .
```

以下のあたりでビルドに失敗する

```txt
error: always_inline function '_mm256_zeroupper' requires target feature 'avx', but would be inlined into function 'VZeroUpperGuard' that is compiled without support for 'avx'
  207 |     inline VZeroUpperGuard() { _mm256_zeroupper(); }
```

普通にビルドする場合では成功したが`intel-oneapi`でビルドすると失敗する。

何が原因だろうか？

### OpenCVのビルド時間

- 使用マシン
    - OS: Arch Linux x86\_64
    - Kernel: 6.12.7-arch1-1
    - CPU: 13th Gen Intel i7-13700 (24) @ 5.100GHz
    - Memory: 31851MiB

- makeでビルドする

```bash
$ mkdir build_cmake_ninja && cd build_cmake_nina
$ cmake ../opencv-4.x --install-prefix /home/aki/opencv_build_test/dep_1
$ time make -j$(nproc)

real    7m24.557s
user    141m16.512s
sys     4m15.902s
```

- Ninjaでビルドする

CMakeからNinjaを使うときは`-GNinja`オプションを追加する

```bash
$ mkdir build_cmake_ninja && cd build_cmake_nina
$ cmake -GNinja ../opencv-4.x --install-prefix /home/aki/opencv_build_test/dep_1
$ time ninja

real    7m45.935s
user    148m56.124s
sys     4m25.195s
```

### intel-vtune-profiler

やろうとしていること:

[intel: Enhancing Rust Performance: Advanced Profiling with VTune and ittapi Crate](https://www.intel.com/content/www/us/en/developer/articles/guide/enhancing-rust-performance-profiling-with-vtune.html)

install

```bash
$ yay -S intel-vtune-profiler-standalone
```

この方法だとコケてinstallできない。

公式サイトからインンストーラーを落してから実行する方法が確実そう。

[inel: Get the Intel® VTune™ Profiler](https://www.intel.com/content/www/us/en/developer/tools/oneapi/vtune-profiler-download.html?operatingsystem=linux&linux-install-type=offline)

### intel-vtune-profiler を installした

- [参考]
    - [inel: Get the Intel® VTune™ Profiler](https://www.intel.com/content/www/us/en/developer/tools/oneapi/vtune-profiler-download.html?operatingsystem=linux&linux-install-type=offline)
    - [Qiita: Intel VTune Profiler の使い方](https://qiita.com/k0kubun/items/df7786611cf32877024c)

intelの公式から Download => Linux => Offline Installerからinstallerをダウンロード

```bash
$ wget https://registrationcenter-download.intel.com/akdlm/IRC_NAS/e7797b12-ce87-4df0-aa09-df4a272fc5d9/intel-vtune-2025.0.0.1130_offline.sh
```

installerは一般ユーザーで実行すると`$HOME`以下にinstallされ、 rootで(sudoで)実行するとシステムのパスにinstallされる。

```bash
$ sh ./intel-vtune-2025.0.0.1130_offline.sh
```

デフォルトでは`~/intel/`以下にinstallされる

PATHを通すには

```bash
$ source ~/intel/oneapi/vtune/latest/vtune-vars.sh
```

とする。これでシェルから`vtune`,`vtune-gui`が実行することが可能になった。

application launcherから起動するには`~/intel/oneapi/vtune/latest/bin64`へのリンクを`~/.local/share/applications`に貼れば良い(\*.desktopへのpathを直で貼ってはダメ)。

```bash
$ ln -s $HOME/intel/oneapi/vtune/latest/bin64 $HOME/.local/share/applications
```

ただ何故かIconが表示されないので相対pathになっていた\*.desktopの`Icon`のセクションを絶対pathでハードコードすることで表示するようになった(力技!)

hot-codeの解析を行うには

```bash
# echo 0 > /proc/sys/kernel/yama/ptrace_scope
```

が必要だった。ここで、この操作は`sudo`ではダメで`root`にログインして行う必要があった。


細かいことはチュートリアル等をやりつつ以下の公式ガイド

[intel: Intel VTune Profiler User Guide](https://www.intel.com/content/www/us/en/docs/vtune-profiler/user-guide/2025-0/overview.html)

を読みキャッチアップしたい。

### pacmanのミラーをJAISTのものを使う

- 参考
    - [Arch wiki: ミラー](https://wiki.archlinux.jp/index.php/%E3%83%9F%E3%83%A9%E3%83%BC)


`/etc/pacman.conf`の`[core]`のセクションの`Include`の前にサーバーのURLを追加する

```txt
[core]
Server = ftp://ftp.jaist.ac.jp/pub/Linux/ArchLinux/core/os/$arch
Include = /etc/pacman.d/mirrorlist
```

追加後に

```bash
$ sudo pacman -Syu
```

を実行すればok!

### C/C++用にclang-formatをNeovimで設定する

今回はNeovimでformatterを利用するためのプラグインとして`formatter.nvim`を利用する。

- 設定:

設定ファイルは`init.lua`にぶらさがっている設定ファイルに実装する必要がある。

```Lua
require("formatter").setup {
  -- Enable or disable logging
  logging = true,
  -- Set the log level
  log_level = vim.log.levels.WARN,
  -- All formatter configurations are opt-in
  filetype = {

    c = {
        require('formatter.filetypes.c').clangformat,
    },

    cpp = {
        require('formatter.filetypes.cpp').clangformat,
    },
  }
```

### pre-commitを導入してみようと..

- 参考
    - [GitHub: pre-commit](https://github.com/pre-commit/pre-commit)
    - [pre-commit](https://pre-commit.com/)
    - [DeveloppersIO produced by classmethod:
【Git】コミット直前に自動でファイルを整形する「pre-commit」が便利すぎたので紹介したい](https://dev.classmethod.jp/articles/introduce-pre-commit/)

`pre-commit`を使うとあ...あれを忘れたのにコミットしちゃった...を防げるらしい。

- intall

Python製のツールなので`pipx`でinstallする

```bash
$ pipx install pre-commit
```

設定や使い方などはまた後で。

### Ruffを導入する

Rust製のPythonのLinter/Formatter

- 参考
    - [GitHub:ruff](https://github.com/astral-sh/ruff)
    - [Zenn: Ruff + pre-commitでコミット時にコード品質を保ちたい](https://zenn.dev/nowa0402/articles/79aaeb8db5731c)

install

```bash
$ pipx instal ruff
```

Ruffの設定は`pyproject.toml`に設定することになっているので`uv`や`poetry`がプロジェクト作成時に生成したものに追記するようにすればよい。

Neovimからsave時に実行するには以下のように設定する

```Lua
require("formatter").setup {
  -- Enable or disable logging
  logging = true,
  -- Set the log level
  log_level = vim.log.levels.WARN,
  -- All formatter configurations are opt-in
  filetype = {

    python = {
        require('formatter.filetypes.python').ruff,
    },
  }
```

### ghqを導入する

- 参考
    - [Zenn: モテるGit管理 (gh, ghq, git-cz, lazygit)](https://zenn.dev/mozumasu/articles/mozumasu-lazy-git)

- install
```bash
$ sudo pacman -S ghq
```

`~/.gitconfig`に`ghq`が管理下に置くディレクトリを設定する

```txt
[ghq]
 root = /home/aki
```

`fzf`と併せてリポジトリの検索ができるようにした

```bash
fgq() {
    src=$(ghq list | grep '^[^\.]' | fzf --preview "bat --color=always --style=header,grid --line-range :80 $(ghq root)/{}/README.*")
    echo "$HOME/$src"
}
```

そもそも`$HOME`の下に直接リポジトリを置くのが不適切な気がするが...(記事ではリポジトリを$HOME/srcなんかに置いていた..)

### straceprofを使ってみる

straceコマンドを使ってビルド時のプロファイルを取るためのツールを使ってみた。

- 参考
    - [Zenn:straceprof——とにかく簡単にビルドのプロファイルを取るためのソフトウェア](https://zenn.dev/a_kawashiro/articles/cfa7456ca49cab)
    - [GitHub: akawashiro/straceprof ](https://github.com/akawashiro/straceprof?tab=readme-ov-file)


install

```bash
$ sudo pacman -S strace
$ pipx install straceprof
```


使うには以下のようにする

```bash
$ cd <project dir>
$ strace \
    --trace=execve,execveat,exit,exit_group \
    --follow-forks \
    --string-limit=1000 \
    --absolute-timestamps=format:unix,precision:us \
    --output=straceprof.log \
    <command to profile>
$ straceprof \
    --log=straceprof.log \
    --output=straceprof.png
```

今回は昔開発したRust製のシミュレーター[aki-ph-chem/diatomic\_simulator](https://github.com/aki-ph-chem/diatomic_simulator)のビルドで試してみる(単純にビルドがそこそこ重そうだったので)

```bash
$ gh repo clone aki-ph-chem/diatomic_simulator
$ cd diatomic_simulator
$ strace \
    --trace=execve,execveat,exit,exit_group \
    --follow-forks \
    --string-limit=1000 \
    --absolute-timestamps=format:unix,precision:us \
    --output=straceprof.log \
    cargo build --release
$ straceprof \
    --log=straceprof.log \
    --output=straceprof.png
```

結果として以下が得られた。

<img src="./fig/straceprof_diatomic_simulator.png"/>

### PGOPHERの Transiion Momentsについて

- 参考
    - [PGOPHER: Transition Moments](https://pgopher.chm.bris.ac.uk/Help/transitionmoments.htm)
    - [PGOPHER: Assymmetric Top Transition Moments](https://pgopher.chm.bris.ac.uk/Help/asymtransitionmoments.htm)
    - [PGOPHER: Cartesian Transition Moments for Asymmetric Tops](https://pgopher.chm.bris.ac.uk/Help/asymcartesiantransitionmoment.htm)

特にAsymmetric Topでデカルト座標を採用する場合について

- View => Consttantで出るwindow上で表示されるtreeの一番下の `<Excited|mu|Ground>` の部分木を見る。
    - この部分木にぶら下がっているブラケット(デフォルトでは一個のみ)でTransition Momentの設定を行う。
    - ぶら下っているブラケットを増やしたい場合では右クリックでCopyしてからペースストすれば良い。

- 各ブラケットでは基底状態、励起状態の振動量子数(`v = <quantom number>` )、軸の方向(`Axis`)双極子モーメントの値(`Strength`)の項目を設定できる。
    - 各軸( $a,b, c$ )は **慣性軸** の方向にする必要がある
    - 特定の軸方向の寄与をゼロにしたければその軸(`Axis`)を選択して0にする必要がある
    - 値(`Strength`)はDebye単位の値が必要である

### pre-commit: 実践編(1)

- 参考
    - [pre-commit hooks](https://pre-commit.com/)
    - [Zenn: Pythonの開発環境はmise, Task, uv, Ruff, mypyで落ち着いた](https://zenn.dev/mottyzzz/articles/20250101111916)
    - [Zenn: pre-commit で Python コードをキレイに管理してみた](https://zenn.dev/fikastudio/articles/73c226000f9a0a)
    - [Zenn: Ruff + pre-commitでコミット時にコード品質を保ちたい](https://zenn.dev/nowa0402/articles/79aaeb8db5731c)
    - [GitHub: pre-commit/mirrors-clang-format](https://github.com/pre-commit/mirrors-clang-format)
    - [GitHub: ssciwr/clang-format-wheel](https://github.com/ssciwr/clang-format-wheel)

実践例のリポジトリはこちら=> [\_\_aki\_\_ / pre\_commit\_example](https://gitlab.com/_aki_/pre_commit_example)

このプロジェクトではC++のファイル`main.cpp`だけで`.clang-format`でフォーマットすることにする。
基本的にはNeovimでformatterの設定をしており、save時に`.clang-format`に基づいてフォーマットされるようにしてはいる。

- 1: pre\_commitのconfigファイルを作成

```bash
$ pre-commit sample-config > .pre-commit-config.yaml
```

このコマンドによって以下のようなファイルが生成される。

```YAML
# See https://pre-commit.com for more information
# See https://pre-commit.com/hooks.html for more hooks
repos:
-   repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v3.2.0
    hooks:
    -   id: trailing-whitespace
    -   id: end-of-file-fixer
    -   id: check-yaml
    -   id: check-added-large-files
```

今回は`clang-format`によるフォーマットがされていない場合にコミットできないようにしたいので、
以下のように設定する。

```YAML
-   repo: https://github.com/pre-commit/mirrors-clang-format
    rev: '19.1.7'
    hooks:
    -   id: clang-format

```

configの設定が終ったら以下を実行して反映させる。
```bash
$ pre-commit install
```

`main.cpp`適当な差分を加えてからcommitするとソースコードの検証が走る(はず)。

ここでは以下のような差分を加えた

```bash
$ git diff main.cpp
diff --git a/main.cpp b/main.cpp
index 600ced3..f823f15 100644
--- a/main.cpp
+++ b/main.cpp
@@ -6,6 +6,8 @@ auto add_two(T a, T b) -> T {
   return a + b;
 }

+auto add_tree(int a, int b, int c) -> int { return a + b + c; }
+
 auto main(void) -> int {
   auto res_1 = add_two(10, 2);
   auto res_2 = add_two(std::string{"foo"}, std::string{" bar"});
```

今のところ上手く動かない。

なんやかんややっていたらconfigを以下のように修正したら正常に動くようになった。

```YAML
repos:

- repo: https://github.com/pre-commit/mirrors-clang-format
  rev: 'v19.1.7'
  hooks:
  - id: clang-format
```

- ポイントは
    - まず`repos:`から初める必要があること(冷静になればテンプレからあたりまえ)
    - `rev`でしてするところが`v`からスタートする必要があること

であった。自分の環境では通常CLIで`git commit`とするとNeovimが起動してそこでコッミトメッセージを記入するが、
`pre-commit`が設定された環境では、一旦検証が走ってからpassした場合のみNeovimが起動する。

### pre-commit: 実践編(2)

では`.clang-format`に基づかないコードを書いてcommitしようとしたときではどうなるかを見てみる。

以下のような差分を`main.cpp`に加えてみた。

```bash
 $ git diff main.cpp
diff --git a/main.cpp b/main.cpp
index f823f15..2a9e120 100644
--- a/main.cpp
+++ b/main.cpp
@@ -6,7 +6,9 @@ auto add_two(T a, T b) -> T {
   return a + b;
 }

-auto add_tree(int a, int b, int c) -> int { return a + b + c; }
+auto add_tree(int a, int b, int c) -> int {
+        return a + b + c;
+}

 auto main(void) -> int {
   auto res_1 = add_two(10, 2);
```

このとき`main.cpp`をステージングしてcommitしてみる

```bash
$ git add main.cpp
$ git commit
clang-format.............................................................Failed
- hook id: clang-format
- files were modified by this hook
```

と`clang-format`のせいで確かにcommitできなかった。

### intelのVtune ProfilerのC++のチュートリアルの準備

Intel Vtune ProfilerのGUI版を立ち上げると下側の`FEATURED CONTENT..`に[Improving Hotspot Observability in a C++ Application Using Flame Graphs](https://www.intel.com/content/www/us/en/docs/vtune-profiler/cookbook/2025-0/flame-graph-cpp.html)
があったのでやってみる。

サンプルのソースコードを見るとC++のライブラリであるBoostを利用しているので、
Boostの環境を整える。

`pacman -Ss boost`で調べてみると自分の環境では`boost-libs`は既にinstall済みだったので`boost`のみinstallすることにした。

```bash
$ pacman -Ss boost
extra/boost 1.86.0-6
    Free peer-reviewed portable C++ source libraries (development headers)
extra/boost-libs 1.86.0-6 [installed] # install済み
    Free peer-reviewed portable C++ source libraries (runtime libraries)
extra/booster 0.12-1
    Fast and secure initramfs generator
extra/fasd 1.0.2-2
    Command-line productivity booster, offers quick access to files and directories
extra/ibus-typing-booster 2.27.10-1
    Predictive input method for the IBus platform
extra/python-boost-histogram 1.5.0-1
    Python bindings for Boost's Histogram library.
extra/python-hist 2.8.0-1
    Python bindings for Boost's Histogram library.
extra/websocketpp 0.8.2-2
    C++/Boost Asio based websocket client/server library

$ sudo pacman -S boost
```
Boostのソースからのbuild & installはまた後で気が向いたらやろう!

### Boostをソースからbuild&install

- 参考
    - [Qiita: C++のライブラリboostの最新版をLinuxサーバーにインストールする方法](https://qiita.com/king_dog_fun/items/0dc555e847acba89aae7)
    - [boostjp:Boost日本語情報サイト: Boostライブラリのビルド方法](https://boostjp.github.io/howtobuild.html)

まず公式サイト[Boost Downloads](https://www.boost.org/users/download/)に行き、ソースコードへのURLをコピーしてからwgetでダウンロードしてから解凍する。

```bash
$ wget https://archives.boost.io/release/1.87.0/source/boost_1_87_0.tar.gz
$ tar -xvf boost_1_870.tar.gz
```

ソースのディレクトリに入り、`bootstrap.sh`を実行する

```bash
$ cd boost*
$ ./bootstrap.sh
```

build用の`b2`が生成されるので以下を実行すればbuildが走り、`--prefix=`以下にinstallされる。

```bash
$./b2 install -j17 --prefix=$HOME/intel_vtune_tutorial/third_party
```

今回はビルドのプロファイルを`straceprof`で可視化してみたいので以下のように`starace`でロギングしながらbuildを行なった。

```bash
$ strace \
    --trace=execve,execveat,exit,exit_group \
    --follow-forks \
    --string-limit=1000 \
    --absolute-timestamps=format:unix,precision:us \
    --output=straceprof.log \
   ./b2 install -j17 --prefix=$HOME/intel_vtune_tutorial/third_party

$ straceprof \
    --log=straceprof.log \
    --output=straceprof.png
```

プロファイルは以下が得られた

<img src="./fig/straceprof_boost_1_87_0.png" />

### ソースからEigenをinstallする

```bash
$ mkdir build_eigen
$ cmake <eigen source dir> -DCMAKE_INSTALL_PREFIX=<myprefix>
$ make install
```

これで`<myprefix>`で指定したディレクトリにinstallされる。

### Eigenを使用するプロジェクトをCMakeでビルドする

- 参考
    - [Eigen: Using Eigen in CMake Projects](https://libeigen.gitlab.io/docs/TopicCMakeGuide.html)

```CMake
cmake_minimum_required(VERSION 3.13)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

project(eigen3_cmake)
find_package(Eigen3 REQUIRED)

add_executable(main main.cpp)
target_link_libraries(main Eigen3::Eigen)
```

### GoogleTestをソースからビルドしてインストール

ソースをクローンしてから、プロジェクト直下の`third_party`にinstallする。

```bash
$ mkdir third_party # install先
$ git clone https://github.com/google/googletest.git
$ mkdir build_google_test
$ cmake ../googletest -DCMAKE_INSTALL_PREFIX=$HOME/google_test_example/third_party
$ make -j17 # build
$ make install # install
```
### Git for Windowsのアップデート

結構アプデのニュースを見たりするので...

GitBashもしくはPowerShellから以下のコマンドを実行する

```bash
$ git update-git-for-windows
```

このコマンドを実行するとシェル上でupdateに同意するように言われるので`y`で同意するとGUIのwindowが出てインストーラーがまた同意を求めてくるので`yes`をクリックしすればOK。


なお、そのときGitBashを使っているとそのシェルを終了するように言われた。

### Windowsにfzfをinstallする

- 参考
    - [GitHub: fzf](https://github.com/junegunn/fzf)
    - [How to get fzf working in PowerShell](https://sathyasays.com/2023/04/11/powershell-fzf-psfzf/#psfzf)
    - [GitHub: kelleyma49/PSFzf](https://github.com/kelleyma49/PSFzf)
    - [Qiita: PowerShellモジュールのPSFzfとZLocationをつかってみる](https://qiita.com/SAITO_Keita/items/f1832b34a9946fc8c716)

`fzf`は `chocolately`,`Scoop`,`Winget`,`pacman(MSYS2)`でinstall可能

今回は`Winget`でinstallした
```bash
$ winget install fzf
```

GitBashでは、`Ctrl + r`でシェルの履歴を検索する機能はLinuxで設定したものと全く同じものを設定すればOk。

```bash
# fuzzy finder
eval "$(fzf --bash)"
```

PowerShellでも同じように使えるようにするには`fzf`をPowerShellでラップした`PSFzf`が必要とのことなので以下でinstallする。

`PowerShell Gallery`というリポジトリからinstallされる。(NuGetの更新？をしろと言われた場合はYで同意すればOK)

```PS
PS> Install-Module -Name PSFzf -scope currentUser
```

続いてエディタで`$PROFILE`を開くと

```PS
PS> vim $PROFILE
```

`$HOME\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1`が開くので、以下の内容を書きこむ。

```txt
Set-PsFzfOption -PSReadlineChordProvider 'Ctrl+t' -PSReadlineChordReverseHistory 'Ctrl+r'
```

これでPowerShellを再起動すればOK。

### 後でやることリスト@ 2025 1/25

- DAPの設定をNeovimに組みこむ
    - 参考: [ryota2357: Neovimでcodelldbを使ってC++をデバッグする(nvim-dap, Mason.nvim)](https://ryota2357.com/blog/2022/cpp-codelldb-debug-nvim/)

- LuaのフォーマッタStyLuaの設定
    - 参考: [Zenn: Luaのコードフォーマッター StyLua](https://zenn.dev/hituzi_no_sippo/articles/20210911092842-introduction_to_stylua)

### mfussenegger/nvim-dapの設定

- 参考
    - [GitHub: mfussenegger/nvim-dap Wiki](https://github.com/mfussenegger/nvim-dap/wiki/C-C---Rust-(via--codelldb))

`codelldb`でデバッッグをする場合では以下のようにC++,Rust向けに設定した。

```Lua
local dap = require('dap')
-- ref: https://github.com/mfussenegger/nvim-dap/wiki/C-C---Rust-(via--codelldb)

-- debugger config
-- codelldbのversionが1.11.0以降(現時点で1.11.2)であることに注意
dap.adapters = {
  codelldb = {
    type = 'executable',
    command = 'codelldb',
  },
}

dap.configurations = {
  cpp = {
    {
      name = 'Launch file',
      type = 'codelldb',
      request = 'launch',
      program = function()
        return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
      end,
      cwd = '${workspaceFolder}',
      stopOnEntry = false,
    },
  },

  rust = {
    {
      name = 'Launch file',
      type = 'codelldb',
      request = 'launch',
      program = function()
        -- デバッグビルドされたバイナリが'tar/get/debug'以下にあることに注意
        return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/target/debug', 'file')
      end,
      cwd = '${workspaceFolder}',
      stopOnEntry = false,
    },
  },
}

-- C向けにはC++の設定を流用
dap.configurations.c = dap.configurations.cpp
```

さらにコマンド&キーマップの設定を以下のように行なった。

```Lua
-- commands & keymap
-- Setting breakpoints
vim.api.nvim_create_user_command('B', function()
  vim.cmd("lua require'dap'.toggle_breakpoint()")
end, { nargs = 0 })

-- Luanching debug session and resuming execution
vim.api.nvim_create_user_command('C', function()
  vim.cmd("lua require'dap'.continue()")
end, { nargs = 0 })

-- Stepping through code
-- ref: ':help dap-mapping'
vim.keymap.set('n', '<C-i>', function()
  require('dap').step_into()
end)
vim.keymap.set('n', '<C-o>', function()
  require('dap').step_over()
end)

-- Inspecting the state vi the buid-in REPL
vim.api.nvim_create_user_command('Rpl', function()
  vim.cmd("lua require'dap'.repl_open()")
end, { nargs = 0 })
```

これで`:B`でブレークポイントを貼って、`:C`で対象の実行バイナリを選び、`Ctrl + i`,`Ctrl + o`でテップ実行できるようになった。

### rcarriga/nvim-dap-ui の設定

- 参考
    - [GitHub: rcarriga/nvim-dap-ui](https://github.com/rcarriga/nvim-dap-ui)
    - [ryota2357: Neovimでcodelldbを使ってC++をデバッグする(nvim-dap, Mason.nvim)](https://ryota2357.com/blog/2022/cpp-codelldb-debug-nvim/)

- 依存するパッケージ
    - [mfussenegger/nvim-dap](https://github.com/mfussenegger/nvim-dap)
    - [nvim-neotest/nvim-nio](https://github.com/nvim-neotest/nvim-nio)


主に [ryota2357: Neovimでcodelldbを使ってC++をデバッグする(nvim-dap, Mason.nvim)](https://ryota2357.com/blog/2022/cpp-codelldb-debug-nvim/)を参考にUIの設定を行なった。

```Lua
local dap_ui = require('dapui')
-- ref: https://ryota2357.com/blog/2022/cpp-codelldb-debug-nvim/
dap_ui.setup({
  icons = { expanded = '', collapsed = '' },
  layouts = {
    {
      elements = {
        { id = 'repl', size = 0.15 },
        { id = 'stacks', size = 0.2 },
        { id = 'watches', size = 0.2 },
        { id = 'scopes', size = 0.35 },
        { id = 'breakpoints', size = 0.1 },
      },
      size = 0.4,
      position = 'left',
    },
    {
      elements = { 'console' },
      size = 0.25,
      position = 'bottom',
    },
  },
})
```

この設定のだけではデバッグを開始したときに自動ではUIが表示されないので以下を追加する

```Lua
-- open / close ui windows automatically
local dap = require('dap')
dap.listeners.after.event_initialized['dapui_config'] = function()
  dap_ui.open()
end
dap.listeners.before.event_terminated['dapui_config'] = function()
  dap_ui.close()
end
dap.listeners.before.event_exited['dapui_config'] = function()
  dap_ui.close()
end
```

さらに、UIを`:Dui`でOn/Offできるようにした。

```Lua
-- toggle dap-ui
vim.api.nvim_create_user_command('Dui', function()
  dap_ui.toggle()
end, { nargs = 0 })
```
