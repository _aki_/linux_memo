### Python debugpy

- 参考
    - [GitHub: microsoft/debugpy](https://github.com/microsoft/debugpy)

nvim-dapでdebugpyを使ってデバッグ実行ができるように設定した。
リモートデバッグはまた後で...。

`debugpy`はmasonではinstallせずに`uv`で作成した仮想環境に

```bash
$ uv install debugpy
```

でinstallしたものを用いることにする。

```Lua
dap.adapters = {
  debugpy = {
    type = 'executable',
    command = vim.fn.getcwd() .. '/.venv/bin/python',
    args = { '-m', 'debugpy.adapter' },
    options = {
      source_filetype = 'python',
    },
  },
}

dap.configurations = {
  python = {
    {
      name = 'Launch file',
      type = 'debugpy',
      request = 'launch',
      program = '${file}',
      pythonPath = function()
        -- debugpy supports launching an application with a different interpreter then the one used to launch debugpy itself.
        -- The code below looks for a `venv` or `.venv` folder in the current directly and uses the python within.
        -- You could adapt this - to for example use the `VIRTUAL_ENV` environment variable.
        local cwd = vim.fn.getcwd()
        if vim.fn.executable(cwd .. '/venv/bin/python') == 1 then
          return cwd .. '/venv/bin/python'
        elseif vim.fn.executable(cwd .. '/.venv/bin/python') == 1 then
          return cwd .. '/.venv/bin/python'
        else
          return '/usr/bin/python'
        end
      end,
    },
  },
}
```

### Rustのデバッグ事情

- 参考
    - [Qiita: Rustプログラムのデバッグ辛すぎ問題](https://qiita.com/k0kubun/items/766dc15773ec73925163)

後で`rr`やらを調べる。

### google-chromeのreading listをツールバーに表示する

menue -> BookMarks and list -> reading list -> show reading list

でreading listが表示される


ピンのマークのPin to toolbarでツールバーに固定できる。

### FHSに対して$HOMEより上の空間でのファイルの置き方の基準にXDG Base Directoryってのがあるらしい

- 参考
    - [Arch Wiki: XDG Base Directory](https://wiki.archlinux.jp/index.php/XDG_Base_Directory)
    - [Arch Wiki: XDG ユーザーディレクトリ](https://wiki.archlinux.jp/index.php/XDG_%E3%83%A6%E3%83%BC%E3%82%B6%E3%83%BC%E3%83%87%E3%82%A3%E3%83%AC%E3%82%AF%E3%83%88%E3%83%AA)
    - [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/latest/)

### プロジェクト側にLSPのconfigを置く試み

以下はC++のclangdの例

プロジェクト側には以下の`clangd_config.lua`を置く、

```Lua
return {

	cmd = {
		"/usr/bin/clangd",
		"--log=verbose",
		"--background-index",
		"--clang-tidy",
		"--completion-style=detailed",
		"--header-insertion=iwyu",
		"--suggest-missing-includes",
		"--cross-file-rename",
	},
}
```

これを`init.lua`側に

```Lua
-- cpp
local clangd_config = {
  cmd = {
    'clangd',
    '--log=verbose',
    '--background-index',
    '--clang-tidy',
    '--completion-style=detailed',
    '--header-insertion=iwyu',
    '--suggest-missing-includes',
    '--cross-file-rename',
  },
}

local path_clangd_config = vim.fn.getcwd() .. '/clangd_config.lua'
local ok, project_config = pcall(dofile, path_clangd_config)
if ok then
  clangd_config = project_config
end
require('lspconfig').clangd.setup(clangd_config)
```

以下を実装することで`clangd_config.lua`が存在したらそれを読み込みそっちconfigに切り変えることができるようになった。
プロジェクト側にconfigを置くことができるようになったことで、プロジェクトによってconfigを変更するようなことが可能になる。

clagd以外にも他の言語のLSPもあるので統一的に管理する方法なども考えるつもりである。

### Hugging Face CLIを導入する

- 参考
    - [Qiita: Hugging Face のキャッシュ操作 / huggingface-cli と HF_HOME 環境変数](https://qiita.com/ma2shita/items/5eb42c59ba0bb9ef346b)
    - [note: 覚え書き：HuggingfaceからGGUFをCLIでダウンロードする方法](https://note.com/lucas_san/n/n9655d09abddc)

install

```bash
$ pipx "huggingface_hub[cli]"
```

モデルをdownload

`mmnga/DeepSeek-R1-Distill-Qwen-7B-gguf`(リポジトリ名)の`Q4_K_M/DeepSeek-R1-Distill-Qwen-7B-Q4_K_M-00001-of-00001.gguf`(ファイル名)
を現在いるディレクトリにdownload

```bash
$ huggingface-cli download mmnga/DeepSeek-R1-Distill-Qwen-7B-gguf Q4_K_M/DeepSeek-R1-Distill-Qwen-7B-Q4_K_M-00001-of-00001.gguf --local-dir .
```

### fzf-luaの設定

- 参考
    - [GitHub: fzf-lua](https://github.com/ibhagwan/fzf-lua?tab=readme-ov-file)
    - [Zenn: fzf-luaを使う](https://zenn.dev/botamotch/articles/a41052477342d5)


intall

```Lua
-- for fzf-lua
local fzf_lua = {
  'ibhagwan/fzf-lua',
  -- optional for icon support
  dependencies = { 'nvim-tree/nvim-web-devicons' },
}

return {
    // others,
    fzf_lua
}
```

設定は以下のようにする

```Lua
require('fzf-lua').setup({
  winopts = {
    height = 0.85, -- window height
    width = 0.80, -- window width
    row = 0.35, -- window row position (0=top, 1=bottom)
    col = 0.50, -- window col position (0=left, 1=right)
    border = 'rounded', -- 'none', 'single', 'double', 'thicc' or 'rounded'
    fullscreen = false, -- start fullscreen?
  },
})

-- search file from cwd
vim.keymap.set('n', '<leader>f', function()
  require('fzf-lua').files()
end)

-- search file from not ignored by .gitignore
vim.keymap.set('n', '<leader>g', function()
  require('fzf-lua').git_files()
end)

-- search file from old files
vim.keymap.set('n', '<leader>o', function()
  require('fzf-lua').oldfiles()
end)

-- search file from buffers
vim.keymap.set('n', '<leader>b', function()
  require('fzf-lua').buffers()
end)
```

LSPを使う設定もあるがまた後で..

### git log --graphで全てのブランチを表示する

```bash
$ git log --graph --all --decorate
```

### Lazygitを導入する

- 参考
    - [GitHub: jesseduffield/lazygit](https://github.com/jesseduffield/lazygit?tab=readme-ov-file)
    - [GitHub: jesseduffield/lazygit configのガイド](https://github.com/jesseduffield/lazygit/blob/master/docs/Config.md)
    - [GitHub: kdheepak/lazygit.nvim](https://github.com/kdheepak/lazygit.nvim)

install

```bash
$ sudo pacman -S lazygit
```

起動

```bash
$ lazygit # qで終了
```

Neovimから起動できるようにする

`lazy.git.nvim`をinstalして以下のように設定する

```Lua
{
  'kdheepak/lazygit.nvim',
  lazy = true,
  cmd = {
    'LazyGit',
    'LazyGitConfig',
    'LazyGitCurrentFile',
    'LazyGitFilter',
    'LazyGitFilterCurrentFile',
  },
  -- optional for floating window border decoration
  dependencies = {
    'nvim-lua/plenary.nvim',
  },
  -- setting the keybinding for LazyGit with 'keys' is recommended in
  -- order to load the plugin when the command is run for the first time
  keys = {
    { '<leader>lg', '<cmd>LazyGit<cr>', desc = 'LazyGit' },
  },
},
```

lazygitそのものの設定はまた後で...

### Neovim + debugpy でPythonのCLI アプリケーションをリモートデバッグ

- 参考
    - [Zenn: VSCode + debugpy でPython CLIをターミナルから快適にデバッグする](https://zenn.dev/shun_kashiwa/articles/debug-python-cli-with-debugpy-vscode)

- 設定

VSCodeでデバッグしている記事でやっていることをNeovimでもできるようにしてみる。

nvim-dapの設定は以下のようにした。
`localhost(127.0.0.1):5678`で立てたサーバーに対してクライアントをattachするようになっている。

```Lua
dap.adapters = {
  debugpy_tcp = {
    type = 'server',
    port = assert('5678', '`connect.port` is required for a python `attach` configuration'),
    host = '127.0.0.1',
    options = {
      source_filetype = 'python',
    },
  },
}

dap.configurations = {
  python = {
    {
      name = 'Launch file by debugpy_tcp',
      type = 'debugpy_tcp',
      request = 'attach',
      program = '${file}',
      pythonPath = function()
        local cwd = vim.fn.getcwd()
        if vim.fn.executable(cwd .. '/venv/bin/python') == 1 then
          return cwd .. '/venv/bin/python'
        elseif vim.fn.executable(cwd .. '/.venv/bin/python') == 1 then
          return cwd .. '/.venv/bin/python'
        else
          return '/usr/bin/python'
        end
      end,
    },
  }
}
```

- やり方

```bash
$ source .venv/bin/activate
$ python -m debugpy --wait-for-client --listen 5678 cli.py args
```

でサーバーを起動してからエディタ側で適当な場所にブレークポイントを貼ってデバッグを開始すればOK!

### Neovimのが依存しているソフトウェア

Language Sever, Formatter, Debugger, Fuzzy Finderなどプラグインの枠組を越えて依存しているソフトウェアたち。

- LSP
    - clangd: pacmanでclangをinstall(グローバルに)すると付属してくるのでそれを利用
    - cmake-language-server: uvでPython3.12の仮想環境を作りその環境の中でpipxによってinstall
    - pyright: masonからinstall
    - rust-analyzer: masonからinstall
    - lua-language-server: masonからinstall
    - gols: masonからinstall
    - typesciript-language-server: masonからintall
    - html-language-server: masonからintall
    - texlab: masonからintall
    - julia-language-server: masonからintall

- Formatter
    - clang-format: pacmanでclangをinstalすると付属するのでそれを利用
    - ruff: pipxよりintall
    - go-imports: masonからintall
    - stylua: masonからintall

- Debugger
    - codelldb: AURのcodelldb-binパッケージを利用
    - debugpy: 各Pythonのプロジェクトの仮想環境内にintallしたものを利用する

- Fuzzy Finder
    - fzf-luaがfzfを要求するのでpacmanによってintall

### Neovimでのパンくずリスト

- 参考:
    - [GitHub: SmiteshP/nvim-navic](https://github.com/SmiteshP/nvim-navic)
    - [Zenn: 君もイカしたNeovimで開発してみなイカ？](https://zenn.dev/monicle/articles/3145cea5c283ad)

必要なプラグインは[SmiteshP/nvim-navic](https://github.com/SmiteshP/nvim-navic)で必要な設定は以下

まず表示されるアイコンをVSCode風にするためと深さの制限等の設定のために以下を設定する。
この設定では最終行が重要でvindbarに対して表示内容を書き込んでいる。

```Lua
local navic = require('nvim-navic')
navic.setup {
  icons = {
    File = ' ',
    Module = ' ',
    Namespace = ' ',
    Package = ' ',
    Class = ' ',
    Method = ' ',
    Property = ' ',
    Field = ' ',
    Constructor = ' ',
    Enum = ' ',
    Interface = ' ',
    Function = ' ',
    Variable = ' ',
    Constant = ' ',
    String = ' ',
    Number = ' ',
    Boolean = ' ',
    Array = ' ',
    Object = ' ',
    Key = ' ',
    Null = ' ',
    EnumMember = ' ',
    Struct = ' ',
    Event = ' ',
    Operator = ' ',
    TypeParameter = ' ',
  },
  lsp = {
    auto_attach = true,
  },
  highlight = true,
  depth_limit = 9,
}
vim.o.winbar = "%{%v:lua.require'nvim-navic'.get_location()%}"
```


続いて設定したい言語サーバーに対しても個別に設定する必要がある。
ここではclangd,pyrightの例を示す。

```Lua
-- C++
require('lspconfig').clangd.setup {
  cmd = {
    path_to_clangd,
    '--log=verbose',
    '--background-index',
    '--clang-tidy',
    '--completion-style=detailed',
    '--header-insertion=iwyu',
    '--suggest-missing-includes',
    '--cross-file-rename',
  },
  on_attach = function(client, bufnr)
    navic.attach(client, bufnr)
  end,
}

-- Python
require('lspconfig').pyright.setup {
  settings = {
    python = {
      pythonPath = './.venv/bin/python',
    },
  },
  on_attach = function(client, bufnr)
    navic.attach(client, bufnr)
  end,
}
```

### Pythonのpackageをintallする時の挙動

PyPlに対してある特定のversionのパッケージを要求したときに、そのパッケージがC/C++による拡張モジュールを含んでいる場合を考える。

実はその場合、要求したversionのバイナリが見付からない場合ではソースコードをローカルにfetchしてビルドが走るようになっているはず。

以下はpoetryの場合であるが、以下のパッケージを`poetry install`でintallしようとしたらやけに時間がかかるように感じプロセスを見てみるとgccが`numpy`,`pandas`などのビルドをしているのに気がついた。(今回はビルドに失敗してpackageのintallはできなかった...)

```TOML
[tool.poetry.dependencies]
python = "^3.12"
pyqt6 = "^6.7.1"
matplotlib = "^3.9.1.post1"
pandas = "^2.2.2"
numpy = "^2.0.1"
```

CI/CD環境ではPyPlではこのことを避けるためにミラーを置くなどの方法があるらしい。

### llama.vimを動かしてみる

ローカルでコーディングAIを使う。

- 参考
    - [GitHub: llama.vim](https://github.com/ggml-org/llama.vim)
    - [Hugging Face: ggml-org/Qwen2.5-Coder-3B-Q8_0-GGUF](https://huggingface.co/ggml-org/Qwen2.5-Coder-3B-Q8_0-GGUF)

- Neovim側の準備

```Lua
{
    'ggml-org/llama.vim',
}
```

- llama.cpp側の準備

モデルのdownload

```bash
$ huggingface-cli download \
    ggml-org/Qwen2.5-Coder-3B-Q8_0-GGUF \
    --local-dir ./models
```

サーバーの起動

RAMが16GiB以下の場合は以下の方法が推奨されていた。

```bash
./build_cmake/bin/llama-server\
    -m models/qwen2.5-coder-3b-q8_0.gguf\
    --port 8012 -ngl 99 -fa -ub 1024 -b 1024\
    --ctx-size 0 --cache-reuse 256
```

サーバーを起動した後で、Neovimで適当なソースコードを開いてコーディングを初めると候補が表示されるようになる。

これで,得にハマることなく動いた。

### tarコマンドで特定のファイル、ディレクトリを除いてアーカイブする

Pythonのプロジェクトでの`.venv`やRustでの`target`のような容量の大きいファイルを無視してアーカイブを、
作りたい場合は`--exculude`オプションを用いる。


uvによるPythonのプロジェクトを`.venv`を除いてアーカイブする例

```bash
$ tar -cvzf my_py_poroject.tar.gz --exclude my_py_project/.venv my_py_project
```

また`--exculde`オプションはワイルドカードによるパターンマッチングでのファイルの指定も可能なので`*.log`なファイルを無視したい場合は`--exclude *.log`をすれば良い。

### Arch Linuxでのman

- 参考
    - [Arch Wiki: manページ](https://wiki.archlinux.jp/index.php/Man_%E3%83%9A%E3%83%BC%E3%82%B8)

Arch Linuxではmanの実装としては`man-db`があるが、それだけでは一部見れないページがあるので
`man-pages`も合せてinstallする必要がある。

```bash
$ sudo pacman -S man-pages
```

### KVMに入門する (1)

- 参考
    - [Arch Wiki: KVM](https://wiki.archlinux.jp/index.php/KVM)
    - [Arch Wiki: QEMU](https://wiki.archlinux.jp/index.php/QEMU)
    - [Arch Wiki: libvirt](https://wiki.archlinux.jp/index.php/Libvirt)

まず、マシンがKVMをサポートしているかを確認する

- CPUの仮想化支援機能の有無を調べる

```bash
$ LC_ALL=C lscpu | grep Virtualization
Virtualization:                       VT-x # => OK
```

より詳細な(コアごとの)情報を見たければ`/proc/cpuinfo`以下を確認すれば良い。

```bash
$ grep -E --color=auto 'vmx|svm|0xc0f' /proc/cpuinfo
```

- 続いてKernelがKVMをサポートしているかどうかを確認する。

カーネルモジュール(`kvm`,`kvm_amd` or `kvm_intel`)が使えるかどうかを調べる。

```bash
$ zgrep CONFIG_KVM /proc/config.gz
```

該当するモジュールが`y` or `m`に設定されていればok。

最後に`VIRTIO`モジュールのサポートを確認する。

```bash
$ zgrep VIRTIO /proc/config.gz
```

実際にロードされているか確認する

```bash
$ lsmod | grep kvm # => Ok
$ lsmod | grep virtio # => NG(とりあえず後まわし)
```

- 必要なパッケージをinstall

今回は`QEMU`は全部入りの`qemu-full`をinstallすることにして仮想マシンの操作用に`libvirt`,`virt-manager`,`virt-viewer`もinstallすることにした。

```bash
$ sudo pacman -S qemu-full libvirt virt-manager virt-viewer
```

続いて`libvirt`をroot以外でも使えるようにしてからsystemdでサービスを起動する

```bash
$ sudo usermod -aG libvirt $USER
$ sudo systemctl enable libvirtd.service # 有効化
$ sudo systemctl start libvirtd.service # 起動
```

### KVMに入門する (2)

- 参考
    - [Arch Wiki: libvirt](https://wiki.archlinux.jp/index.php/Libvirt)

virt-managerからゲストのinstallをしようとしたらNAT(ネットワーク)でこけたので...

- 必要だったパッケージ

NATでネットワークに繋げるには`iptables-nft`,`dnsmasq`も必要でした...

`iptables-nft`は`iptables`が入っていたのでinstallしなかった。

```bash
$ sudo pacman -S dnsmasq
```

この後で再起動したらNATが正常に動いた。

### usermod -aG と gpasswd -a

- 参考
    - [vorfee's Tech Blog: ](Linuxユーザーのグループを追加するにはgpasswd)

dockerコマンドなどのように、デフォルトではroot権がないと実行できないがあるグループにuserを追加することによって,
root権なしで実行できるようにすることがある。

dockerの場合では`docker`グループに属するようにするのに

```bash
$ sudo usermod -aG docker $USER
```

を実行する。ただし`usermod`の場合では`-a`を付けない場合では指定したグループには所属するようになるが、これまで所属していたグループからは排除されてしまう。(`-a`を付け忘れるとメンドウなことになる)


同じようなことが可能なコマンドに`gpasswd`がある。このコマンドで同じことをする場合では

```bash
$ gpasswd -a $USER docker
```

を実行する。

### ファイアウォール

- 参考
    - [gihyo.jp: 第76回 Ubuntuのソフトウェアファイアウォール：UFWの利用（1）](https://gihyo.jp/admin/serial/01/ubuntu-recipe/0076)
    - [Zenn: 私的Arch Linuxインストール講座](https://zenn.dev/ytjvdcm/articles/0efb9112468de3)

```bash
$ sudo pacman -S ufw
```

外部からの通信を全て遮断する設定
```bash
$ sudo ufw default deny
$ sudo ufw enable
```

### Masonからinstalされるhtml-ls

MasonからinstallされるHTMLのLanguage-serverである`html-ls`は[hrsh7th/vscode-langservers-extracted](https://github.com/hrsh7th/vscode-langservers-extracted)
を利用している。

serverの実装は[microsoft/vscode-html-languageservice](https://github.com/microsoft/vscode-html-languageservice)なのだが...

### cargo-binstall

crate.ioからリンクを辿ってGitHubのリリースからビルド済みのバイナリをinstallする

- install
```bash
$ cargo install cargo-binstall
```

- 使用例

```bash
$ cargo-binstall texlab # texlabをinstallする場合
```

### Neovimのが依存しているソフトウェア 2.0

- systemのパッケージマネージャーで管理しているもの(/usr/bin)
    - clangd: pacmanでclangをinstallすると付属してくるのでそれを利用
    - clang-format: pacmanでclangをinstalすると付属するのでそれを利用
    - go-imports: pacmanでgo-toolsとしてinstall
    - gopls: pacmanからgoplsとして install
    - codelldb: AURのcodelldb-binパッケージを利用
    - fzf-luaがfzfを要求するのでpacmanによってintall

- rustupで管理 (~/.cargo/bin)
    - rust-analyzer: rustup component add rust-analyzerでinstall

- cargoで管理 (~/.cargo/bin)
    - texlab: cargo-bininstallでinstall
    - stylua: cargo-bininstallでinstall

- npmで管理
    - typesciript-language-server
    - html-language-server

- uvで管理
    - cmake-language-server: uvでPython3.12の仮想環境を作りその環境の中でinstall
    - pyright: masonからinstall
    - ruff: pipxよりintall

- プロジェクトごと
    - debugpy: 各Pythonのプロジェクトの仮想環境内にintallしたものを利用する

- maosn
    - julia-language-server: masonからintall

Node.js, Python系はそれぞれプロジェクトを作成してそこにinstallしNeovim側からPATHを通している

### juliaを削除

install scriptでinstallした場合

```bash
$ juliaup remove  <version> # juliaupで管理しているjuliaを削除
$ juliaup self uninstall # juliaup自身を削除
$ rm -rf ~/.julia # ~/.juliaupは既に削除済み
```

### Pythonがダイナミックロードするライブラリを調べる

ダイナミックリンクするライブラリならば`ldd`コマンドで可能である。
しかし,Pythonでは普通のライブラリを読み込む場合とnumpyのようにC拡張がある場合で実行時にリンクされるライブラリが異なるのでそれを調べたい。

ちゃんとやるならばPython自体をデバッグシンボル付きでビルドしてデバッガで実行時のメモリを見る方法がやるべきではあるが簡易的な方法として`/proc/<PID>/maps`を読む方法がある。

まず、以下のような簡単なプログラムを考える。

`hello.py`

```Python
import time

print("Hello!")
# 3分間スリープ
time.sleep(60 * 3)
```

```bash
$ python hello.py & # 起動
$ cat /proc/$(pgrep -n python)/maps
```

この場合は以下のような結果が得られる。

<details>
<summary>結果</summary>

```bash
613a81a10000-613a81a11000 r--p 00000000 103:02 1489835                   /usr/bin/python3.13
613a81a11000-613a81a12000 r-xp 00001000 103:02 1489835                   /usr/bin/python3.13
613a81a12000-613a81a13000 r--p 00002000 103:02 1489835                   /usr/bin/python3.13
613a81a13000-613a81a14000 r--p 00002000 103:02 1489835                   /usr/bin/python3.13
613a81a14000-613a81a15000 rw-p 00003000 103:02 1489835                   /usr/bin/python3.13
613aa0045000-613aa0115000 rw-p 00000000 00:00 0                          [heap]
7ba4bef00000-7ba4bf000000 rw-p 00000000 00:00 0
7ba4bf000000-7ba4bf3f6000 r--p 00000000 103:02 1478628                   /usr/lib/locale/locale-archive
7ba4bf416000-7ba4bf516000 rw-p 00000000 00:00 0
7ba4bf516000-7ba4bf525000 r--p 00000000 103:02 1445153                   /usr/lib/libm.so.6
7ba4bf525000-7ba4bf5ab000 r-xp 0000f000 103:02 1445153                   /usr/lib/libm.so.6
7ba4bf5ab000-7ba4bf60c000 r--p 00095000 103:02 1445153                   /usr/lib/libm.so.6
7ba4bf60c000-7ba4bf60d000 r--p 000f5000 103:02 1445153                   /usr/lib/libm.so.6
7ba4bf60d000-7ba4bf60e000 rw-p 000f6000 103:02 1445153                   /usr/lib/libm.so.6
7ba4bf60e000-7ba4bf632000 r--p 00000000 103:02 1445141                   /usr/lib/libc.so.6
7ba4bf632000-7ba4bf7a3000 r-xp 00024000 103:02 1445141                   /usr/lib/libc.so.6
7ba4bf7a3000-7ba4bf7f2000 r--p 00195000 103:02 1445141                   /usr/lib/libc.so.6
7ba4bf7f2000-7ba4bf7f6000 r--p 001e3000 103:02 1445141                   /usr/lib/libc.so.6
7ba4bf7f6000-7ba4bf7f8000 rw-p 001e7000 103:02 1445141                   /usr/lib/libc.so.6
7ba4bf7f8000-7ba4bf800000 rw-p 00000000 00:00 0
7ba4bf800000-7ba4bf83a000 r--p 00000000 103:02 1489838                   /usr/lib/libpython3.13.so.1.0
7ba4bf83a000-7ba4bfaf5000 r-xp 0003a000 103:02 1489838                   /usr/lib/libpython3.13.so.1.0
7ba4bfaf5000-7ba4bfc53000 r--p 002f5000 103:02 1489838                   /usr/lib/libpython3.13.so.1.0
7ba4bfc53000-7ba4bfca1000 r--p 00453000 103:02 1489838                   /usr/lib/libpython3.13.so.1.0
7ba4bfca1000-7ba4bfd21000 rw-p 004a1000 103:02 1489838                   /usr/lib/libpython3.13.so.1.0
7ba4bfd21000-7ba4bfd94000 rw-p 00000000 00:00 0
7ba4bfda9000-7ba4bfe0f000 rw-p 00000000 00:00 0
7ba4bfe1d000-7ba4bfe22000 rw-p 00000000 00:00 0
7ba4bfe22000-7ba4bfe23000 rw-p 00000000 00:00 0
7ba4bfe23000-7ba4bfe2a000 r--s 00000000 103:02 1478669                   /usr/lib/gconv/gconv-modules.cache
7ba4bfe2a000-7ba4bfe2c000 r--p 00000000 00:00 0                          [vvar]
7ba4bfe2c000-7ba4bfe2e000 r--p 00000000 00:00 0                          [vvar_vclock]
7ba4bfe2e000-7ba4bfe30000 r-xp 00000000 00:00 0                          [vdso]
7ba4bfe30000-7ba4bfe31000 r--p 00000000 103:02 1445131                   /usr/lib/ld-linux-x86-64.so.2
7ba4bfe31000-7ba4bfe5a000 r-xp 00001000 103:02 1445131                   /usr/lib/ld-linux-x86-64.so.2
7ba4bfe5a000-7ba4bfe65000 r--p 0002a000 103:02 1445131                   /usr/lib/ld-linux-x86-64.so.2
7ba4bfe65000-7ba4bfe67000 r--p 00034000 103:02 1445131                   /usr/lib/ld-linux-x86-64.so.2
7ba4bfe67000-7ba4bfe68000 rw-p 00036000 103:02 1445131                   /usr/lib/ld-linux-x86-64.so.2
7ba4bfe68000-7ba4bfe69000 rw-p 00000000 00:00 0
7ffe63218000-7ffe63239000 rw-p 00000000 00:00 0                          [stack]
ffffffffff600000-ffffffffff601000 --xp 00000000 00:00 0                  [vsyscall]
```

</details>

`/usr/lib/libm.so.6`, `/usr/lib/libpython3.13.so.1.0`などの共有ライブラリが読み込まれていることが分かる。

続いてnumpyをimportする以下の例を見てみる

`import_numpy.py`
```Python
import numpy as np
import time

time.sleep(60 * 3)
```

```bash
$ python import_numpy.py & # 起動
$ cat /proc/$(pgrep -n python)/maps
```

<details>
<summary>結果</summary>

```bash
5cc5f43b9000-5cc5f43ba000 r--p 00000000 103:02 1489835                   /usr/bin/python3.13
5cc5f43ba000-5cc5f43bb000 r-xp 00001000 103:02 1489835                   /usr/bin/python3.13
5cc5f43bb000-5cc5f43bc000 r--p 00002000 103:02 1489835                   /usr/bin/python3.13
5cc5f43bc000-5cc5f43bd000 r--p 00002000 103:02 1489835                   /usr/bin/python3.13
5cc5f43bd000-5cc5f43be000 rw-p 00003000 103:02 1489835                   /usr/bin/python3.13
5cc605cf4000-5cc6060be000 rw-p 00000000 00:00 0                          [heap]
7d571f200000-7d571f23e000 r--p 00000000 103:02 1493170                   /usr/lib/liblapack.so.3.12.0
7d571f23e000-7d571fee9000 r-xp 0003e000 103:02 1493170                   /usr/lib/liblapack.so.3.12.0
7d571fee9000-7d5720041000 r--p 00ce9000 103:02 1493170                   /usr/lib/liblapack.so.3.12.0
7d5720041000-7d5720048000 r--p 00e40000 103:02 1493170                   /usr/lib/liblapack.so.3.12.0
7d5720048000-7d5720049000 rw-p 00e47000 103:02 1493170                   /usr/lib/liblapack.so.3.12.0
7d5720100000-7d5720400000 rw-p 00000000 00:00 0
7d5720400000-7d572041c000 r--p 00000000 103:02 1445954                   /usr/lib/libgfortran.so.5.0.0
7d572041c000-7d57206f9000 r-xp 0001c000 103:02 1445954                   /usr/lib/libgfortran.so.5.0.0
7d57206f9000-7d5720723000 r--p 002f9000 103:02 1445954                   /usr/lib/libgfortran.so.5.0.0
7d5720723000-7d5720725000 r--p 00323000 103:02 1445954                   /usr/lib/libgfortran.so.5.0.0
7d5720725000-7d5720726000 rw-p 00325000 103:02 1445954                   /usr/lib/libgfortran.so.5.0.0
7d5720800000-7d5720897000 r--p 00000000 103:02 1445981                   /usr/lib/libstdc++.so.6.0.33
7d5720897000-7d57209e4000 r-xp 00097000 103:02 1445981                   /usr/lib/libstdc++.so.6.0.33
7d57209e4000-7d5720a74000 r--p 001e4000 103:02 1445981                   /usr/lib/libstdc++.so.6.0.33
7d5720a74000-7d5720a81000 r--p 00274000 103:02 1445981                   /usr/lib/libstdc++.so.6.0.33
7d5720a81000-7d5720a82000 rw-p 00281000 103:02 1445981                   /usr/lib/libstdc++.so.6.0.33
7d5720a82000-7d5720a86000 rw-p 00000000 00:00 0
7d5720a9a000-7d5720c00000 rw-p 00000000 00:00 0
7d5720c00000-7d5720c0e000 r--p 00000000 103:02 1708029                   /usr/lib/python3.13/site-packages/numpy/_core/_multiarray_umath.cpython-313-x86_64-linux-gnu.so
7d5720c0e000-7d57213b7000 r-xp 0000e000 103:02 1708029                   /usr/lib/python3.13/site-packages/numpy/_core/_multiarray_umath.cpython-313-x86_64-linux-gnu.so
7d57213b7000-7d572151b000 r--p 007b7000 103:02 1708029                   /usr/lib/python3.13/site-packages/numpy/_core/_multiarray_umath.cpython-313-x86_64-linux-gnu.so
7d572151b000-7d572151f000 r--p 0091a000 103:02 1708029                   /usr/lib/python3.13/site-packages/numpy/_core/_multiarray_umath.cpython-313-x86_64-linux-gnu.so
7d572151f000-7d5721540000 rw-p 0091e000 103:02 1708029                   /usr/lib/python3.13/site-packages/numpy/_core/_multiarray_umath.cpython-313-x86_64-linux-gnu.so
7d5721540000-7d5721558000 rw-p 00000000 00:00 0
7d5721593000-7d5721595000 r--p 00000000 103:02 1750389                   /usr/lib/python3.13/site-packages/numpy/linalg/_umath_linalg.cpython-313-x86_64-linux-gnu.so
7d5721595000-7d57215ad000 r-xp 00002000 103:02 1750389                   /usr/lib/python3.13/site-packages/numpy/linalg/_umath_linalg.cpython-313-x86_64-linux-gnu.so
7d57215ad000-7d57215b0000 r--p 0001a000 103:02 1750389                   /usr/lib/python3.13/site-packages/numpy/linalg/_umath_linalg.cpython-313-x86_64-linux-gnu.so
7d57215b0000-7d57215b1000 r--p 0001c000 103:02 1750389                   /usr/lib/python3.13/site-packages/numpy/linalg/_umath_linalg.cpython-313-x86_64-linux-gnu.so
7d57215b1000-7d57215b2000 rw-p 0001d000 103:02 1750389                   /usr/lib/python3.13/site-packages/numpy/linalg/_umath_linalg.cpython-313-x86_64-linux-gnu.so
7d57215b2000-7d57215b6000 r--p 00000000 103:02 1728432                   /usr/lib/python3.13/lib-dynload/_ctypes.cpython-313-x86_64-linux-gnu.so
7d57215b6000-7d57215c7000 r-xp 00004000 103:02 1728432                   /usr/lib/python3.13/lib-dynload/_ctypes.cpython-313-x86_64-linux-gnu.so
7d57215c7000-7d57215cd000 r--p 00015000 103:02 1728432                   /usr/lib/python3.13/lib-dynload/_ctypes.cpython-313-x86_64-linux-gnu.so
7d57215cd000-7d57215ce000 r--p 0001b000 103:02 1728432                   /usr/lib/python3.13/lib-dynload/_ctypes.cpython-313-x86_64-linux-gnu.so
7d57215ce000-7d57215d0000 rw-p 0001c000 103:02 1728432                   /usr/lib/python3.13/lib-dynload/_ctypes.cpython-313-x86_64-linux-gnu.so
7d57215d0000-7d57215d4000 r--p 00000000 103:02 1728453                   /usr/lib/python3.13/lib-dynload/_pickle.cpython-313-x86_64-linux-gnu.so
7d57215d4000-7d57215e6000 r-xp 00004000 103:02 1728453                   /usr/lib/python3.13/lib-dynload/_pickle.cpython-313-x86_64-linux-gnu.so
7d57215e6000-7d57215ec000 r--p 00016000 103:02 1728453                   /usr/lib/python3.13/lib-dynload/_pickle.cpython-313-x86_64-linux-gnu.so
7d57215ec000-7d57215ed000 r--p 0001c000 103:02 1728453                   /usr/lib/python3.13/lib-dynload/_pickle.cpython-313-x86_64-linux-gnu.so
7d57215ed000-7d57215ee000 rw-p 0001d000 103:02 1728453                   /usr/lib/python3.13/lib-dynload/_pickle.cpython-313-x86_64-linux-gnu.so
7d5721621000-7d5721624000 r--p 00000000 103:02 1728436                   /usr/lib/python3.13/lib-dynload/_datetime.cpython-313-x86_64-linux-gnu.so
7d5721624000-7d5721638000 r-xp 00003000 103:02 1728436                   /usr/lib/python3.13/lib-dynload/_datetime.cpython-313-x86_64-linux-gnu.so
7d5721638000-7d572163e000 r--p 00017000 103:02 1728436                   /usr/lib/python3.13/lib-dynload/_datetime.cpython-313-x86_64-linux-gnu.so
7d572163e000-7d572163f000 r--p 0001c000 103:02 1728436                   /usr/lib/python3.13/lib-dynload/_datetime.cpython-313-x86_64-linux-gnu.so
7d572163f000-7d5721642000 rw-p 0001d000 103:02 1728436                   /usr/lib/python3.13/lib-dynload/_datetime.cpython-313-x86_64-linux-gnu.so
7d5721642000-7d5721647000 r--p 00000000 103:02 1492793                   /usr/lib/libblas.so.3.12.0
7d5721647000-7d57216f8000 r-xp 00005000 103:02 1492793                   /usr/lib/libblas.so.3.12.0
7d57216f8000-7d57216fe000 r--p 000b6000 103:02 1492793                   /usr/lib/libblas.so.3.12.0
7d57216fe000-7d57216ff000 r--p 000bb000 103:02 1492793                   /usr/lib/libblas.so.3.12.0
7d57216ff000-7d5721700000 rw-p 000bc000 103:02 1492793                   /usr/lib/libblas.so.3.12.0
7d5721700000-7d5721a00000 rw-p 00000000 00:00 0
7d5721a00000-7d5721df6000 r--p 00000000 103:02 1478628                   /usr/lib/locale/locale-archive
7d5721e02000-7d5721e04000 r--p 00000000 103:02 1728465                   /usr/lib/python3.13/lib-dynload/_struct.cpython-313-x86_64-linux-gnu.so
7d5721e04000-7d5721e09000 r-xp 00002000 103:02 1728465                   /usr/lib/python3.13/lib-dynload/_struct.cpython-313-x86_64-linux-gnu.so
7d5721e09000-7d5721e0c000 r--p 00007000 103:02 1728465                   /usr/lib/python3.13/lib-dynload/_struct.cpython-313-x86_64-linux-gnu.so
7d5721e0c000-7d5721e0d000 r--p 0000a000 103:02 1728465                   /usr/lib/python3.13/lib-dynload/_struct.cpython-313-x86_64-linux-gnu.so
7d5721e0d000-7d5721e0e000 rw-p 0000b000 103:02 1728465                   /usr/lib/python3.13/lib-dynload/_struct.cpython-313-x86_64-linux-gnu.so
7d5721e0e000-7d5721e32000 r--p 00000000 103:02 1445141                   /usr/lib/libc.so.6
7d5721e32000-7d5721fa3000 r-xp 00024000 103:02 1445141                   /usr/lib/libc.so.6
7d5721fa3000-7d5721ff2000 r--p 00195000 103:02 1445141                   /usr/lib/libc.so.6
7d5721ff2000-7d5721ff6000 r--p 001e3000 103:02 1445141                   /usr/lib/libc.so.6
7d5721ff6000-7d5721ff8000 rw-p 001e7000 103:02 1445141                   /usr/lib/libc.so.6
7d5721ff8000-7d5722000000 rw-p 00000000 00:00 0
7d5722000000-7d572203a000 r--p 00000000 103:02 1489838                   /usr/lib/libpython3.13.so.1.0
7d572203a000-7d57222f5000 r-xp 0003a000 103:02 1489838                   /usr/lib/libpython3.13.so.1.0
7d57222f5000-7d5722453000 r--p 002f5000 103:02 1489838                   /usr/lib/libpython3.13.so.1.0
7d5722453000-7d57224a1000 r--p 00453000 103:02 1489838                   /usr/lib/libpython3.13.so.1.0
7d57224a1000-7d5722521000 rw-p 004a1000 103:02 1489838                   /usr/lib/libpython3.13.so.1.0
7d5722521000-7d5722594000 rw-p 00000000 00:00 0
7d5722594000-7d5722596000 r--p 00000000 103:02 1459059                   /usr/lib/libffi.so.8.1.4
7d5722596000-7d572259c000 r-xp 00002000 103:02 1459059                   /usr/lib/libffi.so.8.1.4
7d572259c000-7d572259d000 r--p 00008000 103:02 1459059                   /usr/lib/libffi.so.8.1.4
7d572259d000-7d572259e000 r--p 00009000 103:02 1459059                   /usr/lib/libffi.so.8.1.4
7d572259e000-7d572259f000 rw-p 0000a000 103:02 1459059                   /usr/lib/libffi.so.8.1.4
7d572259f000-7d57225a3000 r--p 00000000 103:02 1445948                   /usr/lib/libgcc_s.so.1
7d57225a3000-7d57225c7000 r-xp 00004000 103:02 1445948                   /usr/lib/libgcc_s.so.1
7d57225c7000-7d57225cb000 r--p 00028000 103:02 1445948                   /usr/lib/libgcc_s.so.1
7d57225cb000-7d57225cc000 r--p 0002b000 103:02 1445948                   /usr/lib/libgcc_s.so.1
7d57225cc000-7d57225cd000 rw-p 0002c000 103:02 1445948                   /usr/lib/libgcc_s.so.1
7d57225cd000-7d57225d7000 r--p 00000000 103:02 1492801                   /usr/lib/libcblas.so.3.12.0
7d57225d7000-7d57225fd000 r-xp 0000a000 103:02 1492801                   /usr/lib/libcblas.so.3.12.0
7d57225fd000-7d5722602000 r--p 00030000 103:02 1492801                   /usr/lib/libcblas.so.3.12.0
7d5722602000-7d5722603000 r--p 00035000 103:02 1492801                   /usr/lib/libcblas.so.3.12.0
7d5722603000-7d5722604000 rw-p 00036000 103:02 1492801                   /usr/lib/libcblas.so.3.12.0
7d572260d000-7d572260f000 r--p 00000000 103:02 1728485                   /usr/lib/python3.13/lib-dynload/math.cpython-313-x86_64-linux-gnu.so
7d572260f000-7d5722618000 r-xp 00002000 103:02 1728485                   /usr/lib/python3.13/lib-dynload/math.cpython-313-x86_64-linux-gnu.so
7d5722618000-7d572261d000 r--p 0000b000 103:02 1728485                   /usr/lib/python3.13/lib-dynload/math.cpython-313-x86_64-linux-gnu.so
7d572261d000-7d572261e000 r--p 0000f000 103:02 1728485                   /usr/lib/python3.13/lib-dynload/math.cpython-313-x86_64-linux-gnu.so
7d572261e000-7d572261f000 rw-p 00010000 103:02 1728485                   /usr/lib/python3.13/lib-dynload/math.cpython-313-x86_64-linux-gnu.so
7d572261f000-7d5722683000 rw-p 00000000 00:00 0
7d5722683000-7d5722692000 r--p 00000000 103:02 1445153                   /usr/lib/libm.so.6
7d5722692000-7d5722718000 r-xp 0000f000 103:02 1445153                   /usr/lib/libm.so.6
7d5722718000-7d5722779000 r--p 00095000 103:02 1445153                   /usr/lib/libm.so.6
7d5722779000-7d572277a000 r--p 000f5000 103:02 1445153                   /usr/lib/libm.so.6
7d572277a000-7d572277b000 rw-p 000f6000 103:02 1445153                   /usr/lib/libm.so.6
7d572277b000-7d572277d000 rw-p 00000000 00:00 0
7d5722786000-7d5722787000 r--p 00000000 103:02 1728430                   /usr/lib/python3.13/lib-dynload/_contextvars.cpython-313-x86_64-linux-gnu.so
7d5722787000-7d5722788000 r-xp 00001000 103:02 1728430                   /usr/lib/python3.13/lib-dynload/_contextvars.cpython-313-x86_64-linux-gnu.so
7d5722788000-7d5722789000 r--p 00002000 103:02 1728430                   /usr/lib/python3.13/lib-dynload/_contextvars.cpython-313-x86_64-linux-gnu.so
7d5722789000-7d572278a000 r--p 00002000 103:02 1728430                   /usr/lib/python3.13/lib-dynload/_contextvars.cpython-313-x86_64-linux-gnu.so
7d572278a000-7d572278b000 rw-p 00003000 103:02 1728430                   /usr/lib/python3.13/lib-dynload/_contextvars.cpython-313-x86_64-linux-gnu.so
7d572278b000-7d5722790000 rw-p 00000000 00:00 0
7d5722790000-7d5722791000 rw-p 00000000 00:00 0
7d5722791000-7d5722798000 r--s 00000000 103:02 1478669                   /usr/lib/gconv/gconv-modules.cache
7d5722798000-7d572279a000 r--p 00000000 00:00 0                          [vvar]
7d572279a000-7d572279c000 r--p 00000000 00:00 0                          [vvar_vclock]
7d572279c000-7d572279e000 r-xp 00000000 00:00 0                          [vdso]
7d572279e000-7d572279f000 r--p 00000000 103:02 1445131                   /usr/lib/ld-linux-x86-64.so.2
7d572279f000-7d57227c8000 r-xp 00001000 103:02 1445131                   /usr/lib/ld-linux-x86-64.so.2
7d57227c8000-7d57227d3000 r--p 0002a000 103:02 1445131                   /usr/lib/ld-linux-x86-64.so.2
7d57227d3000-7d57227d5000 r--p 00034000 103:02 1445131                   /usr/lib/ld-linux-x86-64.so.2
7d57227d5000-7d57227d6000 rw-p 00036000 103:02 1445131                   /usr/lib/ld-linux-x86-64.so.2
7d57227d6000-7d57227d7000 rw-p 00000000 00:00 0
7fff71796000-7fff717b7000 rw-p 00000000 00:00 0                          [stack]
ffffffffff600000-ffffffffff601000 --xp 00000000 00:00 0                  [vsyscall]
```

</details>

この場合では`/usr/lib/liblapack.so.3.12.0`,`/usr/lib/libgfortran.so.5.0.0`,`/usr/lib/libstdc++.so.6.0.33`,`/usr/lib/libblas.so.3.12.0`
などの`hello.py`では見られなかった共有ライブラリが読みこまれていることが分かる。
