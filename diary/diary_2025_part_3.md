### LSPの設定を見直した

- 参考:
    - [Zenn: Neovim+LSPをなるべく簡単な設定で構築する](https://zenn.dev/botamotch/articles/21073d78bc68bf)

LSPの補完周りの設定を整理した、またビルドインのLSPの設定を以下のように追加した。

```Lua
-- hover
vim.keymap.set('n', 'gh', '<cmd>:lua vim.lsp.buf.hover()<CR>')
-- show ref all
vim.keymap.set('n', 'gr', '<cmd>:lua vim.lsp.buf.references()<CR>')
-- rename all
vim.keymap.set('n', 'gn', '<cmd>:lua vim.lsp.buf.rename()<CR>')
```

上からカーソルがある位置の変数の情報を表示、カーソルがある変数の一覧を表示してその場所にジャンプできるようにする、カーソルがある変数をすべてrenameする

### PulseAudioのGUI

PulseAudioを使う場合やPipeWire環境下でPuseAudioを使えるようにした環境(`pipewire-pulse`環境)でのPulseAudioのGUIフロントエンド => `pavucontrol`を使おう

```bash
$ sudo pacman -S pavucontrol
```

### rust-analyzerが空のソースでクラッシュする問題 => nigthlyで解決したっぽい

[Issue](https://github.com/rust-lang/rust-analyzer/issues/17289)による直近の[nightly-release](https://github.com/neovim/neovim/releases/tag/nightly)でとどうやら解決したらしい。

そのバージョンを動かすには以下のようにする

```bash
$ wget https://github.com/neovim/neovim/releases/download/nightly/nvim-linux-x86_64.tar.gz # バイナリをダウンロード
$ tar -xvf nvim-linux-x86_64.tar.gz # 解凍
$ mv nvim-linux-x86_64 ~/.nvim-linux-x86_64_nightly # 適当な名前で$HOME以下に置く
```

最後にPATHを通すいつもはbobで管理していて`~/.local/share/bob/nvim-bin`を.bashrcにてPATHを通しているが`~/.nvim-linux-x86_64_nightly/bin`に変更する

```bash
export PATH="$HOME/.nvim-linux-x86_64_nightly/bin:$PATH"
```

適当に空のソースコードを読みこんで試したところ確かに正常に動作した。

### pacmanで依存関係を無視して特定のパッケージだけをinstallする

普通、pacmanで特定のパッケージを`pacman -S <pkg name>` でinstallしようとすると依存するパッケージもinstallされる。

しかし特定のパッケージのみinstallしたい場合では`-dd`オプションによってそれが可能である。

```bash
$ sudo pacman -S -dd <pkg name>
```
### NeovimのGUIインターフェースneovide

NeovimのGUIインターフェースのneovideをinstallして使用してみた。

- install

GitHubのリリースからバイナリをdownloadして解凍して適当なPATHの通った場所に置く。

```bash
$ wget https://github.com/neovide/neovide/releases/download/0.14.1/neovide-linux-x86_64.tar.gz
$ tar -xvf neovide-linux-x86_64.tar.gz
$ mv neovide ~/.local/bin # ここでは$HOME/.local/binに置いた
```

- launcharから起動できるようにする

以下のようにデスクトップエントリを書き`~/.local/share/applications`に置く(もしくは適当な場所に置いてそこへリンクを貼る)

neovide.desktop
```txt
[Desktop Entry]
Type=Application
Exec=/home/aki/.neovide/neovide --fork --neovim-bin /home/aki/.nvim-linux-x86_64_nightly/bin/nvim
Icon=/home/aki/.neovide/neovide.png
Name=Neovide
Keywords=Text;Editor
Categories=Utility;TextEditor;
Comment=No Nonsense Neovim Client in Rust
MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;
```

ポイントは起動スクリプトで`-fork`を付けることと、`--neovim-bin <PATH TO BIN>`で実行するNeovimへのPATHを指定する点である。
というのもNeovim自体を$HOME下に追いているためneovideが見つけることができないためである(考えられる)。

### poetrで`[virtualenvs]`を`in-project = true`をしなかった場合にin-projectに変更したい

poetry.tomlを以下のように準備する

```TOML
[virtualenvs]
in-project = true
```

仮想環境は

```bash
$ poetry env info -p
```
で確認できるので目的の仮想環境を`rm -r`で削除してから`uv install`すればOK。

### Markdownのlanguage-serverのmarkdown-oxideを導入する

- 参考
    - [GitHub: Feel-ix-343/markdown-oxide](https://github.com/Feel-ix-343/markdown-oxide)
    - [MINERVIA: 📜2024-08-19 markdown-oxideをNeovimで使ってみる](https://minerva.mamansoft.net/Notes/%F0%9F%93%9C2024-08-19+markdown-oxide%E3%82%92Neovim%E3%81%A7%E4%BD%BF%E3%81%A3%E3%81%A6%E3%81%BF%E3%82%8B)


LSPを使うとコード内のシンボルをマッピングできて便利なのでMarkdownでも導入してみようと考えたので...

- LSP本体をinstall

cargo-binstallでバイナリをダウンロード
```bash
$ cargo binstall --git 'https://github.com/feel-ix-343/markdown-oxide' markdown-oxide
```

Neovim側は以下のように設定した

```Lua
-- for markdown-oxide
-- An example nvim-lspconfig capabilities setting
local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())

require('lspconfig').markdown_oxide.setup({
  -- Ensure that dynamicRegistration is enabled! This allows the LS to take into account actions like the
  -- Create Unresolved File code action, resolving completions for unindexed code blocks, ...
  capabilities = vim.tbl_deep_extend('force', capabilities, {
    workspace = {
      didChangeWatchedFiles = {
        dynamicRegistration = true,
      },
    },
  }),
  --on_attach = on_attach, -- configure your on attach config
})
```

補完の設定を`nvim-cmp`で設定しなければならないのだが、それはまた後で...

### memo: Go製のツールのinstallが標準パッケージできるようになった

Go製のツールのinstallと言えばaquaが有名だが、Go標準の機能でもそれができるようになったという話。

[焼売飯店 GoとかTSとか、JSとか: Go 1.24で入ったGo製ツールの管理機能が便利だったのでおすすめしたい](https://blog.syum.ai/entry/2025/03/01/235814)

### FreeBSD install battle (1)

- 参考
    - [FreeBSD: Chapter2. Installing FreeBSD](https://docs.freebsd.org/en/books/handbook/bsdinstall/)

今回はArch Linux上のVirtual Boxで行う

まず、イメージをdownloadしてsumcheckの検証を行う

```bash
# download
$ wget https://download.freebsd.org/releases/amd64/amd64/ISO-IMAGES/14.2/FreeBSD-14.2-RELEASE-amd64-disc1.iso
$ wget https://download.freebsd.org/releases/amd64/amd64/ISO-IMAGES/14.2/CHECKSUM.SHA256-FreeBSD-14.2-RELEASE-amd64
$ wget https://download.freebsd.org/releases/amd64/amd64/ISO-IMAGES/14.2/CHECKSUM.SHA512-FreeBSD-14.2-RELEASE-amd64
# 検証
$ sha256sum -c CHECKSUM.SHA256-FreeBSD-14.2-RELEASE-amd64 FreeBSD-14.2-RELEASE-amd64-disc1.iso
$ sha512sum -c CHECKSUM.SHA512-FreeBSD-14.2-RELEASE-amd64 FreeBSD-14.2-RELEASE-amd64-disc1.iso
```

Virtual Boxで以下のような構成で設定してisoをファイルを読み込ませればOK

- マシン構成
    - ストレージ: 16GiB
    - RAM: 2GiB
    - efi: true

後はデフォルトで

これでinstallerがブートできるようになった。

### FreeBSD install battle (2)

\#\# installの詳細は後で載せる

とりあえずinstallできたので`fastfetch`を入れる

```sh
# pkg update
# pkg search fastfetch # fastfetchがあるか探す
# pkg install fastfetch # fastfetchをinstall
```

こんなかんじ!
<img src="./fig/install_freebsd_cliped.png"/>

このままでは一般userではroot権を要する作業が一切できないので`sudo` を使えるようにする

```sh
# pkg install sudo
```

`/usr/local/etc/sudoers`を編集して

```txt
<user name> ALL=(ALL:ALL) ALL
```

を追加して保存する

とりあえずは`pkg`でビルド済みのバイナリを入れるということで`ports`の導入はまた後で...

後で [FreeBSD: Chapter 4. Installing Applications: Packages and Ports](https://docs.freebsd.org/en/books/handbook/ports/#ports-using) を参考にして導入する。


X11を入れる

```sh
$ sudo pkg install xorg
```

\#\# 今のところここでハマっている。

参考にしているのは以下

- 参考:
    - [FreeBSD: Chapter 8. Desktop Environments](https://docs.freebsd.org/en/books/handbook/desktop/)
    - [FreeBSD: Chapter 5. The X Window System](https://docs.freebsd.org/en/books/handbook/x11/)

### slコマンドとsignal

`sl`コマンドは`SIGINT`を無視するように作られているとのことで、ソースを読んでみました。

- ソースコードをcloneする

```bash
$ git https://github.com/mtoyoda/sl.git
$ nvim sl.c
```

`sl.c`の中の`main()`を見てみると`signal(SIGINT, SIG_IGN)`で`SIGINT`を`SIG_IGN`で無視するようにしてた。
そこでその行をコメントアウトしてからビルドしてみる。

```bash
$ nvim sl.c # signal(SIGINT, SIG_IGN)をコメントアウト
$ make
$ ./sl
```

この場合では`SIGINT`を無視しないようになるので(デフォルトの挙動)`Ctrl + c`で終了できるようになった。
