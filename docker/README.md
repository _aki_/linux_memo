# Dockerのメモ

## install & 有効化

- 参考
    - [dockerdocs: Linux post-installation steps for Docker Engine](https://docs.docker.com/engine/install/linux-postinstall/)

for Arch Linux
```bash
$ sudo pacman -S docker
```

dockerはデーモンを走らせる必要があるので以下を実行する
```bash
$ sudo systemctl start docker.service
$ systemctl status # docker.serviceを確認
```

デフォルトでは`docker`コマンドの実行にroot権が必要なので自分を`docker` groupに追加することでroot権なしで実行できるようにする。

```bash
$ sudo usermod -aG docker $USER
```
その後でログアウトしてから再ログインするとrootなしで`docker`を実行することが可能になる(VMの場合は必須)。

もしくは`newgrp`コマンドで更新されたgroupにログインし直す。

```bash
$ newgrp docker
```

## よく使うコマンド

- docker run: コンテナの実行
- docker stop: コンテナの停止
- docker rm: コンテナの削除
- docker ps: コンテナの確認
- docker exec: コンテナ上でコマンド実行
- docker build: Dockerfileからイメージをビルドする
- docker images: コンテナイメージをの確認
- docker rmi: コンテナイメージの削除
- docker logs: コンテナログの確認
- docker contaniner pune: 停止コンテナの削除
- docker images pune: 未使用イメージの削除

## 基本的な操作

- とりあえず起動して入る
```bash
$ docker run --name <name you want to name> -it <image name> <image name> /bin/bash
```

- 一旦バックグラウンドで起動して後からログインする
```bash
$ docker run -d --name <name you want to name>  -p <ip ip address @ host>:<ipaddress @ container> <container name>
$ docker exec -it <container name> /bin/bash
```

- コンテナを停止

```bash
$ docker stop <name of container or container ID>
```

- コンテナを再開する
```bash
$ docker start <name of container or container ID>
```

- 実行中のコンテナの確認
```bash
$ docker ps
```

- 停止中のコンテナの確認
```bash
$ docker ps -a
```

- コンテナの削除
```bash
$ docker rm <container name>
```

- イメージの削除
```bash
$ docker rmi <image name>
```

- ホストとのファイル及びポートの共有

```bash
$ docker run -d --name <name you want tot name>\
	-p <ip ip address @ host>:<ipaddress @ container>\
	-v <path to mount @ host>:<path @ container>\
	<container name>
```

## イメージが保存される場所を変更する

- 参考
    - [Zenn: Dockerイメージの格納場所を変更する方法](https://zenn.dev/karaage0703/articles/46195947629c35)
    - [GitHub: docker image保存先を外部ストレージに変更する](https://github.com/atinfinity/lab/wiki/change_docker_image_directory)
    - [stackoverflow: How to change the docker image installation directory?](https://stackoverflow.com/questions/24309526/how-to-change-the-docker-image-installation-directory)

Dockerはデフォルトではイメージを`/var/lib/docker`に保存するが、
筆者の環境では`/var`にそれほど容量を割り振っていないのですぐにいっぱいいっぱいになりがちだった。

筆者は、`~/.docker`以下に保存されるようにした。

```bash
$ sudo systemctl stop docker.service # デーモンの停止
$ cd && mkdir .docker # ディレクトリの作成
$ sudo cp -a /var/lib/docker .docker/ # データのコピー
```

続いて、dockerの設定を変更する
```bash
$ sudo vim /lib/systemd/system/docker.service
```

変更前
```txt
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
```

↓

変更後(`--data-root="/home/aki/.docker/docker"`を追加した)
```txt
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock --data-root="/home/aki/.docker/docker"
```

デーモンをリロードして、dockerのデーモンを再び立ち上げる。
```bash
$ sudo systemctl daemon-reload
$ sudo systemctl start docker.service
```

最後に、変更が有効になっているか確認する
```bash
$ docker info | grep 'Docker Root Dir'
```
以下のように無事変更されていたらok
```txt
Docker Root Dir: /home/aki/.docker/docker
```

もともとのデータはもう移行済みでいらないので削除する

```bash
$ sudo rm -rf /var/lib/docker
```

## doker run -d \<image_name\>でコンテナが起動しない場合

```bash
$ docker run -d <image_name> tail -f > /dev/null
```

とする。
