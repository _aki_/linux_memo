## du コマンド

- `-B<K,M,G>`
    - ブロックサイズ(K,M,G)

- `-s`, `--summarize`
    - トータルの容量のみ表示

- `--total`
    - トータルの容量も表示

- `--exclude=pattern`
    - `pattern`を除く
