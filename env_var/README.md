# 環境変数のメモ

参考:[arch wiki 環境変数](https://wiki.archlinux.jp/index.php/%E7%92%B0%E5%A2%83%E5%A4%89%E6%95%B0)

## グローバル

/etc以下に格納されているファイルに環境変数に格納する。

- /etc/environment
    - シェルに依存しない
- /etc/profile
    - ログインシェルにのみおいて初期化される。
- /etc/security/pam\_env.conf, /etc/environment 
    - PAMモジュールpam\_env(8)が環境変数を読み込む
    - ~/.profile, ~/.bash\_profile, ~/.zshenvよりも前に読み込まれる
    - 非推奨の ~/.pam\_environmentは読み込まれない(将来的には廃止するらしい)

## ユーザーごと

環境変数をグローバルに設定したくない場合。
例えば、PATHに/home/my\_user/binを追加したいが他のユーザーには同じPATHを使ってほしくない場合がある。
そういう場合では、様々なファイルを用いることでローカルに(スコープを制限して)環境変数を設定することができる。


- bash, zsh等のシェルの設定ファイル
    - ローカルでPATHにあるディレクトリを入れたい場合~/.bash_profileに

```bash
export PATH="${PATH}:/home/my_user/bin"
```

と追加する。.bash\_profileを更新した場合は再ログインするか、

```bash
$ source ~/.bash_profile
```
とする。

- systemdユーザー環境変数
    - ~/.config/environment.d/\*.confから設定が読み込まれる。

## グラフィック環境

環境変数がグラフィカルなアプリケーションにしか影響を与えない場合では、グラフィカルセッション内でのみ設定してその範囲を制限することが可能である。

- デスクトップ環境ごと
    - KDE Plasmaはログイン時のシェルスクリプトの実行をサポートしているため、環境変数の設定に利用できる。

- xorgのセッションごと
    - ~/.xprofile , ~/.xinitrc, ~/.xsession
    - startx,SLiMは ~/.xinitrcを実行する
    - XDMは ~/.xsessionを実行する

以下のように書き込む

```
export GUI_var=value
```
- Waylandのセッションごと
    - xorg関連のファイルを起動時に読み込まないのでGDMとKDE Plasmaが代わりにsystemd/ユーザー環境変数をソースとする 

~/.config/environment.d/envvars.confに以下のように書き込む

```
GUI_VAR=value
```

なお、SDDMのようなWaylandセッションをサポートする一部のディスプレイマネージャではこの方法をサポートしていない。

- セッションごと
    - ごく短時間だけその環境変数を有効にしたい場合はファイルには書き込まずにシェルで

```bash
$ export PATH="${PATH}:/home/my_user/tmp/usr/bin"
```

として現在のセッションのみ有効となるようにする。


- アプリケーションごと
    - Steamのゲームなど

## Appendix

### bashの設定ファイル

- /etc/profile
    - Sourceのアプリケーション設定を/etc/profile.d/\*.shと/etc/bash.bashrcに記述しています。

- ~/.bash\_profile
    - ユーザーごと/etc/profileよりも後に読み込まれる。
    - このファイルが存在しない場合~/.bashr\_login, ~/.profileの順にチェックされる。 

- ~/.bashr\_logout
    - ユーザー単位でログインシェルが終了した後で読み込まれる。

- /etc/bash.bash\_logout 
    - コンパイラフラグに依存する。ログインシェルの終了後

- /etc/bash.bashrc
    - コンパイラフラグに依存する

- ~/.bashrc 
    - ユーザー単位で/etc/bash.bashrcの後で読み込まれる。

