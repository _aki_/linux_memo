# git & GitHubのメモ

## gitのbranchの流れを可視化するツール

自分は基本的にはCLIのgitを用いて作業している。branchの変化をグラフで可視化したいときは以下のコマンドで可視化することができる。

```bash
$ git log --graph
```

しかし、あまり見やすいとは言えない。そこで、これをより見やすくするツールとしては、`vscode`の`Git Graph`プラグインを使うと良い。

## リモートリポジトリから特定のブランチを引っ張ってくるとき

別のpcでブランチを切って作業していて、GitHub等のリモートリポジトリに各ブランチをpushしているとする。

ここで、別のpcからリポジトリをcloneしても、基本的にはmainブランチのみがcloneされる。
ここに、他のブランチも引っ張ってきたいとする。このときは以下のコマンドを実行すれば良い。

```bash
$ git checkout -b <branch_name> origin/<branch_name>
```

すべてのブランチを見たいならば、`-a`,`--all`オプションを付けて`git branch`を実行すれば良い。

```bash
$ git branch -a
$ git branch -all
```

## rename後の履歴を追跡する

gitで管理されれているファイルの名前変更を行うには

```bash
$ git mv <original name> <new name>
```

とするが、名前変更後のファイルに対して名前を変更する前のログを追跡するには

```bash
$git log --follow <file name>
```

とオプション `--follow` を用いる。

## リポジトリを分割,統合

### サブディレクトリを別のリポジトリとして取り出す

- 参考
    - [Qiita: Git リポジトリを分割する](https://qiita.com/toshikish/items/3529f75c511a65723798)
    - [Qiita: Git で複数のリポジトリをまとめたり、逆に切り出したりする](https://qiita.com/uasi/items/77d41698630fef012f82)


以下の状況を考える
```txt
gomi
├── fuga
├── hoge
└── sub_dir
    └── sub_hoge
```

このリポジトリから`sub_dir`の内容を別のリポジトリとして分離することを考える

この場合は`git-filter-repo`を用いる
というのも、`git`に標準の`git filter-branch`は非推奨で変わりに`git-filter-repo`を使うことを推奨されているためである。

```bash
$ sudo pacman -S git-filter-repo
```

```bash
$ cd sub_dir
$ git-filter-repo --force --subdirectory-filter sub_dir
```

ここで、今回は実験用にローカルに適当なリポジトリを作成したが、本来はリモートからクローンしたものを使うことが推奨されている。
そのためコマンド中では`--force`オプションを使って強制的に実行している。

これを実行するとディレクトリ内は以下のようになっている。

```txt
gomi
├── sub_dir
└── sub_hoge
```

ここで、`sub_dir`はコマンド実行前までは`sub_hoge`を格納していたディレクトリで現在はからのディレクトリとなっている。

最後に空のディレクトリ`sub_dir`を削除してプロジェクトのrootの`gomi`を適当な名前に変更すれば良い。

## branch名をrenameする

```bash
$ git switch <old branch name>
$ git branch -m <new branch name>
```

## sshのconfig

複数の鍵で複数の接続先を扱う場合は必要？

```txt
Host github.com
    HostName github.com
    User git
    port 22
    IdentityFile ~/.ssh/id_ed25519
    IdentitiesOnly yes

Host gitlab.com
    HostName gitlab.com
    User git
    port 22
    IdentityFile ~/.ssh/id_gitlab
    IdentitiesOnly yes
```

## cli tools

GitHub CLI, GitLab CLIのメモ

- [GitHub CLI](https://gitlab.com/_aki_/learn_gh)
- [GitLab CLI](https://gitlab.com/_aki_/learn_glab)
