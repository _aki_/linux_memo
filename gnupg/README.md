# \*.isoファイルの検証

## Debian 12.05-adm64のisoファイルの サムチェックの確認

- 参考:
    - [Debian GNU/Linux Installation Guide: 4.7. Verifying the integrity of installation files]( https://www.debian.org/releases/bookworm/amd64/ch04s07.en.html)
    - [Verifying authenticity of Debian images](https://www.debian.org/CD/verify.en.html)
    - [Arch Wiki: GnuPG](https://wiki.archlinux.org/title/GnuPG)
    - [Debian Public Key Server](https://keyring.debian.org/)
    - [How to verify Debian CD or DVD image using GPG](https://www.cyberciti.biz/faq/verify-downladed-debian-linux-cd-dvd-image-using-gpg/)
    - [ダウンロードしたイメージを検証する](https://alt.fedoraproject.org/ja/verify.html)

`Debian12.05.0-amd64-netinst.iso`をダウンロードするときに一緒に`SHA512SUMS`もダウンロードする。

`shasum512sums`コマンドを実行する

```bash
$ sha512sum *.iso
```

表示されるサムチェックを`SHA512SUMS`と比較する

```bash
$ cat SHA512SUMS
```

### サムチェックの署名の検証

要`Gnupg`

- install
```bash
$ sudo pacman -S gnupg
```

Live CDイメージ用の公開鍵をDebianの鍵サーバーよりimportする

```bash
$ gpg --recv-key --keyserver keyring.debian.org 6294BE9B
```

チェックする

```bash
$ gpg list-keys
```
uidが
```
uid           [ unknown] Debian CD signing key <debian-cd@lists.debian.org>
```

となっている鍵が見つかればok

最後にサムチェックファイル`SHA512SUMS`を`SHA512SUMS.sign`で検証する

```bash
$ ggp --verify SHA512SUMS.sign SHA512SUMS
```

Good signatureと出ればok

## Ubuntuの\*.isoファイルの検証

- 参考
    - [How to verify your Ubuntu download](https://ubuntu.com/tutorials/how-to-verify-ubuntu#1-overview)

基本的にはDebianと同じはず。ここでは`ubuntu-20.04.6-desktop-amd64.iso`を検証する

まずisoイメージをダウンロードするときに一緒に `SHA256SUMS`, `SHA256SUMS.pgp`もダウンロードする

### サムチェックの確認

```bash
$ sha256sum *.iso
```

として`SHA256SUMS`と一致するか確認する

より簡単な方法としては`-c`の比較オプション使って

```bash
$ sha256sum -c SHA256SUMS
```

を実行して、目的の\*.isoファイルに対してOKと出ればok

<!-- あとで書き直す -->
<!-- 

keyをimportする

```bash
$ gpg --keyid-format long --keyserver hkp://keyserver.ubuntu.com --recv-keys 0x46181433FBB75451 0xD94AA3F0EFE21092
```

サムチェックファイルの署名を確認するには

-->

## Arch Linuxの\*.isoファイルの検証

サムチェックの確認は`-c`オプションを用いて

```bash
$ sha256sum -c sha256sum.txt
```

を実行する。

\*.isoイメージの検証はまず

```bash
$ gpg --auto-key-locate clear,wkd -v --locate-external-key pierre@archlinux.org
```

で鍵を取得して

```bash
$ gpg --keyserver-options auto-key-retrieve --verify archlinux-yyyy.mm.dd-x86_64.iso.sig archlinux-yyyy.mm.dd-x86_64.iso
```

を実行することで検証を行う。
