# grub の設定

## grub themeのインストール

```
$ yay -S grub-theme-vimix
```

インストールした後で`/etc/default/grub`に以下のように設定を加える。

```txt
GRUB_THEME="/usr/share/grub/themes/Vimix/theme.txt"
```

GitHubを見ると`/boot/grub/themes/Vimix/theme.txt`となっているが、実際にそこにはインストールされず、

`usr/share/grub/themes` 以下にインストールされている。

## grubの設定を有効化

```bash
$ sudo grub-mkconfig -o /boot/grub/grub.cfg
```
