# HITRANのメモ

HITRANのAPIには第一世代のAPIと第二世代のAPIの二種類が存在し、第一世代はシンプルなHTTPリクエストだが、第二世代はREST APIでアクセスにはAPI Keyが必要である。

## 第一世代 Python API

- 参考
    - [公式: HITRAN online HAPI: The HITRAN Application Programming Interface ](https://hitran.org/hapi/)
    - [公式APIドキュメント: HITRAN Application Programming Interface(HAPI)](https://hitran.org/static/hapi/hapi_manual.pdf)
    - [GitHub: hitraonline/hapi ](https://github.com/hitranonline/hapi)
    - [Qiita: HITRAN データベースの API, HAPI の使い方](https://qiita.com/alphya/items/794b1e792469274d7561)
    - [Qiita: HITRANデータベースのAPIであるHAPIを使用した放射スペクトルの計算](https://qiita.com/syoukera/items/c52eabbc63d3e730953f)

### 環境構築

HITRANのAPIを叩くには`hitran-api`パッケージを使用する。
その他諸々解析で使いそうなツールをinstallする。

```bash
$ poetry init
$ poetry config --local virtualenvs.in-project true
$ poetry add pyqt6 numpy matplotlib
$ poetry add hitran-api
```

とりあえずH$_2$Oの 10000 cm$^{-1}$から 10100 cm$^{-1}$ のデータをdownloadしてみる
```Python
import hapi

def main() -> None:
    hapi.db_begin('data')
    hapi.fetch('H2O', 1, 1, 10000, 10100)

if __name__ == "__main__":
    main()
```

これを実行すると`data`ディレクトリ下に H$_2$O のデータがdownloadされる。
