## 必要なパッケージのインストール

### Xとi3

参考: (1) [arch wiki i3](https://wiki.archlinux.jp/index.php/I3)
(2) [i3 official site](https://i3wm.org/)


i3はX上で動くので`i3`だけでなく、`Xorg`も必要である

```bash
$ sudo pacman -S xorg-server xorg-apps i3-wm
```

### ディスプレイマネージャー＆greeter
(1)[arch wiki lightdm] (https://wiki.archlinux.jp/index.php/LightDM)

xfceで使い慣れたものを用いる。
ディスプレイマネージャには`lightdm`ログイン時のグリーターには `lightdm-gtk-greeter`を用いる。

```bash
$ sudo pacman -S lightdm lightdm-gtk-greeter
```
インストール後に`/etc/lightdm/lightdm.conf`
を以下のように編集する

```
[Seat:*]
greeter-session=example-gtk-gnome
↓
[Seat:*]
greeter-session=lightdm-gtk-greeter
```

設定後にデーモンを登録する。

```bash
$ sudo systemctl enable lightdm.service
```

### ターミナル

ターミナルエミュレータはとりあえず、使い慣れた`xfce4-terminal`

```bash
$ sudo pacman -S xfce4-terminal
```

## 日本語入力

ここでは`fcitx-mozc`を使う。

まず、必要なパッケージをインストールする。
```bash
$ sudo pacman -S fcitx5-im fcitx5-mozc
```

次に環境変数を設定する。
今回は`/etc/environment`に以下を書き込む

```
export GTK_IM_MODULE DEFAULT=fcitx
export QT_IM_MODULE DEFAULT=fcitx
export XMODIFIERS DEFAULT=@im=fcitx
```

`fcitx5`が自動起動するように`~/.config/i3/config`に以下のように書き込む。

```
exec fcitx5
```

これを設定後に、`fcitx5-configration`を起動して上から
keyboard-English(US),Mozcのように並ぶようにする。

最後に日本語、絵文字のフォントを入れる。

```bash
$ sudo pacman -S adobe-source-han-sans-jp-fonts noto-fonts-emoji 
```

## オーディオ

参考:[arch wiki alsa](s://wiki.archlinux.jp/index.php/Advanced_Linux_Sound_Architectur )


オーディオを扱えるようにするようにまず`alsa-utiles`,`puluseaudio` を入れる。
調整をパネル(後述)からできるように`xfce4-pulseaudio-plugin`を入れる。

```bash
$ sudo pacman -S alsa-utiles puluseaudio xfce4-pulseaudio-plugin
```

## ポインティングデバイス

参考:[arch wiki libinput](https://wiki.archlinux.jp/index.php/Libinput) 


デフォルトでは、トラックパッドが使いにくい。
そのため、設定を変える必要がある。

`libinput`を使う。`libinput`はWayland向けであるので、Xorgでは`xf86-input-libinput`を使う。

```bash
$ sudo pacman -S xf86-input-libinput xorg-xinput
```

`/etc/X11/xorg.conf/30-touchpad.conf`に以下の設定を書き込む

```
section "InputClass"
    Indentifier "touchpad"
    MatchDriver "libinput"
    MatchIsTouchpad "on"
    option "Tapping" "on"
    option "NaturalScrolling" "true"
    option "DisableWhileTyping" "true"
EndSection
```

また`~/.config/i3/config`にも以下の設定を書き込む。

```
exec xinput set-prop 20 355 1
```

この設定はタッチパッドのタップでクリック、スクロール方向の設定を含んでいる。

## ディスプレイ設定 

### ディスプレイのロック

参考:[arch wiki lightdm](https://wiki.archlinux.jp/index.php/LightDM)

`LitghtDm`に組み込みのスクリーンロッカーは非推奨であるので`light-locker`を使う。

```
$ sudo pacman -S light-locker
```

ディスプレイをロックするには、`light-locker`が起動している状態で

```
$ light-locker-command -l
```

とする。しかし、これを毎回実行するのはめんどくさいので
`~/.bashrc`に以下のaliasを書き込みlockでロックできるようにする。

```
# for lock screen
alias lock='light-locker-command -l'
```

### ディスプレイの切り替え

参考:[arch wiki xrandr](https://wiki.archlinux.jp/index.php/Xrandr)


`xrandr`を使う。

```
$ sudo pacman -S xorg-xrandr
```

まず、以下のコマンドで、可能な出力先を出力する。

```
$ xrandr
```

例えば、HDMIにつなげたければ以下のようにする。

```bash
$ xrandr --output HDMI-1 --auto
```

`--auto`では解像度がオートである。解像度を指定したければ、`--mode 1920x1080`など指定する

```bash
$ xrandr --output HDMI-1 --mode 1920x1080
```

ラップトップの内蔵ディスプレイへの出力を切り、
HDMIへの出力をするなら以下のようにする

```bash
$ xrandr --output HDMI-1 --auto --output eDP-1 --off
```

ここでeDP-1は内蔵ディスプレイである。

`xrandr`は毎回切り替えがめんどくさいので、自動で切り替える`autorandr`がある


## その他のツール

必須ではないが、必要なツール群。
多くは`xfce`のものを流用している。

### タスクマネージャ

top,htopのようなCLIのものもあるが一応GUIのものも入れて置く

```bash
$ sudo pacman -S xfce4-taskmanager
```

### パネル

i3-wmでも、`xfce`で使い慣れたパネルが使いたいので`xfce4-panel`をインストールして設定する。

```bash
$ sudo pacman -S xfcer-panel
```

`~/.config/i3/config`に以下の設定を書き込む

```
exec --no-startup-id xfce4-panel --disable-wm-check
```

### ファイルエクスプローラ

ファイルエクスプローラは`xfce`で使われている`thunar`
を使う。

ただ、このままではDocuments等のアイコンがあれなので
`xdg-user-dirs`も入れる。

```bash
$ sudo pacman -S thunar xdg-user-dirs
```

インストール後に

```
$ xdg-user-update
```

を実行する。

### アプリケーションファインダー

アプリケーションファインダーはi3向けには、`dmenue`がある。
それに加えて使い慣れた`xfce4-appfinder`も使えるようにする。

設定は`~/.config/i3/config`に以下を書き込む

```
bindsym $mod+z exec xfce4-appfinder
for_window [class="xfce4-appfinder"] floating enable
```

### 起動時のメッセージ

`/etc/default/grub`を

```
GRUB_CMDLINE_LINUX_DEFAULT="text"
```
と編集する

この設定を反映させるため

```bash
$ sudo grub-mkconfig -o /boot/grub/grub.cfg
```

として`/boot/gurb`下に設定ファイルを生成する。

### Xorg関係

<!-- まだドラフト! -->

intel内蔵グラフィック用のドライバは入れていないが(標準でカーネルに組み込まれたもので動作している)、
入れたほうがいいらしい

`/etc/X11/xorg.conf.d`以下に設定ファイル`20-intel.conf`を置くと起動中で
グラフィックのところでハングしてしまう。
この設定を触るためには`xf86-video-intel`ドライバが必要である。

しかし、このドライバは現在推奨されていない。

そこで`mesa`か`modestting`(推奨は`modestting`?)をインストールすることが必要そうである。

tearingはどうなるか?`awesome`ではwindowの中央で発生した。
i3-wmではwindowの上部で少し発生する。
google-chromeはHarware accelationがoffだとtearingが中央で発生した。

とりあえず`mesa`を入れる。
