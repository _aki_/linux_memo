# Arch Linux インストールガイド(UEFI)


Arch Linuxのuefiの場合におけるイントール方法をまとめておく。

## まずはじめに

まず、`UEFI`で起動しているのか、`BIOS`で起動しているかを確認するためには以下のコマンドを実行して正常に表示されるならば`UEFI`でブートしている。

```bash
# ls /sys/firmware/efi/efivars
```

またVMの場合や有線LAN接続の場合はいらないが、wifi接続が必要な場合はまず`iwd`で設定をすること。

```bash
# iwctl device list # wifi deviceの確認
# iwctl station <device name of wifi> get-networks # ネットワークの確認
# iwctl station <device name of wifi> connect <SSID> psk # パスワード方式の場合はpskで接続する
```

## ディスクパーティション

`UEFI`環境でインストールするには、`gdisk`コマンドでパーテショニングを行う。

### パーティションの設計について

パーテーションの設計では以下のように、ざっくりとした設計としっかりした設計がある。
一時的にインストールする場合ではざっくりとでも良いが、継続して運用する場合ではしっかりと設計するべきである。

#### ざっくりとした設計

1. `/boot`

ブートローダーをインストールするための領域 `200 ~ 300 MiB`あればよい。

2. その他

#### しっかりとした設計

1. `/boot`

ブートローダーをインストールするための領域 `200 ~ 300 MiB`あればよい。

2. `/var`

各種キャッシュファイル等を格納する　`8~12 GiB`あればよい

3. `/`

基本的なパーテーション`15~20 GB`

4. `/home`

ユーザーのデータが格納される　一番多く割り振っておくのが良い。

5. `swap`

RAMが`2 GiB`以上あるならばないほうがいいらしい。

### パーティションの作成

```bash
# gdisk /dev/sda
# command (m for help): n
```

1stセクターでは何も入力せずにEnterを押す、2ndセクターでは+\<設定したい容量\>(MB,GB)としてEnterを押す。

`hexcode`は`UEFI`ブート用ならば`ef00`として、`swap`用ならば`8200`でその他のパーテーションでは何も入力セずにEnterを押す。

すでにあるパーテーションを削除して新たなパーテーションを作る場合では


```bash
# gdisk /dev/sda
# command (m for help): o
```

としてから`n`として新たなパーテーションを作成する。

すでに存在するパーテーションを確認するには

```bash
# gdisk -l /dev/sda
```

とする。

### フォーマットとマウント

ディスクのパーテーションを作成しただけでは、ファイルシステムは何を
使うのか、そのパーテーションをどこにマウントするのか等がまだ定まっていないためフォーマット\&マウントを行うことが必要である。

#### フォーマット

1. `/boot`

`/boot`パーテーションをフォーマットするには

```bash
# mkfs.vfat -F32 /dev/sda1
```

として`vfat`でフォーマットする。

2. その他のパーテーション

色々なファイルシステムがあるが、一般的なものとしては`ext4`がある。
ここでは、`ext4`にフォーマットする場合を示す。

```bash
# mkfs.ext4 /dev/sda2
```

#### マウント

ここでは、しっかりとしたパーテーション設計をした場合でのマウントを示す。

```bash
# mount /dev/sda3 /mnt　#rootをマウント
# mkdir /mnt/var
# mount /dev/sda4 /mnt/var
# mkdir /mnt/home
# mount /dev/sda5 /mnt/home
# mkdir /mnt/boot
# mount /dev/sda1 /mnt/boot
```
## 基本的なインストール

まずはデスクトップ環境以外の基本的な環境を構築する。

### 最低限のパッケージのインストール

まず`pacstrap`で以下のパッケージをインストールする。

```bash
# pacstrap /mnt base base-devel linux linux-firmware vim dhcpcd
```

次に`fstab`ファイルを生成する。

```bash
# genfstab -U /mnt >>  /mnt/etc/fstab
# cat /mnt/etc/fstab  # 確認
```

次に`chroot`によって`/mnt`以下をrootユーザーで操作する

```bash
# arch-chroot /mnt
```

### タイムゾーン、ローケーション

タイムゾーンを日本の東京に設定する。

```bash
# ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime  #時間の設定

# hwclock --systohc  # ハードウェアクロックの設定
```

ロケールの設定では、`/etc/locale.gen`を編集する

```bash
# locale-gen # ロケールファイルを生成

# vim /etc/locale.gen # en_US.UTF-8 UTF-8をアンコメント
```

次に言語の環境変数を設定するファイルを生成

```bash
# echo LANG=en_US.UTF-8 >  /etc/locale.conf
```

### ネットーワーク

まずhost名を設定する。

```bash
# echo (hostname) >  /etc/hostname
```

次に`/etc/hosts`に

```text
127.0.0.1 localhost
::1       localhost
127.0.1.1 <hostname>.localdomain <hostname>
```

と書き込む。

最後に、`dhcpcd`のデーモンを起動しておく

```bash
# systemctl enable dhcpcd.service
```

### Rootパスワード

passwdコマンドでrootユーザーのパスワードを設定する。

```bash
# passwd
```
ブートローダーの前にプロセッサのマイクロコードをインストール

```bash
# pacman -S intel-ucode
```

### boot loader(grubの場合)


まず、`grub`,`efibootmgr`をインストール。

```bash
# pacman -S grub efibootmgr
```

最後に `grub`の設定を行う。

```bash
# grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
# grub-mkconfig -o /boot/grub/grub.cfg
```

ここまでで基本的なインストールは終了となる。
これより先の操作に進むために一旦再起動を行う。

### boot loader(systemd-bootの場合)

ブートローダーをinstallする前まではgrubで起動する構成をとる時と同じ。

systemdで動くarch linuxの場合installは

```bash
# bootctl install
```

systemdで自動で更新されるようにするために以下の設定を行なう

```bash
# systemctl enable systemd-boot-update.service
```

更に`/boot/loader/loader.conf`に

```txt
default  arch.conf
timeout  4
console-mode max
editor   no
```

を追記する。

最後に`/boot/loader/entries/`にブートメニューのアイテムを追加する

`/boot/loader/entries/arch.conf`
```txt
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options root=UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx rw
```

ここで、optionsのUUIDの値は`/`のパーテーションのUUIDを入力すること。

### wifiの設定

パッケージのインストールを行い、`iwd.service`を有効化して起動する

```bash
$ sudo pacman -S iwd
$ sudo systemctl enable iwd.service
$ sudo systemctl start iwd.service
```

### 再起動

再起動を行う前に`exit`で`chroot`から抜けて、安全にシャットダウンするためにマウントしているパーテーションをアンマウントしておく。
ここで`umount`の`-R`オプションは再帰的なアンマウントを意味する。

```bash
# exit
# umount -R /mnt
# reboot
```

## 再起動後の操作

### 一般ユーザーの追加

今のままではrootユーザーのみで危険なので一般ユーザーを作成する

```bash
# useradd -m (user_name)
# passwd (user_name)
```

次に`sudo`を使えるように以下の操作を行う。

まず、

```bash
# pacman -S sudo
```
によって`sudo`をインストールし、`/etc/sudoers`に

```txt
(user_name) ALL=(ALL) ALL
```
を追記する。

### AURの設定

AURヘルパーをinstallする

```bash
sudo pacman -S --needed git base-devel
git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin
makepkg -si
```

### ディスクトップ環境の構築(Xfce)

ここからはディスクトップ環境を構築していく。

#### ディスプレイマネージャー

ディスプレイマネージャーである`lightdm`及び`Greeter`をインストールする。

```bash
# pacman -S lightdm lightdm-gtk-greeter
# ls -l /usr/share/xgreeters/
```

として正常に表示されれば無事インストールされている。

次に設定ファイル`/etc/lightdm/lightdm.conf` を次のように編集する。

```txt
~
[Seat:*]
greeter-session=example-gtk-gnome　
↓
greeter-session=lightdm-gtk-greeter　
```

最後にlightdmデーモンを有効化する。

```bash
# systemctl enable lightdm.service
```

#### ディスプレイサーバー(X11)

代表的なディスプレイサーバーであるXサーバーをインストールする。

```bash
# pacman -S xorg-server xorg-apps
```

#### ディスクトップ環境をインストールする

xfceディストップをインストールする場合は

```bash
# pacman -S xfce4 xfce4-goodies
```

とする。

## 日本語入力

このままでは、日本語入力を行うことができないので、以下の手段を踏んで
日本語入力設定を行う。
ここでは、日本語入力システムとしてはgoogle日本語入力のOSS版である`mozc`を用いる。

### 設定ファイルの編集

`/etc/locale.gen` から `en\_US.UTF-8`, `jp\_JP.UTF-8 UTF-8`をアンコメントする。

```bash
$ sudo locale-gen
```

としてロケールを生成する。

また、`/etc/locale.conf` に書き込む設定によってシステム全体の言語設定が変わる。

例) `LANG=en\_US.UTF-8`, `LANG=ja\_JP.UTF-8`

書き換えた後には

```bash
$ sudo localectl set-locale LANG=en_US.UTF-8
```

を実行する。

### インプットメソッドのインストール

インプットメソッドには, `fcitx5-mozc`を用いる。

```bash
$ sudo pacman -S fcitx5-im fcitx5-mozc
```

次に `~/.pam\_environment`を作成し、以下を書き込む

```text
GTK_IM_MODULE DEFAULT=fcitx
QT_IM_MODULE  DEFAULT=fcitx
XMODIFIERS    DEFAULT=@im=fcitx
```

最後にお好みの日本語フォントをインストールする
