# Linuxのキーマップ

Linuxでキーボードのマッピングを設定する方法のメモ

ここでは`xmodmap`を使う。

## `xmodmap`を使う

### keycodeを調べる

まず、キーのkeycodeを調べる。ターミナルで

```bash
$ xev
```

として、特定のキーを押し表示されるkeycodeを読み取る。デフォルトでは読みにくいのでgrepを使って以下のようにするとわかりやすい。

```bash
$ xev | grep keycode
```

### keyを入れ替える

先程調べたkeycodeを用いて以下のように`.bashrc`に書き込む。

```bash
# Up <-> Slash
xmodmap -e 'keycode 111 = slash '
xmodmap -e 'keycode 61 = Up'
```

とりあえず設定するならば`.bashrc`でも良いが実際には`.Xmodmap`にkeymapの設定を書き込み`.bashrc`に

```bash
xmod ~/.Xmodmap
```

と追記したほうが良い。

### RK61について

keymapをいじって/?と矢印キーを入れ替えていたが、そもそも自分のRK61ではFn + Enterを押すと/?メインか矢印キーメインかを入れ替えられるのであんまいじらなくても良かった。

### 尊師スタイルについて(ラップトップの内蔵キーボードを無効化する)

gccやgdbなどのコントリビューターとして有名で、GNUの創設者であるリチャードストルマンはラップトップpcにHHKBを乗っけて使っていた。
かれは尊師として尊敬されているため、このスタイルを尊師スタイルという。

まず、現在認識されているデバイス一覧を表示する。

```bash
$ xinput
```

内蔵キーボードは`AT Translated Set 2 keyboard`という名前のデバイスとして認識されている。
なのでこれを無効化する。

```bash
$ xinput -disable "AT Translated Set 2 keyboard" 
```

やっぱり内蔵キーボードを使いたい場合では、もう一回有効化する。

```bash
$ xinput -enable "AT Translated Set 2 keyboard" 
```

入力内容が長くてめんどくさいので.bashrcにaliasとして書き込むと便利である。

```bash
# 内蔵キーボードの無効化
alias diskey='xinput -disable "AT Translated Set 2 keyboard"' 
# 内蔵キーボードの再有効化
```
