# Linuxのディスクのお引越し
今までは120 GBのSSDでArch Linuxを運用してきたが、容量が逼迫してきたのでosをそのまま240 GBのSSDにお引越し(SSD交換)した。

基本的には[gihyo.jpの記事](https://gihyo.jp/admin/serial/01/ubuntu-recipe/0645)を参考にした。
ただし、それだけではダメだったのでブートローダの設定をやり直した。
また、[新しいハードウェアに移行する(Arch wiki)](https://wiki.archlinux.jp/index.php/%E6%96%B0%E3%81%97%E3%81%84%E3%83%8F%E3%83%BC%E3%83%89%E3%82%A6%E3%82%A7%E3%82%A2%E3%81%AB%E7%A7%BB%E8%A1%8C%E3%81%99%E3%82%8B)
,[Migrate installation to new hardware(Arch wiki)](https://wiki.archlinux.org/title/migrate_installation_to_new_hardware)の記事も有益な情報を得ることができた。

## ディスクのクローン

まず、システムを移行先のSSDをSATA-USB変換ケーブルでpcに接続する。
次にUSBのインストールメディアからライブでLinuxを起動する(今回は手元にあったManjaro Xfceを使った)。
Linuxが起動したらGpatedを起動し、クローンしたいSSDをクリックしてView -> Device -> Informationをクリックする。今回はUEFIでインストールしたシステムなのでPartition tableがgptとなっているとを確認する。

次に移行先のSSDをクリックして、Device -> Create Partition Tableをクリックする。
このとき、パーティションテーブルを選択するダイアログが表示されるので、gptを選択してApplyをクリックする。
(この操作を行うと記録されているデータは全て消去されることに注意)

完了後、コピー元に戻り、まず頭のパーティション(bootパーティション)を選択して右クリックしcopyを選択する。
次にコピー先をクリックして右クリックしてPasteをクリックする。
残りの他のパーティションも同じようにコピー元からコピー先へとcopy & pasteする。
なお、最後にペーストした後はコピー先のパーティションを端まで伸ばして残りのディスクの容量をすべて割り振ってからPasteをクリックすること。
最後にツールバーにある右端のアイコン(チェック)をクリックして、表示されるダイアログのApplyをクリックして適用する。

## ブートローダーの再設定       

ディスクのパーティションをクローンしただけではダメであったのでブートローダーの再設定を行った。
まず、USBからArch Linuxをライブ起動する。起動したら、新しいSSDのパーティションをマウントする

```zsh
# mount /dev/sdb3 /mnt # rootディレクトリのマウント
# mount /dev/sdb2 /mnt/var # /varのマウント 
# mount /dev/sdb4 /mnt/home # /homeのマウント
# mount /dev/sdb1 /mnt/boot # /bootのマウント
```

次にfstabの再設定をする。

```zsh
# genfstab -U /mnt >> /mnt/etc/fstab 
# cat /mnt/etc/fstab # 確認
```

ブートローダーの再設定を行うために/mnt下にchrootで入り、設定を行う。

```zsh
# arch-chroot /mnt 
# grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
# grub-mkconfig -o /boot/grub/grub.cfg
```

最後に再起動をして起動して正常に動けば移行先のドライブをpc内にセッティングして終了。
ついでにファンに溜まっていたホコリの掃除もしておいた。

```zsh
# reboot
```

## デュアルブートの設定

os-proberでwindowsの領域を認識させる。
```
$ sudo os-prober
```

/etc/default/grubを見て

```
GRUB_DISABLE_OS_PROBER=false
```

と書かれていることを確認してgrubの再設定をする。
今回設定したときはupdate-grubではうまく行かなかった(command not fuond)となったので

```
$ sudo grub-mkconfig -o /boot/grub/grub.cfg
```

として再設定する。
