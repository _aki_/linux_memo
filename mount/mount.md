# mount

## LinuxからNTFSなディスクをマウントする

必要なパッケージ

- `ntfs-3g`

```bash
$ sudo pacman -S ntfs-3g
```

マウントするときは

```bash
$ sudo mount -t ntfs /dev/<device name> /<mounting point>
```

と`-t ntfs`オプションをつけて実行する
