# MSYS2

## install

[公式サイト](https://www.msys2.org/)から最新版のインストーラをダウンロードし、実行する。

## ツールのinstall

MSYS2のMSYSシェルを開いて

```bash
$ pacman -Syu
$ pacman -S base-devel
$ pacman -S mingw-w64-x86_64-toolchain mingw-w64-x86_64-toolchain-gcc vim
```

他のPower ShellやGitBash等からもgcc,g++を叩けるようにWindowsのPATHに

```txt
C:\msys64\usr\bin
C:\msys64\usr\local\bin
C:\msys64\bin
```

を追加する。

Power Shell からはC/C++のビルドができたが、GitBashではできない(そもそもGitBashから叩こうとするのが間違いでは?)...(includeのヘッダ、ldによるリンク先がうまく見つからない)

