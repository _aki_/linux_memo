# polkit

polkitとは: 権限周りをうまく扱うためのツールらしい

Dropboxの認証なんかで必要。

## polkitを動かす

xfce,gnomeのようなフルスタックなデスクトップ環境の場合は依存パッケージとしてインストールされ、自動で起動するようになっている。

[Arch wikiでpolkit](https://wiki.archlinux.jp/index.php/Polkit)を調べてみると

> グラフィカル環境を使っている場合、グラフィカルな認証エージェントをインストールし、(xinitrc を使うなどして) ログイン時に自動で実行されるようにしてください。
> Cinnamon、Deepin、GNOME、GNOME Flashback、KDE、LXDE、LXQt、MATE、theShell、Xfce には初めから認証エージェントが入っています。
> 他のデスクトップ環境を使っているときは、以下の実装からどれか一つを選ぶ必要があります:

と書かれている。エージェントには色々なものが存在するが、今回は`polkit-gnome`を使うことにする。

- インストール

```bash
$ sudo pacman -S polkit-gnome
```

- 起動設定

`~/.config/sway/config`に以下を追記する

```txt
# polkit
exec /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
```

この設定によってroot権を要するアプリを起動するときにpasswdを要求して起動することが可能になった。

例)
```bash
$ systemctl start docker.service
```

しかし、polkitはwaylandを直接サポートしているいないらしく`gparted`のようなGUIアプリケーションの起動は出来なかった。

同じような症状を[このフォーラムで発見した](https://bbs.archlinux.org/viewtopic.php?id=270585)

結論としては`xorg-xhost`が必要らしい。

```bash
$ sudo pacman -S xorg-xhost
```

これによりGUIアプリケーションが起動しない問題は解決した。
