# CLIな論文管理ツールpubsのメモ

- 参考
    - [Qiita: pubsを用いたコマンドライン論文管理(2024 11/07 16:00)](https://qiita.com/cfarmeri/items/23d53c2e5c5b0cbe61ae)
    - [note: コマンドラインで文献管理(2024 11/07 16:00)](https://note.com/naba_naba/n/n215dba1f7fed)
    - [GitHub: pubs/pubs (2024 11/07 16:00)](https://github.com/pubs/pubs)

## install & setup

pipxを使うのが一番簡単

```bash
$ pipx install pubs
```

初期設定は


```bash
$ pubs init
```

とすればok

## 基本的な使い方

### 論文を登録する

```bash
$ pubs add -D <DOI> -d <path_to_journal>
```

登録された論文一覧は

```bash
$ pubs list
```

で表示できる。ここで、論文は`clitekey_name`によって名前付けされる。

目的の論文が開きたければ

```bash
$ pubs docc open <citekey_name>
```

でブラウザで論文が開かれる。

### 論文をタグ付けする

複数の論文があったときに、その論文が取り扱っているテーマが共通であった場合そのテーマで論文をグルーピングすると便利である。

これを行なうには

```bash
$ pubs tag <citekey_name> <tag_name>
```

によってタグ付けする。なおタグの名前には`-`は使えない(`_`で代用すること)。

`tag_name`でグルーピングされた論文一覧を取得するには

```bash
$ pubs tag <tag_name>
```

とする。

### citekey\_nameで紐付けられた論文を開く

```bash
$ pubs doc open <citekey_name>
```

あるタグの論文を一括で開きたければ以下のようにワンライナーする。

```bash
$ pubs tag <tag_name>  | awk '{print $1}' | sed -E 's/\[(.*)\]/\1/'  | xargs -I@ pubs doc open @
```

パブリッシャーのHPで見たい場合は

```bash
$ pubs tag <tag_name>  | awk '{print $1}' | sed -E 's/\[(.*)\]/\1/'  | xargs -I@ pubs url @
```

と`pubs doc open`を`pubs url`に置き替える。

### fzfと連携して多くの候補の中から開きたい論文を開く

```bash
$ pubs list | fzf | awk '{print $1}'| sed -E 's/\[(.*)\]/\1/' |xargs -I@ pubs doc open @
```

ここでは、`pubs list`で登録している論文一覧を取得しているが、`pubs tag <tag name>`で特定のタグに登録されいる論文一覧を取得してもok。


毎回このワンライナーを書くのもメンドウなので`.bashrc`に関数として書いておく。

```bash
sldoc() {
    fzf | awk '{print $1}' | sed 's/\[\(.*\)\]/\1/' |xargs -I@ pubs doc open @
}
```

やっぱ`pubs list`や`pubs tag <tag name>`で取得した情報からcitekeyを取り出すコマンドを分離してfzfは使うときに連結した方がよさそう。

```bash
exkey() {
    awk '{print $1}' | sed 's/\[\(.*\)\]/\1/'
}
```

```bash
$ pubs tag <tag name> | fzf | exkey |xargs -I@ pubs doc open @ # <tag name>にタグ付けされている論文から一つを選らんで開く
$ pubs tag <tag name> | exkey | xargs -I@ pubs doc open @ # <tag name>にタグ付けされている論文を全て開く
```

### Google Scholarで検索する

Google Scholarで検索したければ

```bash
$ pubs websearch <key word>
```

とするとデフォルトのブラウザでページが開く。

### tagの中から一つを選び、その\*.pdfファイルへのpathを取得する

```bash
$ echo ~/.pubs/$(pubs tag <tag name>  | fzf | exkey).pdf
```

### 論文ではない資料を登録する

```bash
$ pubs add -d <path to journal file>
# or
$ pubs add --docfile <path to journal file>
```

これをすると、エディタが立ち上ってこの資料の情報を入力することを求められるので、必要な情報を記入して saveすればOK.

### `pubs doc open <cite key>`で起動するブラウザをgoogle-chrome-stableに指定する

pubsの設定は`~/.pubsrc`に書かれていて`open`で実行されるコマンドは`open_cmd`によってデフォルトでは`open_cmd = xdg-open`になっている。
そこでgoogle-chrome-stableに限定したい場合は`open_cmd = google-chrome-stable`と書き替えれば良い。
