# sway

- 頼りになりそうなサイト
    - [Sway-ArchWiKi](https://wiki.archlinux.jp/index.php/Sway#sway_.E3.81.AE.E8.B5.B7.E5.8B.95)
    - [Sway-Gentoo WiKi](https://wiki.gentoo.org/wiki/Sway/ja#Sway_.E3.82.92.E9.96.8B.E5.A7.8B.E3.81.99.E3.82.8B)
    - [GitHub sway/sway WiKi](https://github.com/swaywm/sway/wiki#troubleshooting)

awesome, i3-wmとX11で動くWMを使っていたが、ティアリングを直せなかったので
Waylandで動WMである`sway`(スウェイ)を使うことにした。

## まず必要なパッケージ

`sway`,`xorg-server-xwayland`,`xorg-xhost`,`qt5-wayland`

二個目のパッケージはXで動くアプリをWaylandで動かすためのものである。


[web](https://zenn.dev/syui/articles/archlinux-sway-wm-i3)では
ディスプレイマネージャ、グリータを`lightdm`, `lightdm-gtk-greeter`
で起動しているが、自分の環境ではそれではログインしても再びログイン画面にもどってきてしまった。

なので、現在は仮想コンソールから直に

```bash
$ sway
```

として起動している。

`qpefview`のようなQtで動くアプリケーションが起動したときにwarnigを出すので`qt5-wayland`も新ストールした。

##  設定ファイルをどうするか

基本的には`i3-wm`のものと同じである。しかし、Xでしか有効でない設定もあるため、一部変更が必要である。

日本語入力設定は環境変数を設定するファイルを`/etc/environment`に置いているため、XでもWaylandでも同じように利用可能である。

## ディスプレイ設定

### 壁紙

壁紙を読み込ませるためにはパッケージ`swaybg`が必要である。

```bash
$ sudo pacma -S swaybg
```

インストール後に`~/.config/sway/config`にて

```
output "*" bg <Path to image> fill
```

と設定する。


### ディスプレイのlock

参考[arch wiki セッションをロック](https://wiki.archlinux.jp/index.php/%E3%82%BB%E3%83%83%E3%82%B7%E3%83%A7%E3%83%B3)

`swaylock-effects`を使う。

[github](https://github.com/mortie/swaylock-effects)


`swaylock`のフォークで見た目のカスタムがしやすいらしい

```
$ yay -S swaylock-effects
```

適当なエイリアスを`.bashrc`に書き込む

```
# for lock screen
alias lock='swaylock \
	--screenshots \
	--clock \
	--indicator \
	--indicator-radius 100 \
	--indicator-thickness 7 \
	--effect-blur 7x5 \
	--effect-vignette 0.5:0.5 \
	--ring-color bb00cc \
	--key-hl-color 880033 \
	--line-color 00000000 \
	--inside-color 00000088 \
	--separator-color 00000000 \
	--grace 2 \
	--fade-in 1'
```

### サスペンド時に画面をlockする

[`swayidle`](https://github.com/swaywm/swayidle)を使う

```bash
$ sudo pacman -S swayidle
```

以下の設定を`~/.config/sway/config`に書きこむことで、`systemctl suspend`でサスペンドした時に画面をlockする

```txt
exec swayidle -w \
    before-sleep 'swaylock \
	--screenshots \
	--clock \
	--indicator \
	--indicator-radius 100 \
	--indicator-thickness 7 \
	--effect-blur 7x5 \
	--effect-vignette 0.5:0.5 \
	--ring-color bb00cc \
	--key-hl-color 880033 \
	--line-color 00000000 \
	--inside-color 00000088 \
	--separator-color 00000000 \
	--grace 2 \
	--fade-in 1'
```

## クリップボード

`clipman`を使う

```bash
$ yay -S clipman
```

`~/.config/sway/config`には以下のような起動スクリプトを追加する
```txt
exec wl-paste -t text --watch clipman store --no-persist
```

## ポインティングデバイス

`~/.config/sway/config`に以下を書き込む

```
# tap to click for lap tocp touch pad
input type:touchpad {
    tap enabled
    natural_scroll enabled
}
```

これでタッチパッドのタップでクリックが可能となる。

laptopを使用していて、タイピング中のトラックパッドを無効化したければ以下のオプションを追加する

```txt
# tap to click for lap tocp touch pad
# disable touchpad while typing
input type:touchpad {
    tap enabled
    natural_scroll enabled
    dwt enable disable-while-typing
}
```

## 日本語入力

`fcitx5-mozc`を使う。

```bash
$ sudo pacman -S fcitx5-mozc fcitx5-im
```

再起動後に`fcitx5-config`を開いてmozcを追加する。

必要な環境変数を設定するために`/etc/environment`に以下を設定する

```
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
```


## polkit

polkitとは: 権限周りをうまく扱うためのツールらしい

Dropboxの認証なんかで必要。

xfce,gnomeのようなフルスタックなデスクトップ環境の場合は依存パッケージとしてインストールされ、自動で起動するようになっている。

[Arch wikiでpolkit](https://wiki.archlinux.jp/index.php/Polkit)を調べてみると

> グラフィカル環境を使っている場合、グラフィカルな認証エージェントをインストールし、(xinitrc を使うなどして) ログイン時に自動で実行されるようにしてください。
> Cinnamon、Deepin、GNOME、GNOME Flashback、KDE、LXDE、LXQt、MATE、theShell、Xfce には初めから認証エージェントが入っています。
> 他のデスクトップ環境を使っているときは、以下の実装からどれか一つを選ぶ必要があります:

と書かれている。エージェントには色々なものが存在するが、今回は`polkit-gnome`を使うことにする。

- インストール

```bash
$ sudo pacman -S polkit-gnome
```

- 起動設定

`~/.config/sway/config`に以下を追記する

```txt
# polkit
exec /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
```

この設定によってroot権を要するアプリを起動するときにpasswdを要求して起動することが可能になった。

例)
```bash
$ systemctl start docker.service
```

しかし、polkitはwaylandを直接サポートしているいないらしく`gparted`のようなGUIアプリケーションの起動は出来なかった。

同じような症状を[このフォーラムで発見した](https://bbs.archlinux.org/viewtopic.php?id=270585)

結論としては`xorg-xhost`が必要らしい。

```bash
$ sudo pacman -S xorg-xhost
```

これによりGUIアプリケーションが起動しない問題は解決した。

## login maneger

とりあえずTUIのログインマネージャ`lemurs`を使う


[github](https://github.com/coastalwhite)


```bash
$ yay -S lemurs-git
$ sudo sytemctl enable lemurs.service
```

`/etc/lemurs/`に`wayland`ディレクトリを作って中に`sway`ファイルを作成し以下を書き込む

```bash
#! /bin/sh
exec sway
```

swayのログを記録しければ以下のように適当なファイルにリダイレクトするようにする。

```bash
exec sway -D noatomic >> ~/.sway.log 2>&1
```

ここで、`-D noatomic`はアトミック操作をしないオプションでこれありでは安定化するらしい(ドキュメントにがほぼなくてソースを読まないとかも)。

このファイルに実行権限を与え`seat`グループに自分を追加

```bash
$ sudo chmod +x /etc/lemurs/wayland/sway
$ sudo usermod -aG aki seat
```

## オーディオ

参考:[arch wiki alsa](s://wiki.archlinux.jp/index.php/Advanced_Linux_Sound_Architectur )


オーディオを扱えるようにするようにまず`alsa-utiles`,`puluseaudio` を入れる。

```bash
$ sudo pacman -S alsa-utiles puluseaudio
```

`F1`でミュート/ミュート解除、`F2`で音量down `F3`で音量upするために、`~/.config/sway/config`に以下を書き込む。

```
# vluem control by F1(mute/unmute ) F2(down) ,f3(up) ,alt + f1(mute)
bindsym F1 exec pactl -- set-sink-mute @DEFAULT_SINK@ toggle
bindsym F2 exec pactl -- set-sink-volume @DEFAULT_SINK@ -5%
bindsym F3 exec pactl -- set-sink-volume @DEFAULT_SINK@ +5%
```

## バー

- 参考
1. [gentoo wiki](https://wiki.gentoo.org/wiki/Waybar)
2. [swayのすゝめ](https://inthisfucking.world/sway/)

### インストール

pacmanによりリポジトリからインストールする

```bash
$ sudo pacman -S waybar
```

### 設定

まずデフォルトの設定、`/etc/xdg/waybar`を`~/.config/`以下にコピーする

```bash
$ cp -r /etc/xdg/waybar  ~/.config/
```

このままではデフォルトの`sway-bar`が表示されたままで、`waybar`は表示されないので`~/.config/sway/config`の項目`bar`を以下のように編集する。

```txt
bar {
    #position top

    # When the status_command prints a new line to stdout, swaybar updates.
    # The default just shows the current date and time.
    #status_command while date +'%Y-%m-%d %I:%M:%S %p'; do sleep 1; done
    swaybar_command waybar

    colors {
        #statusline #ffffff
        #background #323232
        #inactive_workspace #32323200 #32323200 #5c5c5c
    }
}
```

ここで重要なのは`statud_command`,`position top`をコメントアウトして、`swaybar_command waybar`を追加することである。
Super + Shift + C(デフォルトの設定)で`~/.config/sway/config`を再読込すると上部にbarが表示される。

しかし、私の環境では絵文字が文字化けしてしまいました。
これは、`awesome-font`を入れることで解決できた[2]。

```bash
$ sudo pacman -S ttf-font-awesome
```

デフォルトではたくさん表示されてごちゃごちゃしているので、設定ファイル`~/.config/waybar/config`のを編集して必要なものみを表示するように変更する。

今回は"height","width","modules-left","modules-center","modules-right"のみを以下のように編集した

```txt
    "height": 10, // Waybar height (to be removed for auto height)
    "width": 1912, // Waybar width
    "spacing": 2, // Gaps between modules (4px)
    "modules-left": ["sway/workspaces", "sway/mode", "sway/scratchpad"],
    "modules-center": ["sway/window"],
    "modules-right": ["idle_inhibitor", "pulseaudio", "network", "backlight", "sway/language", "battery", "clock", "tray"],
```

デフォルトの配色は自分好みではないので`~/.config/waybar/style.css`を編集して好みの色になるようにした。

## Application launcher (rofi)

本家の`rofi`はwaylandをサポートしていないので、派生の`rofi-lbonn-wayland` をインストールする。

```bash
$ yay -S rofi-lbonn-wayland
```

## 設定

まず、デフォルトの設定ファイルを生成するために以下のコマンドを実行する。

```bash
$ rofi -dump-config > ~/.config/rofi/config.rasi
```

設定箇所はたくさんあるが、今回は`configuration {}`のうち以下の部分を設定した。

```txt
configuration {
	modes: "window,drun,run,ssh";
	font: "hack 20";
	icon-theme: "Papirus";
	combi-modes: "window,drun,run";
}
```

また、ショートカットキーで起動するようにするため`~/.config/sway/config`に以下を書き足した。

```txt
bindsym $mod+z exec rofi -show drun
```

その後、シェルから直接rofiを起動し、アプリケーションを起動することはできるが、`Super + z`では起動した場合では起動するが、アプリケーションを起動することができなくなってしまった。

そこで`rofi-lbonn-wayland`の代わりに`rofi-lbonn-wayland-only`をインストールしたところ問題なく動作した。

## swayでスクリーンショットを取る

xfceでは何もせずともPrint key(HHKBの配列ではFn + I)でスクショをとれたがwindowマネージャーのの環境を運用しているのでは、それはできない。
そのためFn + Iでスクショを取るためにはパッケージのインストール、keymapの設定が必要である。

1. パッケージのインストール

Arch wikiで調べるとWaylandを使用している場合では`grim`を使用することを推奨していたのでまず、これをインストールする。

```bash
$ sudo pacman -S grim
```

`grim`を使ってコマンドラインから以下を実行するとスクリーン全体のスクショが取れる。

```bash
$ grim hoge.png
```

2. ~/.config/sway/configにkeymapを設定する

Rk61でkeymap変更したときのことを参考に

## フローティングwindow

いくらタイル型windowマネージャーといっても全てのアプリがタイル型に配置されると、だいぶ面倒である。
そこで、一部のアプリはフローティングスタイルで配置するように設定する

まず、起動しているアプリの`app_id`もしくは、`class`のような情報を得る

```bash
swaymsg -t get_tree
```

これを基に設定を`~/.config/sway/config`に書き込む。

```txt
for_window [class=<class_name>] {
    floating enable
}
```

もしくは


```txt
for_window [app_id=<class_name>] {
    floating enable
}
```

フローティングモードでアプリが立ち上ったときのサイズは以下のように設定する(ここでは、xfce4-terminalを例にあげる)

```
for_window [app_id="xfce4-terminal"] {
   floating_minimum_size 1880 x 1080
}
```

## GUIのファイルエクスプローラ

依存するパッケージが少く軽量&使いやすさから`thunar`を使っている。

```bash
$ sudo pacman -S thunar
```

ファイルマネージャにてDesktop,Musicなどの"一般的な"ディレクトリにアイコンが適用されない場合にはxdg-user-dirsを用いるとうまくいく。

- xdg-user-dirs: インストール

```bash
$ sudo pacman -S xdg-user-dirs
```


- $HOME下にローカライズされたディレクトリを作成する

```bash
$ xdg-user-dirs-update
```


- 設定ファイル
    - デフォルトの設定ファイルは `~/.config/user-dirs.dirsと/etc/xdg/user-dirs.defaults`にある


## ToDo

7. 音量のスライダー
