<!-- draft: 後で書き直す -->
# thinkpad E14 Gen5 のメモ

thinkpad E14 Gen5にLin入れたときのメモ

## ストレージ

thinkpakd E14 Gen5(intel)はm.2ssdが二枚刺さるので二枚目を追加した。
しかし、サイズが2242サイズでないとだめなのが厄介

## 暗号化

Linuxを運用するにあたってセキュアブートは厄介である。
しかし、セキュアブートをオフにするとwindowsが起動できなくなってしまった。

そこで、windowsの設定からストレージの暗号化をオフにした

## オーディオ   

alsa-utilを入れた

これまではpulsaudioを入れていたが、今回はpipewireを入れた

### pipewire

- [pipewire: Arch Wiki](https://wiki.archlinux.jp/index.php/PipeWire#.E3.82.AA.E3.83.BC.E3.83.87.E3.82.A3.E3.82.AA) 
- [pipewire: Gentoo Wiki](https://wiki.gentoo.org/wiki/PipeWire)
- [UbuntuにPipeWireを入れて見る](https://zenn.dev/moru3_48/articles/e50c4ef9b0a5c8)
- [Fedora34 「音がならない!」で困ったら](https://www.magic-object.page/fedora34-%E3%80%8C%E9%9F%B3%E3%81%8C%E9%B3%B4%E3%82%89%E3%81%AA%E3%81%84%EF%BC%81-%E3%80%8D%E3%81%A7%E5%9B%B0%E3%81%A3%E3%81%9F%E3%82%89/)

インストール:

```bash
$ pacman -S pipewire
```

ドキュメントは`pipewire-docs`をインストールする

GUI:

`helvum`をインストール

plusaudioとの互換性のためには`pipewire-pulse`をインストール

設定ファイルをコピーする

```bash
$ cp -r /usr/share/pipewire ~/.config/pipewire
```

最後にデーモンを起動しておく

```bash
$ systemctl --user enable --now pipewire.socket pipewire-pulse.socket wireplumber.service
```

```bash
$ systemctl --user enable --now pipewire.service
```

## 音が出ない!(出力先がダミーになってしまう)

alsamixerを起動しようとすると、サウンドカードが見つからない!と出てしまう。

似たような症状でググると以下のフォーラムを発見した

[Archlinux cannot find my sound card.](https://www.reddit.com/r/archlinux/comments/p061fz/archlinux_cannot_find_my_sound_card/)

ここでは

```txt
We're gonna need some more info to go on, have you explicitly installed PulseAudio? If not you're using ALSA.

How do you know it can't find your sound card, are you saying this just because there's no audio playing or some other reason?

What's the output of lspci | grep -i audio?

It maybe possible you need either (or both of) alsa-firmware or sof-firmware via this link
```

とあったので`alsa-firmware`と`sof-firmware`を一個づつインストールして試したら、`sof-firmware`を入れたことでサウンドカードが認識されてちゃんと音が出た
