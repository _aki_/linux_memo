# Virtual Box

## Arch Linux をホストにする場合

`virtualbox`パッケージをインストールする

```bash
$ sudo pacman -S virtualbox
```

普通のカーネルを使っている場合では`virtualbox-host-arch`を他のカーネルを使っている場合は`virtualbox-host-dkms`を選択すること。

カスタムされたカーネルを使っているならばそれに対応したヘッダファイルが必要(dkmsでモジュールをビルドするのに必要)
例えば`linux-lts`(ltsカーネルを使っている)なら`linux-headers-lts`も必要
