# waylandでデスクトップ環境を構築する

今のところX11(xorg)以外のディスプレイサーバーを使ったことがないが、
Ubuntu 21.04から標準でWaylandになるなど、注目されているので、
Arch LinuxでKDE Plasmaデスクトップの環境をWaylandで構築してみた。

- 参考
    - [Wayland(Arch wiki)](https://wiki.archlinux.jp/index.php/Wayland)
    - [SDDM(Arch wiki)](https://wiki.archlinux.jp/index.php/SDDM)
    - [KDE(Arch wiki)](https://wiki.archlinux.jp/index.php/KDE)

## wayland

waylandの実装としてはwestonがある

install
```bash
$ sudo pacman -S weston 
```

## ディスプレイマネージャー

KDE Plasmaで推奨のディスプレイマネージャーはSDDMである。
現状、Waylandで動かすにはsddm-git(AUR)パッケージで動かす必要がある。

```bash
$ sudo yay -S sddm-git
```

waylandでSDDMを動かすには以下のように設定ファイルを作成する必要がある。

まず、/etc/sddm.conf.d/ディレクトリを作成して、そこに10-wayland.conf
という名前で設定ファイルを作成する。設定ファイルの内容を以下に示す。

/etc/sddm.conf.d/10-wayland.conf
```
[General]
DisplayServer=wayland
GreeterEnvironment=QT_WAYLAND_SHELL_INTEGRATION=layer-shell

[Wayland]
CompositorCommand=
```

次にSDDMを表示するためのWaylandのコンポジタが必要であるので以下のように設定する。


/etc/sddm.conf.d/10-wayland.conf
```
[Wayland]
CompositorCommand=kwin_wayland --no-lockscreen
```

KDEの設定マネージャーにはsddm-kcmを入れる(任意)これがあるとGUIでディスクトップ環境の設定が可能になる。

```bash
$ sudo pacman -S addm-kcm
```

SDDMをログイン時に起動するためにはsystemdに登録する必要がある。

```bash
$ sudo systemctl enable sddm.service
```

## KDE

KDE Plasmaを使うには plasmaグループをインストールする。
Waylandで動かすにはplasma-wayland-sessionも必要である

```bash
$ sudo pacman -S plasma plasma-wayland-session
```

## 日本語入力

- 参考
    - [waylandでfcitxを使う](https://www.archlinux.site/2017/11/waylandfcitx.html)

xorgを使っていたときは.xprofileや、.profileに環境変数を書き込んでいたが、waylandではこれらを読み込まない。
そのため、/etc/environmentに以下のように書き込む必要がある。

/etc/environment
```
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
```

