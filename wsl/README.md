# WSL

WSLの設定のメモ

## Arch Linux

本家 Arch Linuxのtarballをいじって構築する方法もあるのだが、今回はMicrosoft Storeで配布されているものを使って構築した。

### 日本語対応

デフォルトだとローケールが日本語に設定されていないめ、日本語のLaTexなどでコケてしまうので以下のように設定をする。
(Arch Linuxをinstallして日本語入力を構築するときにやったこととほぼ同じ)

`/etc/locale.gen` から `en\_US.UTF-8`, `jp\_JP.UTF-8 UTF-8`をアンコメントする。

```bash
$ sudo locale-gen
```

としてロケールを生成する。

また、`/etc/locale.conf` に書き込む設定によってシステム全体の言語設定が変わる。

例) `LANG=en\_US.UTF-8`, `LANG=ja\_JP.UTF-8`

書き換えた後には

```bash
$ sudo localectl set-locale LANG=en_US.UTF-8
```

を実行する。

### LaTex日本語環境

`pacman`で

```bash
$ sudo pacman -S texlive
```

として引っかかるパッケージをinstallするだけでは日本語関係のパッケージが入らないので

```bash
$ sudo pacman -S texlive-langcjk texlive-langjapanese
```

とcjk,japaneseのパッケージもinstallする必要がある。
